## [1.5.7](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.6...v1.5.7) (2024-09-12)


### Bug Fixes

* add link to cts ([4384aae](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/4384aae70cd75690dc70dc97215192f74545ea60))
* increase doc version ([da365b4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/da365b44268b1b3d5a239ef40994092640fd1235))

## [1.5.6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.5...v1.5.6) (2024-03-25)


### Bug Fixes

* add footer template ([67b2e57](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/67b2e57fb1d906d160de5cf47b19f8b8556d7384))
* add logos to footer ([2554f51](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/2554f51790007e6292425bf9ff426c8c29fcdfb5))
* make transition faster in logos ([6f3b4f6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/6f3b4f6fdb7e8610cffc96e783c0024b2d012b30))
* remove unused plugins ([fec8dca](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/fec8dca7acf33497776214fd26e42fd602979203))

## [1.5.5](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.4...v1.5.5) (2023-08-11)


### Bug Fixes

* another doc link fix ([46af051](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/46af051d90ebce924ac74b8e30a6ae6a8d5b1fbd))
* fix attachment ([1de4d08](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/1de4d08d8f7da4a735be75676ef4410c4213c1b5))
* fix more broken links ([598026d](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/598026d5edb34f537680177d5c44f90967194e55))
* fix some broken links ([81398e4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/81398e40aa005d4270e867874a31a3d56c384e1d))
* replace all the links to wiki.de.dariah.eu or remove them ([609cdf4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/609cdf4d6e214715333aea5df645f10ebc4ac5bd))

## [1.5.4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.3...v1.5.4) (2023-08-10)


### Bug Fixes

* resolve merge conflicts ([13d5da6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/13d5da64437326514f5f95c8acb6f393ebb1c8ef))

## [1.5.3](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.2...v1.5.3) (2023-08-03)


### Bug Fixes

* merging some things ([8800ff6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/8800ff673ae7712fad89c8e8c183b6faf18a4243))
* remove extra --- for footnotes ([4248ec6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/4248ec66a819ab2bb836b0aa41b46465e4e12673))

## [1.5.2](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.1...v1.5.2) (2023-08-03)


### Bug Fixes

* build pages for tags, too ([59fa37b](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/59fa37b6c56b95a9554a126ca305d3babc9f3080))

## [1.5.1](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.5.0...v1.5.1) (2023-08-03)


### Bug Fixes

* fix som references on main site ([2be6555](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/2be655527a8624b553ea0e64842e814e9364ff24))

# [1.5.0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.4.1...v1.5.0) (2023-08-03)


### Features

* unused ttachments removed from sites ([5881c15](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/5881c151ba2fb5a3965b13458062857656d2d81d))

## [1.4.1](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.4.0...v1.4.1) (2023-08-01)


### Bug Fixes

* add - (again!)? ([cef377b](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/cef377b53423b3cea46c5d0ba5e1721c4b4c2620))
* add - in title ([0663e98](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/0663e98e69e48d02985591c24fa277d6b18ed1a3))
* remove automatic date generation ([2c029d9](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/2c029d9e3bc867aefd67c616c22c74b1cc732625))
* rt-add – instead of - ... :-D ([8d7bda6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/8d7bda6ad6a2c70cbd396b726bc7b64054b3cd18))

# [1.4.0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.3.0...v1.4.0) (2023-06-08)


### Bug Fixes

* add strong to date ([2339583](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/23395832b7c3daa1140f08cc07a5106114e2e380))
* correct date ([88d622a](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/88d622a12acfe553198d6c9a2156d1099afe4eb9))
* we need to get the next version from semantic-release to env var! ([83c55e0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/83c55e0022aab5f505705ab54710b85216287694))


### Features

* add date as version ([ff262ff](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/ff262ff6b226d069e5c1e911b959971fa4a5a4f6))

# [1.3.0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.8...v1.3.0) (2023-06-06)


### Features

* use RELEASE for deps naming ([69b51c2](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/69b51c2b738f0262f97e524ef3b2246c6e5b4396))

## [1.2.8](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.7...v1.2.8) (2023-06-06)


### Bug Fixes

* update image build and tagging ([52e772c](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/52e772ca1767be115eb72bc891cbe9f475b88312))

## [1.2.7](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.6...v1.2.7) (2023-06-06)


### Bug Fixes

* add custom css ([29a0615](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/29a061573becc3496fd82282b47253d3811dab4c))

## [1.2.6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.5...v1.2.6) (2023-06-05)


### Bug Fixes

* add correct edit workflow to readme ([ee6a64c](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/ee6a64c730a1fc49bc4f98544a6d3ce01e4b5c9b))

## [1.2.5](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.4...v1.2.5) (2023-06-05)


### Bug Fixes

* no pages for main ([d86a3a1](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/d86a3a162f40c9c5469eddb561083ef1ae35f967))

## [1.2.4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.3...v1.2.4) (2023-06-05)


### Bug Fixes

* add date to dockerfile, too ([6689cac](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/6689cacdc7ebdf824e14e379990b395a3910ecdc))
* address all the dev container jobs, we do not need them to run! ([67cefd0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/67cefd0184b543311e0894240ac767420211990e))
* branches only! ([03eedb6](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/03eedb694c4918db6a5aa952156b4763a6ecbebd))
* correct date creation ([34f2ab4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/34f2ab43d780fa5ee65a578836c1067b94d45e34))
* dates, dates, dates ([bdbbe49](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/bdbbe49fcbc2d9c1d412331aee324156c2c5b646))
* dating....... .. ([23cf8e5](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/23cf8e52e950e5c096e439ecc28ca963e0b8abf6))
* mc ([157d533](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/157d5331a56cbee41e529bdb5b342fb0209b06f3))
* more date testing ([a6aa356](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/a6aa3567990db47aaba20ac3d656407b22f45fbf))
* play with date format ([c950172](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/c950172f835051378dcc31271ec0b0ab588c771f))

## [1.2.3](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.2...v1.2.3) (2023-06-05)


### Bug Fixes

* add version to pages ([1fe327c](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/1fe327c137185d7c36f6a3c16150654e77650aef))

## [1.2.2](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.1...v1.2.2) (2023-06-05)


### Bug Fixes

* fix gitlab ci ([9f491f8](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/9f491f8057ddc432b4827888cf95c04b51189973))

## [1.2.1](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.2.0...v1.2.1) (2023-06-05)


### Bug Fixes

* remove BOM template ([5471e97](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/5471e970e99b8fcf2b588f6ba4ac926b832db75e))

# [1.2.0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.1.0...v1.2.0) (2023-06-05)


### Features

* using semantic release on main branch only ([63fc42f](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/63fc42fb51998160f49bef6d7db03b11c0d70eb5))

# [1.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.0.2...v1.1.0) (2023-06-01)


### Features

* complete ci with semver ([c0aad08](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/c0aad089c38de9e243ee49cc0dbddacba2252ef6))

## [1.0.2](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.0.1...v1.0.2) (2023-06-01)


### Bug Fixes

* mc ([f37a0a5](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/f37a0a5d1883fee9c03d9ece13c51ffdde8a483b))

## [1.0.1](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/compare/v1.0.0...v1.0.1) (2023-06-01)


### Bug Fixes

* add date as version for gitlab pages ([b197a16](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/b197a1658ebe39681a30e35b8e3f6d87053c5fd8))
* arg! trying sed syntax ([e4e59eb](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/e4e59ebd0057856e19b26d58ab31a73613db38f3))
* correct sed ([828dba8](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/828dba898013834e449a91f61167e00092d945da))
* krams! ([06c44d0](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/06c44d045b8d090880c3ed9a7a331a9423c0acd7))
* mc ([5ae5a42](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/5ae5a427dc8df300feab0ea0461b0d005a8660e1))

# 1.0.0 (2023-06-01)


### Bug Fixes

* add footnote extension ([976305c](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/976305c0b4bf861da17bfda5d5b1d6309f0fdde6))
* add more md files withoud ids ([bb9e8cd](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/bb9e8cdfc775feaad570162729bbe35c5648fc1c))
* add semantic releases to ci ([be189c3](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/be189c3102beadfa072a4511b8d94f5036165b08))
* chose an nginx image that runs unprivileged ([7c28467](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/7c284670467e346dd83381e169fa3940f4000f32))
* first table entry added to look fine. Please DO NOT LOOK at the MD code! ([e710530](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/e7105302aa8c9d1f138153f846a7028adeffffd3))
* remove page ids from md files (so far) ([42932b4](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/42932b43113d39fb033aa9ad5a098ca339bccd15))
* update footnotes ([3409440](https://gitlab.gwdg.de/dariah-de/dariah-de-documentation/commit/34094400944923ca1a0d7bf71391e52326ba153e))

# 0.0.0 (2023-04-01)

* first commit and deployment to GitLab pages
