# 3.2 Archäologie

last modified on Jul 03, 2015

[3.1 Überblick](3.1-Ueberblick.md)

Die Zuordnung der Archäologie entweder zum kunstwissenschaftlichen oder
dem geschichts- und kulturwissenschaftlichen Fachbereich an den
Universitäten deutet bereits darauf hin, dass das methodische Vorgehen
bzw. die Forschungsschwerpunkte in dieser Wissenschaft breit gefächert
sind([^1]). Man kann zudem nicht
von *der* Archäologie sprechen, sondern nur von *den* Archäologien, da
eine Vielzahl von Kulturkreisen mit archäologischen Methoden untersucht
werden, aber aufgrund ihrer Verschiedenheit jeweils andere
Herangehensweisen erfordern. Beeinflusst durch die Entwicklung der “New
Archaeology” in der amerikanischen und britischen prähistorischen
Forschung, entstanden hochspezialisierte, verwandte Wissenschaften wie
die Archäobotanik, Archäozoologie, Bauforschung, Unterwasserarchäologie
oder Spatial archaeology. Des Weiteren fanden Methoden aus der
Anthropologie und Soziologie Eingang in den Methodenkanon der
Archäologie. Wenn man also über Standardisierung in den Archäologien
diskutiert, wird deutlich, dass die Archäologien sich aus Standards
anderer Disziplinen bedienen und diese auf ihre Bedürfnisse hin
modifizieren.

In den Archäologien wurden deshalb **verschiedene Metadatenstandards entwickelt**, die für bestimmte Aufgaben optimiert sind, z.B. für
die **Grabungsdokumentation** (Befund, Funde, Vermessungsdaten etc.)
oder **Objektbeschreibung**. Häufig orientieren sich die
zugrundeliegenden Datenmodelle für die Dokumentation an projekt- bzw.
institutionenspezifischen Forschungs- und Aufgabenschwerpunkten.

Aufgrund der **Öffnung der Archive für die überregionale Forschung und Nachnutzung der Forschungsdaten** initiiert durch Förderrichtlinien, die
die Digitalisierung und den Austausch von Forschungsdaten fördern, und
des Weiteren durch das **Outsourcing von Grabungsaufträgen an private Grabungsfirmen**, entstand in den letzten Jahren zunehmend der Druck,
nationale Lösungen für die Standardisierung bei der Datenerfassung
archäologischer Forschung zu finden. Ergebnisse dieser Entwicklung sind
Standardisierungsaktivitäten für Austauschformate von Daten und
Grabungsberichten
wie **[ADex](http://www.landesarchaeologen.de/verband/kommissionen/archaeologie-und-informationssysteme/projektearbeitsgruppen/adex/)** und
dem **[MIDAS Heritage framework](http://fishforum.weebly.com/midas-heritage-standard.html)** oder
auch der **[Dutch Archaeology Quality Standard](http://www.sikb.nl/upload/documents/archeo/knauk.pdf)**, der
Richtlinien für das Berichtswesen für Grabungsfirmen definiert. Die
Landesdenkmalpflegeämter in Deutschland haben vergleichbare Richtlinien
für die Dokumentation von Grabungen
herausgegeben([^2])[:](https://de.dariah.eu/web/guest/fachspezifische-empfehlungen?p_p_id=1_WAR_wikinavigationportlet_INSTANCE_3SyyFue0Njpo&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=4&p_r_p_185834411_nodeId=275828&p_r_p_185834411_title=2.1+Archaeologie#f17)

- [LVR-Amt für Bodendenkmalpflege im Rheinland, Prospektions- und Grabungsrichtlinien für drittfinanzierte archäologische Maßnahmen](http://www.bodendenkmalpflege.lvr.de/media/bodendenkmalpflege/service/pdf_3/Grabungsrichtlinien_2011.pdf)
- [Vorgaben zur Dokumentation archäologischer Ausgrabungen in Bayern](http://www.blfd.bayern.de/bodendenkmalpflege/service/index.php)
- [Ausgrabungen und Prospektion, Durchführung und Dokumentation, Verband der Landesarchäologen in der Bundesrepublik Deutschland](http://www.landesarchaeologen.de/fileadmin/Dokumente/Dokumente_Kommissionen/Dokumente_Grabungstechniker/grabungsstandards_april_06.pdf)
- [Brandenburgisches Landesamt für Denkmalpflege und Archäologisches Landesmuseum, Abt. Bodendenkmalpflege, Richtlinien Grabungsdokumentation](http://www.bldam-brandenburg.de/images/stories/PDF/richtlinie_grabungsdok_mit_ergaenz_2010.pdf)
- [Landesamt für Denkmalpflege Hessen, Abt. Archäologie und Paläontologie, Richtlinien zur Grabungsdokumentation](http://hessen-archaeologie.de/Download/LfDHessenGrabungsrichtlinien2005.pdf)
- [Landesarchäologie Sachsen, Dokumentation](http://www.archaeologie.sachsen.de/59.htm)

Damit liefern diese Richtlinien bereits Standards für den
Datenerhebungsprozess der Grabung in den Archäologien. Ihren
Niederschlag finden diese Daten bisher in Grabungsberichten,
Grabungstagebüchern, Karten von Plana, in fotografischer und
zeichnerischer Dokumentation, in Karten zu geomagnetischer Prospektion
u.a., die aber bisher nicht in digitaler Form und zum Schutz der
Fundstätten auch nicht ohne Zugriffskontrolle zugänglich gemacht werden
können. Weiterhin wird deutlich, dass aufgrund der förderalen
Struktur **jedes Bundesland eigene Richtlinien** herausgibt.

In den USA entwarf David Schloen die Ontologie **[OCHRE (Online Cultural Research Environment)](http://ochre.uchicago.edu/)**, die für
verschiedene altertumswissenschaftliche Projekte zur Datenmodellierung
genutzt wird. Ein Teilausschnitt aus dessen Datenstruktur wird im
Metadatenformat **[ArchaeoML](http://opencontext.org/about/concepts)** für
das Projekt **[Open Kontext](http://opencontext.org/)** genutzt. Open
Context entwickelt eine webbasierte Open-Access-Plattform für die
Organisation, den Zugang zu und die Anreicherung archäologischer Daten,
bisher mit dem Fokus auf amerikanische Projekte.

Viele Fundobjekte sind in die Sammlungen von Museen überführt worden und
unterliegen hier den Dokumentationsstandards aus der Museologie. In
Deutschland erarbeitete die Fachgruppe Dokumentation im Deutschen
Museumsbund das
Harvestingformat **[museumdat](http://www.museumdat.org/)**, angelehnt
an das in den USA vom J. Paul Getty Trust mit ARTstor
entwickelte **[CDWA lite](http://www.getty.edu/research/publications/electronic_publications/cdwa/index.html)**.
In Großbritannien hat
sich [](http://www.collectionslink.org.uk/programmes/spectrum)**[](http://www.collectionslink.org.uk/programmes/spectrum)**[SPECTRUM](http://www.collectionslink.org.uk/spectrum)**[](http://www.collectionslink.org/uk/programmes/spectrum)** durchgesetzt,
das sowohl Richtlinien für die Prozesse im Museumsmanagement bietet, als
auch dazu passend ein Metadatenformat aufweist. Unter Leitung von
**[Collection Trust](http://www.collectionstrust.org.uk/)** wird
SPECTRUM weiterentwickelt.

Im Zuge der Internationalisierung von Forschungsprojekten sind nun
weitere Standards für Austauschformate auf europäischer und
internationaler Ebene definiert worden. Im Projekt **CARARE (Connecting
ARchaeology and ARchitecture in Europeana)** entstand der gleichnamige
Standard [CARARE](http://carare.eu/ger/Materialien) für den
Datenaustausch zu archäologischen Objekten und Denkmälern mit Europeana.
Das
Metadatenformat **[LIDO](http://network.icom.museum/cidoc/working-groups/data-harvesting-and-interchange/what-is-lido.html)** (Lightweight
Information Describing Objects) führt auf internationaler Ebene
Standards der Museumsdokumentation zusammen und ist konform zur
Ontologie **[CIDOC-CRM](http://www.cidoc-crm.org/)** (CIDOC
object-oriented Conceptual Reference Model) für Kulturelle
Überlieferung, die von der Fachgruppe Dokumentation des Komitees des
internationalen
Museumsverbandes ([ICOM](http://icom.museum/professional-standards/standards-guidelines/))
entwickelt wurde.

Vielfach sind in der Archäologie Standards bzw. Richtlinien zur
Erfassung von Forschungsdaten und -prozessen bereits vorhanden, diese
sind aber noch nicht digital auswertbar, da sie bisher keinen Eingang in
Metadatenformate (siehe Grabungsdokumentationsrichtlinien) gefunden
haben. Das Projekt [ARIADNE](http://www.ariadne-infrastructure.eu/)
(Advanced Research Infrastructures for Archaeological Dataset Networking
in Europe) stellt zwei umfangreiche Berichte zu den
[Metadatenstandards](http://www.ariadne-infrastructure.eu/Resources/D3.1-Initial-Report-on-the-project-registry),
[Normdaten, Thesauri](http://www.ariadne-infrastructure.eu/content/download/4016/23202/file/ARIADNE_D3.2_Report_on%20project_standards.pdf)
und Projektregistern seiner Projektpartner zur Verfügung, der die hier
zusammengestellte Liste an Standards ergänzt.

[3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)


[^1]: Einführende Literatur zur Notwendigkeit von Standards in der
Archäologie, siehe: J.D. Richards, "From anarchy to good practice: the
evolution of standards in archaeological computing", in: *Archeologia e
Calcolatori*,
2009, [http://eprints.whiterose.ac.uk/10707/](http://eprints.whiterose.ac.uk/10707/) sowie
J. David Schloen, "Archaeological Data Models and Web Publication Using
XML", in: *Computers and the Humanities*,
2001, [http://www.springerlink.com/content/jk1q91115506k6j5/abstract/](http://www.springerlink.com/content/jk1q91115506k6j5/abstract/).

[^2]: Als grundlegende Literatur für sämtliche Richtlinien in der
Grabungsdokumentation, siehe: Jörg Biel, Dieter Kling (Hg.), *Handbuch der Grabungstechnik*, Stuttgart 1994.
