# Begleitkonzept zur Vorbereitung eines Stakeholdergremiums „Fachgesellschaften“ (M 5.4.1)

last modified on Apr 12, 2016

**Version** April 2016 / **Cluster** 5 / **Verantwortlicher
Partner** Universität Hamburg / DHd

**Dokumentstatus:** \<Final\> / **Verfügbarkeit:** \<DARIAH-DE-public\>
/ **Autoren:** \<Höckendorff, Mareike - DHd\> \<Pielström, Steffen - UWÜ\>

**Revisionsverlauf:**

|            |                     |                                                                                                  |
|------------|---------------------|--------------------------------------------------------------------------------------------------|
| **Datum**  | **Autor**           | **Kommentare**                                                                                   |
| 31.08.2014 | Mareike Höckendorff | Anlegen des ersten Entwurfes für die Konzeptionierung des Stakeholdergremiums Fachgesellschaften |
| 12.05.2015 | Mareike Höckendorff | Einarbeiten von Änderungen und ersten Ergebnissen                                                |
| 12.04.2016 | Steffen Pielström   | Finalisierung                                                                                    |

## 1. Einleitung

In diesem Dokument wird die Konstituierung eines Stakeholder Gremiums
Fachgesellschaften theoretisch dokumentiert. Zum Aufbau eines solchen
ist von zentralem Interesse, herauszufinden, welche
geisteswissenschaftlichen Fachgesellschaften in welchem Maße mit dem
Gebiet der Digital Humanities aktiv sind, oder werden könnten. Eine
besondere Rolle nehmen hier die fachgesellschaftlichen AGs ein, die sich
bereits klar zu den digitalen Geisteswissenschaften positionieren.
Jenseits individueller Fachgesellschaften hat sich im Jahre 2012 der
interdisziplinäre Verband der Digital Humanities im deutschsprachigen
Raum (DHd) konstituiert. Auch
innerhalb dieses Verbandes haben sich AGs gegründet, die allerdings
zumeist nicht fach- sondern themenspezifisch organisiert sind. Über die
Laufzeit von DARIAH II wird mit verschiedenen einander ergänzenden
Methoden der Frage nachgegangen werden, welche Fachverbände und AGs in
ein Stakeholdergremium Fachgesellschaften integrierbar wären und die
Arbeit eines solchen besonders effektiv gestalten könnten. Dieses
Dokument fußt auf den vorbereitenden Recherchen und Maßnahmen bezüglich
eines engeren Kontaktes zu Fachgesellschaften aus der ersten
Projektphase von DARIAH und wird als Living Document den Prozess der
Findung eines Gremiums Fachgesellschaften begleiten und darum bis zum
Ende der Laufzeit von DARIAH II weiter wachsen.

Das Ziel der besseren Verknüpfung zwischen DARIAH-DE, den
geisteswissenschaftlichen Fachgesellschaften und dem DHd ist es, DARIAH
als gemeinsame Infrastruktur zu nutzen, um das Forschungsfeld der
Digital Humanities genauer zu fassen. Inhaltlich wird das Arbeitspaket
“Stakeholder Gremium Fachgesellschaften”, das hier dokumentiert wird,
der Frage nachgehen, ob es so etwas wie einen oder mehrere skalierbare
“DH Faktor/en” gibt und wie weit diese/r im deutschsprachigen Raum
bereits ausgeprägt ist/sind. Dazu wird mit Hilfe von Umfragen, Workshops
und Arbeitstagungen abgefragt, in welchen Phasen des Forschungsprozesses
bereits auf welche Weise digital gearbeitet wird und an welchen Stellen
noch Bedarf an

a\) Kompetenzvermittlung
b\) Entwicklungen methodischer und
c\) Entwicklungen technischer Art

besteht. Besonders wichtig an diesem Vorgehen ist, dass eine beidseitige
Bereicherung entsteht. DARIAH-DE erhält wichtige Informationen zum
derzeitigen Stand des Forschungsfeldes und der Entwicklungsdesiderata
während die FachwissenschaftlerInnen eine gegebene Infrastruktur dazu
nutzen können, ihre Kompetenzfelder zu erweitern und interdisziplinär
Kontakte zu knüpfen.

## 2. Bedeutung von Fachgesellschaften, digitalen AGs und DHd für die digitalen Geisteswissenschaften

Obwohl die Digital Humanities im deutschsprachigen Raum inzwischen
bereits auf einige Jahrzehnte der Entwicklung digitaler Tools und
Methoden für die Geisteswissenschaften zurückblicken können, ist bis
dato noch unklar, welche Form sich das interdisziplinäre Forschungsfeld
geben möchte. Vor allem durch die Entstehung neuer Studiengänge und
Lehrstühle wird zunehmend die Frage laut, ob DH eher eine spezielle Form
der Informatik mit einem Methodenkonvolut für die Geisteswissenschaften
ist oder ob es sich um eine eigenständige Disziplin handelt. Nicht
zuletzt durch die Entwicklung von Curricula kommen immer wieder Fragen
auf, die um das Verhältnis zwischen Informatik und Geisteswissenschaft
in den DH, das Verhältnis von digitalen Geisteswissenschaften zu ihren
Fachdisziplinen und den Möglichkeiten der Interdisziplinarität kreisen.
Zwar kann das Bedürfnis danach, die Digital Humanities in die
fachwissenschaftliche Arbeit zu integrieren, an der Gründung zahlreicher
digitaler AGs innerhalb der Fachgesellschaften abgelesen werden, dies
sagt aber noch nichts darüber aus, inwiefern die neue Methodik außerhalb
dieser AGs in den Fachgesellschaften angenommen wird. Mit Gründung des
DHds im Jahre 2012 ist noch einmal zusätzlich Bewegung in das Feld
geraten, da nun digitalen Geisteswissenschaftlern unabhängig von den
traditionellen Fachgesellschaften die Möglichkeit angeboten wird, sich
innerhalb des neuen Verbandes zu organisieren. Dieses Angebot führte
dazu, dass einige der digitalen AGs den neuen organisatorischen Rahmen
für sich entdeckt und sich so ein Stück von ihren ursprünglichen
Fachverbänden weg entwickelt haben. Hinzu kommt die inhaltliche und
damit nicht selten auch personelle Überschneidung mit neuen, sich
innerhalb des DHd gründenden AGs. Insgesamt ist das Feld nach wie vor in
Bewegung und kann wie folgt schematisiert werden:

![](attachments/Begleitkonzept-zur-Vorbereitung-eines-Stakeholdergremiums-Fachgesellschaften/34046295.png)

*Abb. 1: Schematische Darstellung von Fachverbänden und AGs innerhalb der Geisteswissenschaften*

Es fällt in das Aufgabengebiet des hier beschriebenen Arbeitspaketes, zu
analysieren, welche Position sich innerhalb dieses Gefüges strategisch
am besten für DARIAH-DE eignet.

### 1.1.Geisteswissenschaftliche Fachgesellschaften und digitale AGs

Die vorbereitenden Recherchearbeiten des Arbeitspaketes “Stakeholder
Gremium Fachgesellschaften” haben gezeigt, dass die meisten
geisteswissenschaftlichen Fachgesellschaften inzwischen über eine
Arbeitsgruppe verfügen, die sich mit Digital Humanities
auseinandersetzt. Der Besuch einiger Tagungen dieser AGs (#Digitization
Konferenz an der Universität Hamburg und Treffen des Arbeitskreises
digitale Kunstgeschichte in Berlin) hat jedoch zwei Faktoren deutlich
gemacht, die bei der folgenden Konzeption des Stakeholdergremiums und
der Zusammenarbeit mit Fachgesellschaften zu berücksichtigen sind:

1\. Nicht jede AG ist in gleichem Maße im Gebiet der Digital Humanities
angekommen - während sich z.B. der Arbeitskreis digitale Kunstgeschichte
sehr intensiv mit der Methodik auseinandersetzt, herrscht unter den
Mitgliedern der Kommission Digitalisierung des Verbandes für Volkskunde
und Kulturanthropologie ein sehr unterschiedlicher Kenntnis- und
Interessenstand.

2\. Teilweise scheinen die digitalen AGs innerhalb der
Fachgesellschaften eher ein eingekapseltes Dasein zu führen, in welchem
die Verbindung zum eigenen Fachverband in praktischer Hinsicht nicht
mehr sehr aktiv genutzt wird. Auch hier ist eine Bandbreite zu
beobachten, die von der noch sehr neuen AG digitale Romanistik reicht,
die sehr gut vom Vorstand des Fachverbandes akzeptiert und unterstützt
wird, bis hin zu Fachverbänden wie z.B. dem Germanisten-, dem
Historiker- oder dem Archäologen Verband, die auf eine Kontaktaufnahme
seitens DARIAH-DE gar nicht erst reagiert
haben.

Im zweiten Faktor ist ein Grund dafür zu sehen, dass innerhalb des DHd
entweder neue digitale AGs gegründet werden oder die AGs der
Fachverbände den neuen Veranstaltungsrahmen organisatorisch für sich
nutzen.

### 1.2.DHd

Der DHd ist ein Interessenverband für digitale Geisteswissenschaftler,
der unabhängig von den disziplinären Fachgesellschaften agiert. Der
Verband ist Teil der internationalen DH Organisationen ADHO (Alliance of
Digital Humanities Organisations) und EADH (European Association for
Digital Humanities) und fungiert als Plattform und Interssenvertretung
digitaler Geisteswissenschaftler in Deutschland, Österreich und der
Schweiz. Für das DARIAH
Arbeitspaket “Stakeholdergremium Fachgesellschaften” sind folgende
Aspekte und Funktionalitäten des DHd von besonderem Interesse:

- Inhaltlich arbeitende AGs
- fachlich organisierte AGs, die in keinem Fachverband verankert sind
    (z.B. AG digitale Museumswissenschaft)
- die jährlich stattfindende DHd Konferenz als Veranstaltungsrahmen
- Möglichkeit der Verknüpfung von DARIAH Portal und DHd Webseite
    ([http://dig-hum.de](http://dig-hum.de/))
- Informationskanal der DHd Mailingliste

Darüber hinaus ist der DHd als Ergänzung zu den Fachgesellschaften bei
der Analyse des Feldes der Digital Humanities heranzuziehen. Im
Zusammenhang mit der oben genannten Leitfrage soll die Betrachtung
digitaler AGs in den Fachverbänden und des DHds zeigen, ob mit dem
zunehmenden Zugehörigkeitsgefühl zu den digitalen Geisteswissenschaften
(höherer DH-Faktor) eine Abkehr vom disziplinspezifischen Fachverband
hin zu den Organisationen der Digital Humanities einhergeht, wie nicht
nur die steigenden Mitgliedszahlen des DHd sondern vor allem auch die
Zunahme der Konferenzteilnehmer und vor allem die Organisation innerhalb
themenspezifischer AGs im DHd vermuten lassen.

## 2.   Stakeholder Gremium Fachgesellschaften - Konstituierung, Aufgaben, Ziele

Die Möglichkeiten eines Stakeholdergremiums Fachgesellschaften werden in
der zweiten Phase der Projektlaufzeit von DARIAH II vom 1.3.2014 -
28.2.2016 anhand unterschiedlicher Erhebungen und Veranstaltungen
ausgelotet. Auf diese Weise wird einerseits eruiert, welche
Schnittstellen sich zwischen Fachgesellschaften und DARIAH-DE natürlich
ergeben, an welchen Stellen Vermittlungsbedarf besteht und welche
Grenzen ein solches Gremium einhalten sollte. Auf der anderen Seite
können bei denen in DARIAH II durchgeführten und noch durchzuführenden
Veranstaltungen und Sitzungen erste Themengebiete für ein
Stakeholdergremium Fachgesellschaften bearbeitet werden.

### 2.1.Beschaffenheit des Stakeholder Gremiums Fachgesellschaften

Das Stakeholdergremium Fachgesellschaften wird in dieser zur
Konzipierung und Findung genutzten Phase als Netzwerk gedacht, dass in
seiner Struktur wandelbar bleibt. Am Ende von DARIAH II soll dann
bilanziert werden, ob und welche festeren Strukturen für eine
Fortführung von DARIAH geeignet wären. In diese Analyse muss mit
einbezogen werden, dass bereits mehrere Gremien innerhalb von DARIAH-DE
agieren (Exekutivkommittee, wissenschaftlicher Beirat,
Steuerungsgremium) und darum eine alternative Form für das
Stakeholdergremium Fachgesellschaften zu bevorzugen wäre.

### 2.2.Aufgaben eines Stakeholder Gremiums Fachgesellschaften

Wie bereits in der Einleitung erwähnt, zerfallen die Aufgabengebiete
eines Stakeholdergremiums Fachgesellschaften in drei Teilbereiche –
Vermittlung von Methodenkompetenz, Rückspiegelung tatsächlich relevanter
digitaler Methoden und Entwicklung von Desiderata für fehlende Tools und
technische Hilfsmittel. In allen drei Bereichen wird auf der
übergeordneten, strukturellen Ebene angesetzt, um einen möglichst
breiten Rücklauf verzeichnen zu können.

#### 2.2.1. Methodenkompetenzvermittlung

Bezüglich der Methodenkompetenz bedeutet dies, dass direkt bei den
Fachverbänden Kenntnisse verbessert und Vorbehalte gegenüber digitalen
Methoden und Anwendungen abgebaut werden sollten. Außerdem kann DARIAH
auf diese Weise das Bewusstsein für bestimmte methodische Besonderheiten
der DH stärken. Es ist zu hoffen, dass die Anerkennung von Forschungs-
und Lehrleistungen im Bereich der Digital Humanities verbessert wird und
so auch die Bewertung von Bewerbern für Stipendien, Preise oder Stellen
insgesamt positiver ausfallen wird. Indem Möglichkeiten der
Zusammenarbeit von DARIAH mit den Fachverbänden innerhalb der von DARIAH
angebotenen Infrastruktur eruiert werden, wird diese den Fachverbänden
bereits automatisch näher gebracht. Es soll bewusst gemeinsam an Zielen
und Inhalten gearbeitet werden, damit die Stakeholder von Beginn an das
Gefühl der Beteiligung erhalten und so auch gegenüber anderen Angeboten
DARIAHs offener und positiver eingestellt werden.

#### 2.2.2. Exploration des Forschungsfeldes Digital Humanities im deutschsprachigen Raum

Der zweite Teilbereich im Aufgabenfeld eines Stakeholdergremiums
Fachgesellschaften besteht im Wesentlichen im Gegenseitigen Austausch
zur Anwendung von digitalen Methoden in den Geisteswissenschaften.
FachwissenschaftlerInnen sollen ebenso über Trendthemen der Digital
Humanities wie z.B. Big Data oder Geohumanities informiert werden wie
sie zurückspiegeln können, inwiefern diese Themen für ihr Fachgebiet
eine Rolle spielen und welche aktuellen Strömungen dort gerade einen
hohen Stellenwert haben. So soll der Dialog und die Vernetzung auf
thematischer Ebene gestärkt werden. In dieses Aufgabenfeld gehört auch
die inhaltliche Exploration bezüglich der unterschiedlichen Verortung
innerhalb der DH auf den Ebenen der EinzelwissenschaftlerInnen, der AGs
und der Verbände. Auch hier wird wieder ein besonderer Schwerpunkt
darauf gelegt, dass die Kommunikation wechselseitig bleibt und nicht in
die Einbahnstraße führt, dass das Gremium bzw. Netzwerk als reine
Informations- und Marketingplattform oder für ein einseitiges
Informationsinteresse genutzt wird.

#### 2.2.3. Entwicklungsdesiderata

Schließlich wird der Kontakt zu FachwissenschaftlerInnen auch dazu
genutzt werden können, die von DARIAH entwickelten Tools, Methoden und
Lehrmaterialien zu testen und Verbesserungsvorschläge aus der
wissenschaftlichen Community zu bekommen. Darüber hinaus sollen Bedarfe
ermittelt werden, die vom hier dargelegten AP zu Entwicklungsdesiderata
zusammengefasst werden. Diese sollen insbesondere an die innerhalb der
Cluster ausgearbeiteten Use Cases sowie die bereits vorhandenen Tools
und Services von DARIAH anschließen. Auf diese Weise wird
sichergestellt, dass DARIAH weiterhin bedarfsgenau arbeitet. Außerdem
wird die Verbreitung von Tools und Services durch die Beteiligung der
FachwissenschaftlerInnen und die Orientierung an tatsächlichen
Forschungsabläufen gesteigert.

### 2.3.Ziele der Einbindung eines Stakeholder Gremiums Fachgesellschaften in die Arbeit von DARIAH-DE

Primäres Ziel des Aufbaus eines Stakeholdergremiums Fachgesellschaften
ist es, VertreterInnen aus den einzelnen Fachdisziplinen aktiv an den
Entwicklungen innerhalb von DARIAH-DE teilhaben zu lassen. Dabei liegt
ein besonderes Augenmerk auf den Fachdisziplinen, die bis dato
zahlenmäßig noch nicht sehr stark in DARIAH vertreten sind. Dazu gehören
insbesondere die Gebiete der Bildwissenschaften und der angewandten
Medien- und Kulturwissenschaften.

Durch die Vernetzung mit konkreten AnsprechpartnerInnen aus den
Fachdisziplinen erhalten die einzelnen Cluster und Arbeitsgruppen
innerhalb von DARIAH die Möglichkeit, ihre Zwischenergebnisse auf
Faktoren wie Stichhaltigkeit, Übertragbarkeit auf andere Disziplinen
oder ihre Ideen auf Bedarfsgerechtigkeit zu prüfen. Die Arbeit von
DARIAH kann so effektiver gestaltet werden.

Auf der anderen Seite stärkt der direkte Kontakt zwischen DARIAH und
FachwissenschaftlerInnen die Anerkennung digitaler Methoden innerhalb
der Geisteswissenschaften. Der Bekanntheitsgrad von DARIAH kann auf
diese Weise erheblich gesteigert werden. So ist ein weiteres Ziel des
Stakeholdergremiums Fachgesellschaften, die Digital Humanities zu
stärken und gleichzeitig die Nutzung der Infrastruktur, die von DARIAH
bereitgestellt wird zu erhöhen.

## 3. Strategie zum Aufbau eines Stakeholdergremiums Fachgesellschaften in DARIAH II

Die Strategie zum Aufbau des Stakeholdergremiums Fachgesellschaften
umfasst im Kern zwei Aufgabenkomplexe – einen organisatorischen und
einen inhaltlichen - , die gleichzeitig bearbeitet werden müssen:

1. Gegenseitige Vernetzung zwischen DARIAH-DE, Fachverbänden und DHd
2. Versuch der Definition eines oder mehrerer DH-Faktors/en und
    genaueres Erfassen des Forschungsfeldes

Darüber hinaus ist zu berücksichtigen, dass die einzelnen Arbeitspakete,
Cluster und AGs innerhalb von DARIAH-DE jeweils ein eigenes inhaltliches
Interesse an der Zusammenarbeit mit FachwissenschaftlerInnen haben.
Diese sollen von Anfang an mit einbezogen werden, sodass hiermit ein
dritter Aufgabenkomplex entsteht. Auf diese Weise dient das Arbeitspaket
Stakeholdergremium Fachgesellschaften auch der Interessenbündelung
innerhalb von DARIAH-DE und als erster Kommunikationskanal zwischen
DARIAH und Fachgesellschaften. So soll gewährleistet bleiben, dass die
Fachgesellschaften sich nicht von zahlreichen Einzelinteressen aus
DARIAH überrannt fühlen. Durch die zielgerichtete Ansprache soll
außerdem gesichert werden, dass Anfragen von DARIAH gegenüber
Fachwissenschaftlern auch als relevant wahrgenommen werden.

### 3.1.Gegenseitige Vernetzung - Vorgehen

Ein erstes Ziel auf dem Wege zur Gründung eines Stakeholder Gremiums
Fachgesellschaften ist es, aus jedem Fachgebiet Kontakt zu mindestens
einem Vertreter/ einer Vertreterin aufzunehmen und so ein zunächst nur
lose verbundenes Netzwerk zu erstellen. Hierzu wurde zuerst der Kontakt
zu den jeweiligen Vorsitzenden der Fachverbände und der digitalen AGs
aufgenommen. Diese VertreterInnen sind Erstkontakte, die sowohl selbst
zum Teil des Netzwerkes als auch zu Multiplikatoren geworden sind, die
diese Aufgabe an weitere Vertreter ihrer Disziplin herantragen, seien
diese nun besonders DH-affin oder besonders kritisch gegenüber digitalen
Methoden.

Parallel wurde der losere Kontakt zu Einzelwissenschaftlern gesucht,
indem z.B. Präsenz auf einschlägigen Fachtagungen gezeigt wurde. In
diesem Rahmen wurde auch eine Umfrage zur Nutzung digitaler Tools und
Methoden durchgeführt, die sich an alle Mitglieder des Fachverbandes
richtete.

Zum derzeitigen Zeitpunkt gab es darüber hinaus bereits einen Workshop,
bei dem die Fachgesellschaften dazu eingeladen wurden, Vertreter ihrer
Wahl anzumelden, um über die Entwicklung der Digital Humanities im
deutschsprachigen Raum zu diskutieren. Bei allen Vorgehensweisen wird
darauf geachtet, stets meherere Mitglieder einer Disziplin zu erreichen.

Ein ähnliches Vorgehen ist für die Vernetzung mit dem DHd anzustreben.
Auch hier werden etablierte Kanäle (Mailinglisten, Tagungen) genutzt, um
mittels einer Umfrage die Verbreitung von Tools in den einzelnen
Forschungsphasen abzufragen. Auch hier werden die Mitglieder zur
Teilnahme an den Workshops des Gremiums eingeladen. Auch hier werden
also potentiell mehrere VertreterInnen zum Netzwerk der Stakeholder
Fachgesellschaften hinzukommen. Die folgende Grafik zeigt, dass durch
diese Vorgehensweise ein Großteil der Fachdisziplinen erreicht werden
können.

![](attachments/Begleitkonzept-zur-Vorbereitung-eines-Stakeholdergremiums-Fachgesellschaften/40435837.png)

*Abb. 2: Konstituierung eines Netzwerkes der Stakeholder aus Fachgesellschaften. Personen zu denen bereits Kontakt besteht sind mit (K) gekennzeichnet, Personen, die in DARIAH mitarbeiten mit (D).*

Die Kommunikation in die Fachgesellschaften hinein wird also immer über
zwei Kanäle laufen. Einer geht über die AnsprechpartnerInnen der
digitalen AGs in die Fachverbände hinein. Dies ist der erste Ansatzpunkt
für den Aufbau eines Stakeholdergremiums, da hier die größte positive
Resonanz zu erwarten ist. Der zweite Weg der Kommunikation führt direkt
zu den Fachverbänden. Auf dem zweiten Weg werden entweder über die
FachverbandsleiterInnen Ankündigungen für DARIAH Veranstaltungen
versendet, die sich direkt an Fachwissenschaftler richten, oder die AG
Stakeholdergremium Fachgesellschaften beteiligt sich an
DARIAH-Präsentationen, die bei großen Fachverbandstagungen durchgeführt
werden.

Zusätzlich zu den Fachgesellschaften und deren digitalen AGs haben sich
innerhalb des DHds weitere Arbeitsgruppen konstituiert, die sich
ebenfalls mit dem Einsatz digitaler Methoden innerhalb der eigenen
Fachdisziplin auseinandersetzen. Diese AGs sind derzeit noch dabei, ihre
Rolle und Aufgaben zu definieren und eignen sich darum besonders für die
Integration in ein Stakeholdergremium Fachgesellschaften.

### 1.1.Versuch der Definition eines fachübergreifend anwendbaren „DH-Faktors“ – Maßnahmen

Da der verbleibende zeitliche Rahmen von DARIAH II eingeschränkt ist,
werden die Möglichkeiten eines Stakeholdergremiums Fachgesellschaften in
diesem Zeitraum nur in einem Umfang durchgeführt werden können, der als
exemplarisch gelten muss. Der erste Schritt zur besseren Einschätzung
des Forschungsfeldes ist, dieses genauer definierbar zu machen und es
einzelnen FachwissenschaftlerInnen so zu ermöglichen, sich innerhalb des
Feldes zu positionieren. Für DARIAH ergibt sich hierdurch die
Möglichkeit, sich einen Überblick darüber zu verschaffen wie weit
Digital Humanities tatsächlich in den Geisteswissenschaften verbreitet
sind. Methodisch werden zur Erkundung des Feldes zwei Formate genutzt –
die quantitative Befragung einzelner FachwissenschaftlerInnen und die
qualitative Erarbeitung von Faktoren, die die digitalen
Geisteswissenschaften bedingen, sowie die Diskussion dieser bei den
Workshops, die für das Gremium organisiert werden. Umgesetzt wird
erstere in Online-Umfragen, die teilweise in einer Interviewsituation
begleitet und teilweise autark über Informationskanäle wie Mailinglists
oder den DHd Blog geschaltet werden. Für die inhaltliche Diskussion
wurde im März 2015 bereits ein konstituierender Workshop durchgeführt,
bei dem sich die Fachdisziplinen mit ihrer Einstellung zu den Digital
Humanities vorgestellt haben. Ein zweiter darauf aufbauender Workshop
ist für Juli 2015 geplant. Hier wird die inhaltliche Diskussion von
DH-Faktoren, die beim ersten Workshop nur kurz angerissen wurde, im
Mittelpunkt stehen.

Die Liste der Maßnahmen wird im Projektverlauf weiter um zusätzliche
Aktionen ergänzt werden. Die Erweiterung des Maßnahmenkatalogs folgt der
Prämisse, möglichst unterschiedliche Kommunikationswege mit den
FachwissenschaftlerInnen zu beschreiten, um so die effektivste
Arbeitsweise für das Stakeholder Gremium Fachgesellschaften auszuloten.

#### 1.1.1. Arbeitstreffen der digitalen Arbeitsgemeinschaften in Rahmen des DH Summits im März 2015

Im Rahmen des DH Summit im März 2015 wurde das erste Arbeitstreffen des
DARIAH Stakeholder Gremiums Fachgesellschaften durchgeführt. Vorgestellt
wurde zunächst die Arbeit von DARIAH-DE und dann eine Auswahl der
vertretenen Fachdisziplinen und deren Anknüpfungspunkte an die Digital
Humanities. Diese Auswahl wurde so getroffen, dass die Varianz der
Forschungsgegenstände sowie der Standpunkt in Bezug auf die DH möglichst
groß war. Vorgestellt wurden die Fächer Archäologie, Mediävistik,
Kulturanthropologie, Romanistik und Kunstgeschichte. Unter den
Teilnehmern vertreten waren darüber hinaus die Disziplinen Germanistik,
Geschichtswissenschaft, Informationswissenschaft und Ethnologie.  

Das Thema des Arbeitstreffens lautete „Verortung der Fachdisziplinen
innerhalb des Forschungsfeldes der Digital Humanities“. Nachdem die
Vortragenden ihren jeweiligen Fachbereich und Anknüpfungsmöglichkeiten
an die DH dargestellt hatten, wurde jeweils noch einmal diskutiert,
inwiefern sich die digitale von der traditionellen Methodik
unterscheidet und ob davon gesprochen werden kann, dass bei Verwendung
bestimmter digitaler Tools und Methoden, bei bestimmten theoretischen
Überlegungen oder bei der Entwicklung neuer Forschungsansätze von
„DH-Faktoren“ gesprochen werden kann. Dabei sind erste Aspekte genannt
worden, die wenn auch nicht unbedingt konstitutiv, so doch typisch für
die Arbeit mit digitalen Geisteswissenschaften sind:

- Kluft oder Konflikt zwischen denjenigen, die auf traditionelle
Weise in einem Fachbereich arbeiten und denjenigen, die digitale
Methoden anwenden
- Selbstverständnis und Zugehörigkeitsgefühl zu den digitalen
Geisteswissenschaften
- Arbeit mit digitalen Daten, sowohl als Forschungsgegenstand als
auch im Sinne der Digitalisierung (wobei noch diskutiert wird, wie mit
diesen Daten umgegangen werden kann und soll, wenn es um
Veröffentlichung und Nachnutzung geht)
- Nutzung digitaler Tools
- Transdisziplinarität, die ihren Ursprung zwar in der
Zusammenarbeit geisteswissenschaftlicher Fächer mit der Informatik hat,
die aber auch darüber hinaus gehen kann
- Entwicklung neuer Forschungsfragen oder Betrachtungsweisen

Während die oben genannten Punkte in unterschiedlichem Maße an- oder in
Kauf genommen werden, werden folgende Aspekte der DH in den
Fachdisziplinen eher kritisch betrachtet:

- Verwendung von Standards (da diese häufig als einengend
betrachtet werden)
- Reduktion von Inhalten auf das, was digital erfassbar ist
- Die lückenhafte Digitalisierung erschwert die Arbeit in den DH
und verzögert den Forschungsprozess

Im weiteren Projektverlauf werden diese Ergebnisse in die Entwicklung
eines Faktorenmodells der digitalen Geisteswissenschaften einbezogen.
Dieses weiterentwickelte Modell wird beim 2. Arbeitsworkshop des
Stakeholdergremiums Fachgesellschaften am 17.7.2015 an der Universität
Hamburg zur Diskussion gestellt. Anschließend wird diskutiert werden,
inwiefern eine Kartierung der Digital Humanities möglich ist, die den
vertretenen Fachdisziplinen erlaubt, ihren Standort innerhalb des Feldes
der DH zu bestimmen.

#### 1.1.2.  Umfragen auf Tagungen dreier Fachverbände aus unterschiedlichen Disziplinen

Gemeinsam mit Cluster 1 wurde in den vergangenen ersten Monaten der
Projektlaufzeit von DARIAH II eine Umfrage entwickelt, die abfragt, in
welchen Phasen eines Forschungsprozesses auf welche Weise Methoden aus
den Digital Humanities eingesetzt werden und an welchen Stellen Bedarfe
für digitale Hilfsmittel bestehen. Diese Umfrage richtet sich explizit
sowohl an bereits im Feld der Digital Humanities angekommene
WissenschaftlerInnen als auch an solche, die ihre Forschungsprozesse
noch weitgehend analog ausrichten.

Um einen möglichst breiten Rücklauf zu erreichen (möglichst im
vierstelligen Bereich), wird diese Umfrage auf verschiedenen Wegen
verbreitet. Aus den Erfahrungen mit einer Umfrage anlässlich der DH2012
in Hamburg kann abgelesen werden, dass ein besonders hoher Rücklauf
erreicht werden kann, wenn eine Umfrage in einer persönlichen
Interview-Situation ausgefüllt wird. Darum wird die Umfrage in erster
Linie auf den einschlägigen Fachtagungen verbreitet. Hierzu wird eine
Zusammenarbeit mit den jeweiligen Vorsitzenden der digitalen AGs des
entsprechenden Fachverbandes angestrebt. Zusätzlich wird die Umfrage auf
fachspezifischen Mailinglisten verbreitet. Als zweite (Test-)Gruppe
werden diejenigen WissenschaftlerInnen angesprochen, die innerhalb des
DHd und damit des Feldes der digitalen Geisteswissenschaften organisiert
sind. Hierzu wird die Mailingliste des DHd sowie der DHd-Blog genutzt
werden.

##### Deutscher Historikertag 2014

Vom 23.-26.9.2014 fand der 50. Deutsche Historikertag in Göttingen
statt; rund 3.000 Gäste wurden
erwartet. DARIAH war dort
gemeinsam mit dem Kooperationsprojekt TextGrid mit einem Stand
vertreten.

Die DARIAH-Umfrage wurde hier in Form einer Online-Umfrage verbreitet,
die auf Tablet-Computern zugänglich gemacht wurde. Hierzu wurden von der
Universität Hamburg 5 Tablets und 2 studentische Hilfskräfte gestellt.
Diese sprachen mit jeweils einem Tablet auf der Tagung gezielt
potentielle Teilnehmer an und assistierten beim ausfüllen der Umfrage.
Zwei weitere Tablets standen am DARIAH Stand bereit, wo ebenfalls immer
ein Mitarbeiter zugegen war, der beim Ausfüllen der Umfrage behilflich
sein konnte. Trotz dieser und teilweise gerade durch diese
Hilfestellungen wurden wir mit der Tatsache konfrontiert, dass vor allem
bei der Zielgruppe der wenig technikaffinen Historiker, die Bedienung
eines Tablets nicht intuitiv und selbstverständlich war. Außerdem wurde
die Umfrage von sehr vielen Befragten als zu lang empfunden, was dazu
führte, dass einige ihre Teilnahme abbrechen mussten, da sie durch
andere Termine nur einen kleinen Zeitrahmen zur Verfügung hatten.
Nichtsdestotrotz wurde das Format der persönlichen Anfrage und Assistenz
positiv bewertet.

Die Umfrage wurde inhaltlich in Report 1.2.1 ausgewertet, weshalb an
dieser Stelle lediglich kurz darauf hingewiesen werden soll, dass trotz
der allgemein positiven Reaktion auf die persönlich Ansprache, dieses
Vorgehen nicht dazu führen konnte, dass ausreichend zahlreiche
Teilnehmer aquiriert wurden. Auch blieb der Kontakt flüchtig und trug
nicht dazu bei, dass Stakeholder für das Stakeholdergremium
Fachgesellschaften gefunden werden konnten. Der Kontakt war also
insgesamt einseitig und konnte so nur einen kleinen Beitrag dazu
leisten, dass DARIAH-DE in den Fachdisziplinen stärker wahrgenommen
wird. Vor allem im Gegensatz zum oben beschriebenen Workshop, der den
Teilnehmern auch die Möglichkeit bot, zu DARIAH-DE Fragen zu stellen und
Vertretern von DARIAH in einem themenbezogenen Gespräch zu begegnen.
Insgesamt verspricht ein solches Format für die stärkere Verknüpfung mit
Fachgesellschaften also weit mehr Erfolg.

##### Deutscher Romanistentag 2015

Vom 26.-29.7.2015 ist der Romanistentag in Mannheim
geplant. Hier soll gemeinsam
mit der AG digitale Romanistik ein Stand aufgebaut werden, an dem ein
persönliches Gespräch mit Vertretern von DARIAH und der AG digitale
Romanistik ermöglicht wird. Hier geht es vor allem darum, Fragen zu den
digitalen Geisteswissenschaften zu beantworten und auf die Services von
DARIAH aufmerksam zu machen. Durch die Möglichkeit des persönlichen
Gesprächs wird die Schwellenangst, die häufig den Umgang mit neuen
Methodiken begleitet herabgesetzt.  
