# Best-Practice-Empfehlungen

last modified on Sep 19, 2018

Die Nutzung der Collection Registry sowie sämtlicher anderer Dienste
DARIAH-DEs steht grundsätzlich jeder Person zur Verfügung, die über
einen DARIAH-Account verfügt. Um einen DARIAH-Account beantragen zu
können, ist eine institutionelle Anbindung nötig; Privatpersonen
bekommen nach gründlicher Prüfung des wissenschaftlichen Interesses
ebenfalls einen Account erstellt. Dem inhärent ist also auch, dass
DARIAH-DE davon ausgeht, dass die zur Verfügung gestellten Dienste
verantwortungsbewusst und auf wissenschaftlicher Basis genutzt werden.

DARIAH-DE setzt sich zudem grundsätzlich für offene Lizenzen, Open
Access und wissenschaftliche Vernetzung und Kollaboration, sowie
Nachnutzung von Forschungsdaten ein. Dem gegenüber stehen oft Ängste um
die eigenen Daten und die eigene schöpferische Leistung und eine
grundsätzliche Vorsicht bei der Vergabe von Nutzungsrechten an andere.

Die sichere, langfristige Aufbewahrung von Forschungsdaten, sowie deren
hinreichende Beschreibung und Dokumentation sind ebenfalls
Grundsatzthemen in DARIAH-DE.

Ausgehend von diesen Grundvoraussetzungen entwickelte DARIAH-DE die
folgenden Best-Practice Empfehlungen zur Nutzung der Collection
Registry:

![Best-Practice Empfehlungen zur Nutzung der Collection Registry](attachments/52723944/64970658.png)

Grafik: Hanna-Lena Meiners (SUB Göttingen)

## Im Detail

| **Eigenschaften**                     | **Mindestanforderungen/obligatorisch** | **Empfohlen** | **Optionale Zusatzangaben** | **Bemerkungen**                                     |
|---------------------------------------|----------------------------------------|---------------|-----------------------------|-----------------------------------------------------|
| **Beschreibungen (Titel, Sprache)**   |                                        |               |                             |                                                     |
| **Kollektionstyp**                    |                                        |               |                             |                                                     |
| **Rechte an Kollektionsbeschreibung** |                                        |               |                             |                                                     |
| **Zugriffsrechte**                    |                                        |               |                             |                                                     |
| **Homepage**                          |                                        |               |                             |                                                     |
| **Email**                             |                                        |               |                             |                                                     |
| **Sammlungsorte**                     |                                        |               |                             |                                                     |
| **Akteure**                           |                                        |               |                             |                                                     |
| **Sprachen der Elemente**             |                                        |               |                             |                                                     |
| **Themen**                            |                                        |               |                             |                                                     |
| **Zeitliche Angaben**                 |                                        |               |                             |                                                     |
| **Räumliche Angaben**                 |                                        |               |                             |                                                     |
| **Kollektion erzeugt**                |                                        |               |                             |                                                     |
| **Elemente erzeugt**                  |                                        |               |                             |                                                     |
| **Elementtypen**                      |                                        |               |                             |                                                     |
| **Größe**                             |                                        |               |                             |                                                     |
| **Übergeordnete Sammlung**            |                                        |               |                             |                                                     |
| **Unterkollektionen**                 |                                        |               |                             |                                                     |
| **Identifikatoren**                   |                                        |               |                             |                                                     |
| **Zielgruppen**                       |                                        |               |                             |                                                     |
| **Provenienz**                        |                                        |               |                             | Wichtig z.B. für systematische Provenienzforschung. |
| **Assoziiertes Projekt**              |                                        |               |                             |                                                     |
| **Entstehung**                        |                                        |               |                             |                                                     |
| **Rechte an Elementen**               |                                        |               |                             |                                                     |
| **Zugang**                            |                                        |               |                             |                                                     |

