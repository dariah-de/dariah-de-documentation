# Code of Conduct for DARIAH Services

last modified on Jan 29, 2021

This Service Provider (SP) will respect the relevant legal frameworks
for the protection of personal data, especially the EU data protection
directive. The [Privacy Policy](https://de.dariah.eu/privacy-policy)
applies.

| **Name of the service**              | **DARIAH Services** |
|--------------------------------------|---------------------|
| Description of the service  | Various services provided by the DARIAH-DE project (Digital Research Infrstructure for the Arts and Humanities) |
| Data controller and<br>a contact person | GWDG<br>Am Faßberg 11<br>37077 Göttingen<br>Tibor Kálmán, [dariah-coc@gwdg.de](mailto:dariah-coc@gwdg.de) |
| Jurisdiction | DE-NS Germany Niedersachsen |
| Personal data processed | 1. Following data is retrieved from your Home Organisation:<br> - your unique user identifier (eduPersonPrincipalName, ePPN)<br>2. Following data is requested from yourself unless your Home Organisation provides it:<br> - your Given Name (givenname)<br> - your Surname (sn)<br> - your Common Name (cn)<br> - your E-Mail Address (mail)<br> - your Organization's Name (o)<br>3. Following data is requested from yourself:<br> - Acceptance of the [DARIAH Terms Of Use](https://auth.dariah.eu//Terms_of_Use_germ_engl_v6.pdf)<br>4. Following data is gathered from the central DARIAH Attribute Authority:<br> - membership in authorization groups |
| Purpose of the processing<br>of personal data | Personal data and log files are used for:<br> - personalization Web content<br> - user authorization<br> - producing automatic e-mails<br> - diagnosing technical problems and statistics |
| Third parties to whom<br>personal data is disclosed | Personal data is not disclosed to 3rd parties. |
| How to access, rectify<br>and delete the personal data | Contact the contact person above.<br>To rectify the data released by your Home Organisation, contact your Home Organisation's IT helpdesk. |
| Data retention | Your personal data is deleted upon request or after five years of inactivity as a user.<br>Web server software logs are deleted after 90 days. |
| Data Protection Code of Conduct | Your personal data will be protected according to the [Code of Conduct for Service Providers](http://www.geant.net/uri/dataprotection-code-of-conduct/v1), a common standard for the research and higher education sector to protect your privacy. |

The current list of services bound to this Code of Conduct can be found
under [https://de.dariah.eu/access-to-protected-resources-with-your-home-organisation-s-user-account](https://de.dariah.eu/access-to-protected-resources-with-your-home-organisation-s-user-account).
