# Connect the DARIAH Attribute Authority

last modified on Jul 23, 2018

***This page is only for historical reference and has been superseded by
the introduction of the AAI Proxy in July 2018.***

## Overview

For convenience, here's a repeated picture of the DARIAH AAI overview:

![](attachments/44763941/44827059.png)

## Detailed Flow

The same architecture, shown as a sequence diagram, follows:

![](attachments/44765539/44827060.png)

## Configure the SP to connect to the DARIAH Test AAI

Your SP wil be provided additional attributes by the DARIAH IdP, which
will act as a SAML Attribute Authority (AA).

### Metadata exchange

First we need to exchange Metadata with the DARIAH AA, which is treated
as an IdP. If not done already...

- ...download the DARIAH test IdP's Metadata from
    [https://ldap-dariah-clone.esc.rzg.mpg.de/idp/shibboleth](https://ldap-dariah-clone.esc.rzg.mpg.de/idp/shibboleth)
    and add an appropriate \<MetadataProvider\> row (see [basic registration](DARIAH-AAI-Documentation.md).
- ...send your own SP's metadata (from
    [https://your.sp.edu/Shibboleth.sso/Metadata)](https://your.sp.edu/Shibboleth.sso/Metadata%29)
    to [register@dariah.eu](mailto:register@dariah.eu) with a request
    for entering them locally at test IdP
- For the production DARIAH IdP / AA, see below under Production

### SimpleAggregation Callout to the AA

Behind the scenes, the SP now must be configured to do a callout to the
DARIAH AA for each new session. In shibboleth2.xml, locate the existing
AttributeResolver element. Add another AttributeResolver row like this:

``` syntaxhighlighter-pre
 <AttributeResolver 
               type="SimpleAggregation"
               attributeId="eppn"
               format="urn:oid:1.3.6.1.4.1.5923.1.1.1.6">
         <Entity>https://ldap-dariah-clone.esc.rzg.mpg.de/idp/shibboleth</Entity>
</AttributeResolver>
```

For calling to the real AA, see [Production](#ConnecttheDARIAHAttributeAuthority-Production)

### Make AttributeChecker Send to Registration

Triggered on the dariahTermsOfUse attribute, the user is sent off to
DARIAH central registration (see [DARIAH Self Service Documentation](DARIAH-Self-Service-Documentation.md)). First,
tell the SP that these custom attributes exist. You do that in
attribute-map.xml. Near the end, add the line: \<Attribute
name="urn:oid:1.3.6.1.4.1.10126.1.52.4.15" id="dariahTermsOfUse"/\>

Now, on every new session, let the SP check whether your user is already
registered to DARIAH AAI. Locate the existing \<Handler\> elements in
\<Session\> in shibboleth2.xml and add another Handler below the others:

``` syntaxhighlighter-pre
<Handler type="AttributeChecker"                         
         Location="/AttrChecker"
         template="attrChecker.html"
         attributes="dariahTermsOfUse"
         flushSession="true"/>
```

In order to enable the AttributeChecker Handler, one must use a
SessionHook. Locate the \<ApplicationDefaults\> element itself and add
the sessionHook attribute:

``` syntaxhighlighter-pre
<ApplicationDefaults entityID="..."
         REMOTE_USER="eppn"
         sessionHook="/Shibboleth.sso/AttrChecker">
```

This relative URL is fixed and must correspond to the one defined for
the Handler above.

Note the REMOTE_USER: *DARIAH AAI will only accept the **eduPersonPrincipalName** attribute as an ID!*

Also make sure you have at least the following two attributes defined in
attribute-map.xml (but see all available Attributes
[here](DARIAH-AAI-Documentation.md)):

**attribute-map.xml**

``` syntaxhighlighter-pre
<!-- eduPerson attributes -->
    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.6" id="eppn">
 
<!-- DARIAH-specific -->
    <Attribute name="urn:oid:1.3.6.1.4.1.10126.1.52.4.15" id="dariahTermsOfUse"/>
```

### Redirect the User's Browser to Registration

Upon DENY, the AttributeChecker Handler will cause a client-side
redirect to some HTML page. The default lives at
/etc/shibboleth/attrChecker.html. We will configure there an HTML-based
redirect to the DARIAH registration portal. Included in the Redirect:
is:

- The entityID of the IdP we had used to authenticate to
- The return URL of our application behind SP (the "target")

Open the distributed attrChecker.html page. In the \<head\> section, add
the following on one long line:

``` syntaxhighlighter-pre
<meta http-equiv="refresh" content="5; URL=https://dariah.daasi.de/Shibboleth.sso/Login?target=/cgi-bin/selfservice/ldapportal.pl%3Fmode%3Dauthenticate%3Bshibboleth%3D1%3Bnextpage%3Dregistration%3Breturnurl%3D<shibmlp target/>&entityID=<shibmlp entityID/>"/>

```

These elements mean:

- 5 is the Delay (seconds) this page is shown. This can of course be
    adapted from 0..infinity
- entityID=\<shibmlp entityID/\> conveys the chosen IdP to the SP
    protecting the SelfService Portal, such that it can skip IdP
    discovery
- returnurl%3D\<shibmlp target/\> tells the URL of your application to
    the SelfService portal. It will redirect there once registration is
    complete
- %3Fmode%3Dauthenticat%3Bshibboleth%3D1%3Bnextpage%3Dregistration are
    just parameters the DARIAH SelService Portal needs to log you in

In case your service has service-specific Terms of Use enabled, you
should send the according DARIAH-group's ID along. Just insert another
Query parameter "initialgroup=**my-service-controibutors**", i.e. change
the URL like this: ...registration%3Breturnurl... to
...registration**%3Binitialgroup%3Dmy-service-contributors**%3Breturnurl...
To learn your group's ID assigned to your service, and register some ToU
document or link, please ask at the DARIAH Support.

Feel free to style and adapt the attrCheck.html page

- L10N / I18N
- use more \<shibmlp\> tags (cf.
    [https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPErrors](https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPErrors))
- Decide how long it is shown

Example text for HTML \<body\>:

``` syntaxhighlighter-pre
<h2>Insufficient Information</h2>
Your home organisation did not provide sufficient attributes to this service.
Therefore, you are now being redirected to the DARIAH central user registry.
<h3>More Details</h3>
Home Organisation Name: <b><shibmlp Meta-displayName /></b><br/>
Home Organisation entityID: <b><shibmlp entityID /></b><br/>
You were trying to access the following URL: <b><shibmlp target /></b><br/>
After Registration, you will be able to access your resources provided you are
authorized.<br/>
```

## Test Access

Make *sure you have an account at some Test IdP,* i.e. you have
exchanged Metadata, and your Shibboleth SP's shibboleth2.xml should
cointain:

``` syntaxhighlighter-pre
<SSO entityID="https://idp.test.org/idp/shibboleth">
    SAML2
</SSO>
```

Then proceed as follows:

1. Restart Shibboleth Daemon
2. Navigate to your protected resource
    [https://sp.example.org/secure](https://sp.example.org/secure)
3. If your user account already exists in DARIAH LDAP, including ToU:
    you should be logged in
4. If not: Web Browser will follow redirect to DARIAH test self service
    / registration portal
    1. Fill in your details (mind the mandatory fields)
    2. Do not forget to check the Terms of Use checkbox
    3. Click "save"
    4. You will be redirected back to /secure
    5. Depending on your Discovery Service settings, you might need to
        re-select your IdP

## Switch to DARIAH Production AA

Only little changes needed to move from DARIAH test AAI to production

- In shibboleth2.xml, replace the SAML Attribute Authority in
    \<AttributeResolver type="SimpleAggregation", i.e. replace
    \<Entity\>https://ldap-dariah-clone.esc.rzg.mpg.de/idp/shibboleth\</Entity\>
    with \<Entity\>https://idp.de.dariah.eu/idp/shibboleth\</Entity\>
- In attrCheck.html, replace test SelfService with production:
    URL=[https://dariah.daasi.de/](https://dariah.daasi.de/)... replace
    with URL=[https://auth.dariah.eu/](https://auth.dariah.eu/)...
- Metadata trust is established by entering your SP in a real
    federation, i.e. follow the [federation documentation](DARIAH-AAI-Documentation.md)
    and do *not forget to opt-in for eduGAIN*.
- Tell the DARIAH admins via
    [register@dariah.eu](mailto:register@dariah.eu) that your SP (don't
    forget to mention the entityID of your production SP) wants to
    receive attributes from the DARIAH production IdP
- Configure IdP discovery:

``` syntaxhighlighter-pre
    <SSO discoveryProtocol="SAMLDS" discoveryURL="https://auth.dariah.eu/CDS/WAYF">
        SAML2
    </SSO>
```
