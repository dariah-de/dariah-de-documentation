# Cultural Objects Name Authority (CONA) und Art & Architecture Thesaurus (AAT)

last modified on Jan 28, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

**[The Cultural Objects Name Authority (CONA)](http://www.getty.edu/research/tools/vocabularies/cona/)** und der
**[Art & Architecture Thesaurus (AAT)](http://www.getty.edu/research/tools/vocabularies/aat/about.html)** sind
Produkte des J. Paul Getty Trust. CONA wird als Normdatei für Kunst,
Archäologische Artefakte und Gebrauchsgegenstände in Museen,
Universitätssammlungen und privaten Sammlungen dienen, die Aufnahme von
naturwissenschaftlichen Sammlungen ist jedoch nicht geplant. Über CONA
werden Persistente Identifikatoren zu Objekten verwaltet. Der Art &
Architecture Thesaurus unterscheidet sich von CONA dadurch, dass es sich
hier um einen Thesaurus handelt, der auch zur Klassifikation von
Objekten in CONA genutzt werden kann. Derzeit ist er nur in englischer
und spanischer Sprache erhältlich, jedoch arbeiten das Institut für
Museumsforschung in Berlin, das [National Digital Archives Program in Taiwan](http://www.ndap.org.tw/1_org_en/introduction.php), das
[Rijksbureau voor Kunsthistorische Documentatie](http://english.rkd.nl/about-the-rkd), das [Istituto Centrale per il Catalogo e la Documentazione in Rom](http://www.iccd.beniculturali.it/index.php?en/115/cataloguing-standards) und
das [Canadian Heritage Information Network](http://www.rcip-chin.gc.ca/index-eng.jsp) an einer Übersetzung
bzw. Integration ihres Vokabulars in den AAT. Der AAT weist **sieben
Hauptfacetten** auf:

- Assoziierte Begriffe – abstrakte Begriffe, wie Schönheit,
    Kennerschaft, Metapher, Freiheit
- Physische Begriffe – empfindbare und messbare Eigenschaften, wie
    Größe, Form, Textur
- Stile und Epochen – stilistische und Epochen-Begriffe, wie
    Tang-Dynastie oder Chippendale
- Agens – Personen oder Organisationen, wie Zeichner, Drucker,
    Architekt, Werkstatt
- Aktivitäten – Begriffe, wie Korrosion, Ausstellung, Malen
- Material – Stoffe/Substanzen, wie Holz, Eisen, Ton, Nylon
- Objekttypen – Begriffe, wie Gemälde, Amphore, Garten
