# DARIAH AAI NG Service Provider Workshop 2019

last modified on Okt 30, 2018

**January 21/22, 2019, Tübingen, Germany**

This Service Provider workshop is intended for researchers in the DARIAH
context that develop or operate online services for the Digital
Humanities (DH). The workshop will introduce the DARIAH AAI Next
Generation and enable its participants to install, configure and test
the Open Source Shibboleth SP to integrate with an online service. The
main goal is to make the participants familiar with the Shibboleth SP
and how it integrates with their Web application.

Researchers that want to share their online services within DARIAH can
take advantage of the [**DARIAH Authentication and Authorization Infrastructure**](DARIAH-AAI-Documentation.md) **(AAI)**.

The DARIAH AAI enables researchers from
[eduGAIN](https://technical.edugain.org/status) to access DARIAH
services, by using the interoperable
[SAML](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=security)
standard. Users can log in at their home institution, without the need
to create accounts and remember passwords for the online services they
want to access. Adding to this, the DARIAH AAI allows for central yet
distributed management of group memberships. Thus, DARIAH online
services can base their authorization decisions on these memberships.

**Registration is now open!**

For more details on the agenda, the venue and to *register your place*,
visit
[https://daasi.de/en/workshop-for-dariah-aai-ng-service-provider-at-daasi-international/.](https://daasi.de/en/workshop-for-dariah-aai-ng-service-provider-at-daasi-international/)
