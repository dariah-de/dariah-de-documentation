# DARIAH Collection Description Data Model (DCDDM)

last modified on Jun 28, 2017

## [![Zur DARIAH-DE Website](attachments/DARIAH-Collection-Description-Data-Model-DCDDM/52729325.png "Zur DARIAH-DE Website")](http://de.dariah.eu)

## XML-Schema und ausführliche Dokumentation

[https://github.com/DARIAH-DE/DCDDM](https://github.com/DARIAH-DE/DCDDM)

## Einleitung

Das DARIAH Collection Description Data Model (DCDDM) ist, schlicht
gesagt, ein Datenmodell für Sammlungsbeschreibungen. Es wurde in
Zusammenarbeit mit der Herzog August Bibliothek Wolfenbüttel (HAB)
entwickelt und basiert in seinen Grundzügen auf dem Dublin Core
Collections Application Profile (DCCAP). Das Hauptziel bei der
Erstellung des DCDDM lag in der Unterstützung von Institutionen und
EinzelwissenschaftlerInnen bei der Erstellung von Beschreibungen von
physikalischen (und/oder analogen) UND digitalen Sammlungen, die sowohl
von Menschen als auch maschinell gelesen und interpretiert werden
können.

Das DCDDM gibt eine feste Anzahl von Klassen, Elementen, Attributen und
Werten an, die für die Beschreibung von Sammlungen verwendet werden
können. Nach der Definition des Dublin Core ist eine Kollektion “eine
Aggregation von Ressourcen“. Bei diesen Ressourcen kann es sich um
digitale Objekte wie digitalisierte Gemälde, Bücher, Manuskripte sowie
“born-digital objects” handeln wie bspw. Aufzeichnungen von Metadaten,
Datenbankinhalte oder Transkripte, aber auch um physische Objekte
(Gemälde, Bücher, Statuen, Briefmarken, Münzen usw.)

Es ist jedoch nicht immer leicht zu erkennen, was genau die Ressourcen
einer Sammlung überhaupt sind. Nehmen wir zum Beispiel eine Sammlung von
frühen gedruckten Büchern, deren Metadaten gesammelt und veröffentlicht
werden durch einen Online Public Access Catalog (OPAC). Durch die
Beschreibung dieses OPAC-Eintrags wird eine Sammlung von (physischen)
Büchern oder die Sammlung der jeweiligen Metadaten (Autor,
Erscheinungsjahr, Seitenzahl, Unterschrift etc.) definiert. Auf der
einen Seite stehen die gemeinsamen Benutzer eines OPAC, die vor allem
(wenn nicht sogar ausschließlich) an dem physischen Objekt interessiert
sind. Sie würden vermutlich danach suchen, wann die Bücher
veröffentlicht oder erstellt wurden und an dem Datum der Erstellung der
Metadatensätze eher nicht interessiert sein. Entsprechend des Interesses
des Nutzers werden bereitgestellte Informationen also anders
wahrgenommen und genutzt. Die Frage stellt sich, wie der Zugang zu
Informationen auf NutzerInnen zugeschnitten werden kann, ohne vorher zu
wissen, wie Sammlungsbeschreibungen genutzt werden und für welchen
Zweck. Eines der Hauptziele des DCDDM ist es, dieses Problem zu lösen,
indem die Möglichkeit geboten wird, so viele (sinnvolle) Informationen
wie möglich in einer Sammlungsbeschreibung anzugeben.

Eine weitere Herausforderung, die häufig schnell offensichtlich wird,
ist das meist unklare oder nicht einheitliche Verständnis und der
zugrunde liegenden Definitionen zu den Begriffen „Sammlung“ und
„Sammlungsobjekt“ in den unterschiedlichen geistes- und
kulturwissenschaftlichen Disziplinen. Darüber hinaus gibt es keine
strikte Unterscheidung zwischen den Objekten der Sammlung (z. B.
Bücher), den Metadaten der Objekten (z. B. Informationen über den Autor
und den Titel) und den tatsächlichen Manifestationen dieser
Informationen in Form von beispielsweise in der Datenbank gespeicherten
Bit-Streams.

Ein beträchtlicher Aspekt der Bereitstellung solcher breit gefächerten
Informationen ist, dass der Prozess der Erfassung aller relevanten
Informationen sehr zeitaufwendig sein kann. Nach den aktuellen
Anforderungen des DCDDM kann man eine gültige Beschreibung erstellen,
indem man nur rudimentäre Informationen hinzufügt. Dies bedeutet aber
auch, dass eine Sammlungsbeschreibung in ihrem Detaillierungsgrad stark
variieren kann (siehe Kapitel User-Guides und Nutzungsempfehlungen). Ein
wichtiger Aspekt bei der Nutzung der Collection Registry und der
Beschreibung von Sammlunge ist in dieser Hinsicht auch die Bereitschaft
der NutzerInnen, Ressourcen zu investieren, um eine
Sammlungsbeschreibung, die originär beispielsweise im Rahmen eines
Forschungsvorhabens erstellt wurde, um dezidierte Fragestellungen
abzubilden, mit den notwendigen Informationen für das generisch
nachnutzbare DCDDM Metadatenprofil zu erweitern. Natürlich hängt dabei
die Granularität und die Anzahl der bereitgestellten Informationen auch
von der Sammlung selbst ab, den sammelnden Einrichtungen sowie der
Infrastruktur, die Einrichtungen und Wissenschaftler zur Verfügung
stehen. Um Sammlungen tiefer zu erschließen, ist weiterhin ein gewisses
Maß an Know-How in Sachen Metadatenmanagement von Vorteil.

## Anforderungen und Ziele des DCDDM

Das Datenmodell ermöglicht Beschreibungen von digitalen und physischen
Sammlungen.

Die Beschreibung enthält allgemeine beschreibende Informationen über den
Inhalt der Sammlung oder die Sammlungsgegenstände, wie z. B.:

- Themen, die von der Sammlung abgedeckt werden;
- Standorte und Regionen, mit denen der Inhalt der Sammlung verbunden
    ist;
- Datum und Zeiten mit denen die Sammlungsobjekte verknüpft sind.

Ermöglichen einer eindeutigen Identifizierung der beschriebenen
Sammlung;

Kontextualisierung von Beziehungen bzw. Relationen

- Zwischen Sammlungen selbst (eine Sammlung als Teil einer anderen)
- Zwischen Sammlungen und Akteuren (Agents), die mit der Sammlung
    interagieren (wie Besitzer oder Kuratoren)

Sammlungsbeschreibungen auf der Grundlage der DCDDM fördern die
Erkundung und Nutzung der beschriebenen Sammlungen und stellen
vielfältige administrative Informationen zur Verfügung:

- Wie man auf die Sammlung oder ihre Gegenstände zugreifen kann
- Mögliche gesetzliche Einschränkungen hinsichtlich der Möglichkeiten,
    auf die Sammlung zuzugreifen und ihre Gegenstände zu nutzen.

Wenn Sammlungen online zugänglich gemacht werden, sorgt die
Bereitstellung technischer Metadaten dafür, dass andere Dienste Zugriff
auf die Objekte der Sammlung oder die Metadaten des Objekts erhalten

Um eine Mehrdeutigkeit zu vermeiden, fördert das DCDDM die Verwendung
kontrollierter Vokabularien und Normdaten, eindeutiger Identifikatoren
und Syntaxcodierungsschemata. Um die menschliche Lesbarkeit der
Sammlungsbeschreibung zu gewährleisten, liefert das Datenmodell
Bezeichnungs-Elemente.

- Zur Erleichterung von Zuordnungen und Transformationen bereits
    bestehender Sammlungsbeschreibungen zu den auf Basis des DCDDM
    erfolgten Beschreibungen werden kontrollierte Vokabularien und
    Kodierungssysteme unterstützt (in vollem Bewusstsein der Nachteile
    einer solchen Politik)

Das DCDDM unterstützt mehrsprachige Beschreibungen, die flexibel und
dennoch praktisch gestaltet werden können. Dies bedeutet, dass die
beschreibende Person die Sprache für eine gegebene Information auswählen
kann.

Das Datenmodell muss flexibel genug sein, um gut kuratierte Sammlungen
in Museen, Bibliotheken, Archiven usw. zu beschreiben aber auch
Material, das von einzelnen Forschern gesammelt wird. Um diesen
Anforderungen gerecht zu werden, ist die Anzahl der obligatorischen
Elemente so niedrig wie möglich gehalten.

Um eine hohe und weit verbreitete Akzeptanz zu erreichen, ermöglicht das
Datenmodell zumindest eine einfach auszugebende, zu lesende und zu
verstehende Serialisierung (wie eine csv-Tabelle).

Um die Interoperabilität zu erleichtern und die semantische Nähe zu
bekannten und weitgehend verwendeten Metadatenaufzeichnungen (z. B.
Dublin Core) zu vermitteln, werden die Namen und Definitionen vieler
Klassen, Elemente und Attribute von etablierten Standards übernomme. Um
jegliche (technischen) Abhängigkeiten zu vermeiden, werden diese Namen,
Definitionen und Spezifikationen nicht nur durch einfaches Angeben von
Zitaten oder Links zu den referenzierten Elementen referenziert, sondern
auch im DCDDM angegeben.
