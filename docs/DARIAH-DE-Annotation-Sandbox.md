# DARIAH-DE Annotation Sandbox

last modified on Sep 11, 2018

## Idee

Annotation ist in den letzten Jahren ein wichtiges Thema geworden und
jetzt erst wurde vom W3C dafür ein Standard
([https://www.w3.org/annotation/](https://www.w3.org/annotation/))
beschlossen. Diesen nun zu Implementieren, zu testen und ggf.
fortzuschreiben, bietet noch Innovationspotantial. Eine mächtige
Infrastruktur mit zahlreichen Funktionen gibt es bei DARIAH-DE auch. Die
Sandbox zielt auf einen Tauglichkeitstest der Infrastruktur für diese
Zwecke und möchte die bestehenden Komponenten auf die Nutzung für
Annotationswerkzeuge prüfen. Das Annotationstool selbst soll nicht neu
geschrieben werden.

## Anforderung

Die Anforderung lautet: Annotationen im Browser vornehmen, speichern,
abrufen und ggf. weiterverarbeiten und weitergeben zu können. Als
Beispiel kann jede beliebige Digitale Edition dienen, die man in
irgendeiner Form annotieren möchte. D.h. vorgenommen werden können
sowohl Kommentare der Herausgeber, als auch Kommentare der damit
arbeitenden NutzerInnen.

## Frontend

Annotationswerkzeuge gibt es viele. Weit verbreitet ist das
JavaScript-Tool AnnotatorJS, was auch beim Annotationsdienst
von *hypothesis* genutzt wird. Es ist eine JS-Bibliothek, die nach laden
der Dateien auf einen Webserver mit ein paar wenigen Zeilen Javascript
in jede Webseite integriert werden kann. Die dahinter stehende
Architektur muss entsprechend für die Authentifizierung, Speicherung und
Ausgabe der Annotationen sorgen.

Annotationen werden wie folgt erfasst. Man markiert Text auf der
Webseite, kann anschließend auf ein am Ende der Markierung befindliches
Icon klicken, wonach sich ein Textfeld öffnet. Möglich ist die Eingabe
von "Kommentar" und "Tag", wobei beides Freitextfelder sind. Tags werden
durch Leerzeichen getrennt.

### Beispiel 1: TextGrid Repository

In einer kleinen Beispielanwendung haben wir das TextGrid Repository
(alte Version) das JavaScript in die Seite eingebaut und wir lassen
Annotation im Text (Daten und Metadaten eines Objektes)
zu: [https://annotation.de.dariah.eu/textgridrep/browse.html?id=textgrid:jmrh.0](https://annotation.de.dariah.eu/textgridrep/browse.html?id=textgrid:jmrh.0)

Lädt man diese Seite ohne zuvor eingeloggt zu sein, erscheint eine
blendet sich kurz eine Warnung ein. Die Funktion "AnnotatorLogin" ist im
Menü oben hinterlegt und führt zum DARIAH-DE Single Sign On. D.h. mit
bestehendem DARIAH-DE Login muss man keine Daten eingeben, man wird
direkt wieder zurück zur Herkunftsseite geschickt und kann Annotationen
sehen und vornehmen.

### Beispiel 2: Epidat

In die [Epigraphische Datenbank "Epidat"](http://www.steinheim-institut.de/cgi-bin/epidat?id=hha-3361),
in der Grabinschriften jüdischer Friedhöfe erfasst und verarbeitet
werden, wurde die Funktion ebenfalls integriert. Es gibt ein dezentes
Icon oben rechts, was die Annotationsfunktionen bereitstellt. Wieder ist
ein Login erforderlich und danach kann annotiert werden.

### Beispiel 3: VIA

Mit "VIA" steht innerhalb der Annotation Sandbox die Funktion bereit,
eine beliebige Webseite zu annotieren, auch wenn diese nicht die
entsprechende Funktion integriert hat. Man steuert eine URL
wie [https://annotation.de.dariah.eu/via/**http://www.spiegel.de/**](https://annotation.de.dariah.eu/via/http://www.spiegel.de/) an
und kann diese Seite dann annotieren, aber auch jede weitere Seite zu
der man per Link geleitet wird.

## Data Annotation

Um nicht nur die Annotation auf Ebene der Visualisierung vornehmen zu
können, sondern vielmehr eine echte Standoff Annotation zu XML-Daten zu
ermöglichen, steht eine Komponente bereit, die XML-Dokumente aus
Repositorien abholen kann, die XML-Elemente in HTML5 Custom Elements
umschreibt und damit innerhalb einer Webseite eine stabile Annotation
auf der Datenebene ermöglicht.

| **TEITEI Source
&lt;div type=&quot;text&quot; xml:id=&quot;tg105\.2&quot;&gt;
	&lt;div type=&quot;h4&quot;&gt;
		&lt;head type=&quot;h4&quot; xml:id=&quot;tg105\.2\.1&quot;&gt;72\. Das Erdloch bei Elvese\.&lt;/head&gt;
		&lt;p xml:id=&quot;tg105\.2\.2&quot;&gt;
Auf einer Wiese bei Elvese, \[…\] gekommen sind\.
		&lt;/p&gt;
	&lt;/div&gt;
&lt;/div&gt;
** | **HTML5Custom Elements
&lt;tei\-div type=&quot;text&quot; xml:id=&quot;tg105\.2&quot;&gt;
	&lt;tei\-div type=&quot;h4&quot;&gt;
		&lt;tei\-head type=&quot;h4&quot; xml:id=&quot;tg105\.2\.1&quot;&gt;72\. Das Erdloch bei Elvese\.&lt;/tei\-head&gt;
		&lt;tei\-p xml:id=&quot;tg105\.2\.2&quot;&gt;
Auf einer Wiese bei Elvese, \[…\] gekommen sind\.
		&lt;/tei\-p&gt;
	&lt;/tei\-div&gt;
&lt;/tei\-div&gt;
** |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

Die Steuerung der Darstellung erfolgt über ein minimal angepasstes CSS,
bereitgestellt von der [Text Encoding Initative via GitHub](https://github.com/TEIC/TEI/blob/dev/TEICSS/tei.css).

Momentan stehen die Dokumente aus dem TextGrid Repository und dem
Deutschen Textarchiv zur Annotation zur Verfügung. Man lädt die externen
Daten via URL-Parameter in das Tool:

- für DTA-Texte gilt folgende URL:
  - Parameter: uri
  - Prefix: dta
  - URI: interner Bezeichner, zu finden in der URL des
        Titeleintrages und auch via Resolvereintrag (letzer Part der
        URL,
        siehe [Beispiel](http://nbn-resolving.org/urn/resolver.pl?urn=urn:nbn:de:kobv:b4-200905191340))
  - Beispiel: [https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=](https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=dta:fontane_stechlin_1899)**[dta:fontane_stechlin_1899](https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=dta:fontane_stechlin_1899)**
  - Die Stabilität des Abrufes der XML-Datei ist nicht geprüft.
- für TextGrid-Dokumente gilt folgende URL:
  - Parameter: uri
  - Prefix: dta
  - URI: TextGrid-URI
  - Beispiel: [https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=**textgrid:qx89.0**](https://annotation.de.dariah.eu/AnnotationViewer/data.html?uri=textgrid:qx89.0)
  - zusätzlich werden Abbildungen als Schatten-Elemente integriert, siehe [Commit](https://github.com/DARIAH-DE/eXanore-viewer/blob/ca946bdc07f3c25ede772871220696453881c3f8/data.html#L65).

Diese Feature kann einfach auf weitere Repositorien angewendet werden.
Anfragen dazu können via Pull Request oder Issue
unter [GitHub](https://github.com/DARIAH-DE/eXanore-viewer) gestellt
werden.

## Store

Eingerichtet wurde ein eigener Annotation Store, der innerhalb einer
eXist-db läuft. Der Quellcode liegt bei GitHub.

## Manager

Eigene Annotationen werden im Überblick in einem
einfachen [Webinterface](https://annotation.de.dariah.eu/AnnotationViewer/index.html) dargestellt.
Die Tabelle zeigt alle getätigten Annotationen und bietet Sortier- und
Filterfunktionen. Zudem werden noch einige Metadaten ausgegeben. Man
kann zu den einzelnen Annotationen browsen und bei Bedarf Änderungen der
Annotationen auf den jeweiligen Seiten vornehmen.  
Die Tabelle lässt die Auswahl bestimmter Annotationen zu. Dafür gibt es
die Buttons "Select all" und "Deselect all" und die Möglichkeit per
Klick (Mehrfachauswahl mit \[SHIFT oder CTRL\] + Klick) auszuwählen.

### Export

Diese Applikation bietet zwei Exportformate an. Zum einen kann man alle
angezeigten Annotationen (also auch nach Filtern und in Reihenfolge der
angezeigten Sortierung) im CSV-Format exportieren, zudem steht die
Option ausgewählte (Auswahlfunktionen siehe oben) Annotationen in einem
XML-Format in TextGrid speichern zu lassen. Damit steht auch der
Weiterverarbeitung der Annotationen mittels gängiger XML
Transformationswerkzeuge (XQuery, XSLT) nichts im Wege.

### Gruppen

Innerhalb der Applikation kann man Gruppen anlegen, Mitglieder einladen,
Annotationen auf diesem Weg teilen und Gruppenannotationen mit
vorgegebener Farbe auf den Webseiten hervorheben zu lassen.  
Die Gruppen sind nicht nur zur Collaboration auf Arbeitsgruppenebene
einsetzbar. Denkbar ist auch, einen Gruppenname mit semantischen Gehalt
zu versehen und auf diesem Wege ein gemeinsames Tagset zu etablieren.
D.h. man würde eine Gruppe nicht nach Projekt benennen, sondern eher
eine Bezeichnung nach dem Schema "Projekt-Tag" benutzen. Annotationen
selbst könnten dann erfasst werden und über den *Annotation
Manager* einer solchen Gruppe hinzugefügt werden und so ihren
inhaltlichen Gehalt bekommen. Dieses Vorgehen wäre bei immer
gleichartiger Auszeichnung zu bevorzugen, da die Alternative eher
mittels einer Liste, aus der zu jeder Annotation das Tag auszuwählen
ist, realisiert wäre und so redundante Arbeitsschritte entstünden.

#### Rollen

Derzeit gibt es in einer Gruppe beliebig viele Mitglieder. Sie sind
entweder Administratoren oder Contributor.

##### Admin

Administratoren haben das Recht die Farbe der Hervorhebung der
Annotation auf der Webseite gruppenweit zu ändern und neue
Administratoren und Contributor der Gruppe hinzuzufügen.

##### Contributor

Contributor haben nur Leserechte, sie können damit aber auch die
Exportfunktionen des *Annotation Manager* benutzen.

#### Annotation einer Gruppe hinzufügen

Über die Auswahlfunktionen können Annotationen selektiert werden und
über den Button "Add to Groups" einer oder mehreren Gruppen hinzugefügt
werden. Es bekommen dadurch alle Gruppenmitglieder Leserechte an einer
Annotation.

#### Annotationen einer Gruppe betrachten

Es können die Annotationen einer Gruppe über den *Annotation
Manager* betrachtet werden.

## Grenzen und weitere Ideen

Da die Annotationen auf Grundlage der HTML-Datei und der Position des
darin markierten Textes erfasst wird, führen inhaltliche Änderungen auf
dynamische Seiten oder Änderungen des Layout zu falsch platzierten
Ausgaben. Zu empfehlen ist die Anwendung daher bei persistenten Inhalten
oder eigenen Seiten, die entsprechend stabil gehalten werden.

Daher ist es auch denkbar, die Annotation von der Visualisierungsschicht
zu lösen. Man würde bei TEI-Dateien etwa keine Transformation anwenden,
sondern dem Browser die Möglichkeit geben, [TEI nativ darzustellen](https://tei2016app.acdh.oeaw.ac.at/pages/show.html?document=CaylessViglianti.xml&directory=editions&stylesheet=editions).
Damit würde man direkt TEI annotieren und ein Standoff-Markup erzeugen.

Momentan werden die Informationen zur farblichen Gestaltung öffentlich
ausgegeben, d.h. für jeden Nutzer wäre die Anzahl an getätigten
Annotationen öffentlich sichtbar.
