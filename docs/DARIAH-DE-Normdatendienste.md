# DARIAH-DE Normdatendienste

last modified on Nov 29, 2021

## Thesaurus of Geographical Names (Getty)

### RESTful API

This DARIAH-DE's Getty service currently is using life data from
[getty.edu](http://getty.edu).

The API for DARIAH-DE's Getty service provides several possible usages:

- by providing the beginning of a place name
- by providing the exact place name
- a place's Getty Thesaurus ID

Queries are case insensitive.

The autocomplete and exact match modes return a custom XML by default,
but a JSON output can be retrieved by appending .json to the REST path
(confer examples for usage).

#### Autocomplete

| **API** | **Functionalities** | **Query (example)** | **Result (example)** |
| ------- | ------------------- | ------------------- | -------------------- |
| **/tgn/v1/starts-with(.json)?q=beginning-of-some-place-name**                           | Returns:Place namewhere the place is locatedGetty ID                                                                                 | <https://ref.de.dariah.eu/tgn/v1/starts-with?q=vagen>                  | <pre><code class="xml">&lt;term id="tgn:7091421"&gt;<br>  &lt;name&gt;Vagen&lt;/name&gt;<br>  &lt;path&gt;Bavaria \| Germany \| Europe \| World&lt;/path&gt;<br>&lt;/term&gt;<br>&lt;term id="tgn:7220304"&gt;<br>  &lt;name&gt;Vagenítion&lt;/name&gt;<br>  &lt;path&gt;Ioánnina \| Epirus \| Greece \| Europe \| World&lt;/path&gt;<br>&lt;/term&gt;</code></pre> |
| **/tgn/v1/starts-with(.json)?q=beginning-of-some-place-name&amp;georef=true-or-false**  | Returns:Place namewhere the place is locatedGetty IDGeo coordinates (longitude and latitude)                                         | https://ref.de.dariah.eu/tgn/v1/starts-with?q=vagen&amp;georef=true  | 
&lt;term id="tgn:7091421"&gt;
	&lt;name&gt;Vagen&lt;/name&gt;
	&lt;path&gt;Bavaria | Germany | Europe | World&lt;/path&gt;
	&lt;longitude&gt;11.866667&lt;/longitude&gt;
	&lt;latitude&gt;47.866667&lt;/latitude&gt;
&lt;/term&gt;
&lt;term id="tgn:7220304"&gt;
	&lt;name&gt;Vagenítion&lt;/name&gt;
	&lt;variant&gt;Vageníti&lt;/variant&gt;
	&lt;path&gt;Ioánnina | Epirus | Greece | Europe | World&lt;/path&gt;
	&lt;longitude&gt;20.712778&lt;/longitude&gt;
	&lt;latitude&gt;39.645278&lt;/latitude&gt;
&lt;/term&gt;
                                                                                                                                                                                                                                                                                                                                                    |
| **/tgn/v1/starts-with(.json)?q=beginning-of-some-place-name&amp;details=true-or-false** | Returns:Place namewhere the place is locatedGetty IDGeo coordinates (longitude and latitude)link to RDF data set of the place in TNG | <https://ref.de.dariah.eu/tgn/v1/starts-with?q=vagen&amp;details=true> |
&lt;term id="tgn:7220304"&gt;
	&lt;match_name&gt;Vagenítion&lt;/match_name&gt;
	&lt;preferred_name&gt;Vagenítion&lt;/preferred_name&gt;
	&lt;variant&gt;Vageníti&lt;/variant&gt;
	&lt;path&gt;Ioánnina | Epirus | Greece | Europe | World&lt;/path&gt;
	&lt;link target="http://textgridlab.org/tgnsearch/tgnquery.xql?id=7220304"/&gt;
	&lt;rdf-link target="http://vocab.getty.edu/tgn/7220304.rdf"/&gt;
&lt;/term&gt;
&lt;term id="tgn:7091421"&gt;
	&lt;match_name&gt;Vagen&lt;/match_name&gt;
	&lt;preferred_name&gt;Vagen&lt;/preferred_name&gt;
	&lt;path&gt;Bavaria | Germany | Europe | World&lt;/path&gt;
	&lt;longitude&gt;11.866667&lt;/longitude&gt;
	&lt;latitude&gt;47.866667&lt;/latitude&gt;
	&lt;link target="http://textgridlab.org/tgnsearch/tgnquery.xql?id=7091421"/&gt;
	&lt;rdf-link target="http://vocab.getty.edu/tgn/7091421.rdf"/&gt;
&lt;/term&gt;
 |

#### Exact matches

| **API**                                                                     | **Functionalities**                                                                                                                  | **Query (example)**                                                       | **Result (example)**                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **/tgn/v1/exact-match(.json)?q=exact-place-name**                           | Returns:Place namewhere the place is locatedGetty ID                                                                                 | https://ref.de.dariah.eu/tgn/v1/exact-match?q=scharzfeld                  | 
&lt;term id="tgn:1041079"&gt;
	&lt;name&gt;Scharzfeld&lt;/name&gt;
	&lt;path&gt;Lower Saxony | Germany | Europe | World&lt;/path&gt;
&lt;/term&gt;
                                                                                                                                                                                                                                                                                                                    |
| **/tgn/v1/exact-match(.json)?q=exact-place-name&amp;georef=true-or-false**  | Returns:Place namewhere the place is locatedGetty IDGeo coordinates (longitude and latitude)                                         | <https://ref.de.dariah.eu/tgn/v1/exact-match?q=scharzfeld&amp;georef=true>  |
&lt;term id="tgn:1041079"&gt;
	&lt;name&gt;Scharzfeld&lt;/name&gt;
	&lt;path&gt;Lower Saxony | Germany | Europe | World&lt;/path&gt;
	&lt;longitude&gt;10.383333&lt;/longitude&gt;
	&lt;latitude&gt;51.633333&lt;/latitude&gt;
&lt;/term&gt;
                                                                                                                                                                                                                          |
| **/tgn/v1/exact-match(.json)?q=exact-place-name&amp;details=true-or-false** | Returns:Place namewhere the place is locatedGetty IDGeo coordinates (longitude and latitude)link to RDF data set of the place in TNG | <https://ref.de.dariah.eu/tgn/v1/exact-match?q=scharzfeld&amp;details=true> |
&lt;term id="tgn:1041079"&gt;
	&lt;match_name&gt;Scharzfeld&lt;/match_name&gt;
	&lt;preferred_name&gt;Scharzfeld&lt;/preferred_name&gt;
	&lt;path&gt;Lower Saxony | Germany | Europe | World&lt;/path&gt;
	&lt;longitude&gt;10.383333&lt;/longitude&gt;
	&lt;latitude&gt;51.633333&lt;/latitude&gt;
	&lt;link target="http://textgridlab.org/tgnsearch/tgnquery.xql?id=1041079"/&gt;
	&lt;rdf-link target="http://vocab.getty.edu/tgn/1041079.rdf"/&gt;
&lt;/term&gt;
 |

#### Result by Getty ID

| **API**                     | **Functionalities**                                                        | **Query (example)**                                                                  |
|-----------------------------|----------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| **/tgn/v1/getty-id(.rdf)**  | Returns:the full RDF data set at <http://vocab.getty.edu/tgn/getty-id.rdf>   | <https://ref.de.dariah.eu/tgn/v1/7003670orhttps://ref.de.dariah.eu/tgn/v1/7003670.rdf> |
| **/tgn/v1/getty-id.json**   | Returns:the full JSON data set of <http://vocab.getty.edu/tgn/getty-id>      | <https://ref.de.dariah.eu/tgn/v1/7003670.json>                                         |
| **/tgn/v1/getty-id.jsonld** | Returns:the full JSONLD data set of <http://vocab.getty.edu/tgn/getty-id>    | <https://ref.de.dariah.eu//tgn/v1/7003670.jsonld>                                      |
| **/tgn/v1/getty-id.n3**     | Returns:the full N3/Turtle data set of <http://vocab.getty.edu/tgn/getty-id> | <https://ref.de.dariah.eu/tgn/v1/7003670.n3>                                           |
| **/tgn/v1/getty-id.nt**     | Returns:the full N-Triples data set of <http://vocab.getty.edu/tgn/getty-id> | <https://ref.de.dariah.eu/tgn/v1/7003670>.nt>                                           |

### RESTful API (deprecated)

DARIAH-DE's Getty service currently runs on a (n relatively old) TGN
dump in an eXist-db.

**ac** – autocompletion

**?ac=xxx** – return every hit starting with xxx

[https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?ac=Bam](https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?ac=Bam)

**ln** – long

**?ln=xxx** – return more detailed information on place xxx

[https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?ln=berlin](https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?ln=berlin)

**id** – identifier

**?id=xxx** – return everything about place with id xxx

[https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?id=7004325](https://ref.de.dariah.eu/tgnsearch/tgnquery.xql?id=7004325)

## Gemeinsame Normdatei (GND) 

Live requests from current DNB GND via SRU, **persons only**

### RESTful Parameters

**ac** – autocompletion

**?ac=xxx** – return every hit starting with xxx

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?ac=Ringel](https://ref.de.dariah.eu/pndsearch/pndquery.xql?ac=Ringel)

**ln** – long

**?ln=xxx** – return more detailed information on person xxx

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=Ringeln](https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=Ringeln)

**id** – identifier

**?id=xxx** – return everything about person with id xxx

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?id=118601121](https://ref.de.dariah.eu/pndsearch/pndquery.xql?id=118601121)

It is possible to retrieve the whole dataset in rdf from
[https://d-nb.info/gnd/118601121/about](https://d-nb.info/gnd/118601121/about)

### Advanced Usage (passing SRU parameters)

You also can use DNB parameters, as stated in
[https://services.dnb.de/sru/dnb?operation=explain&version=1.1](https://services.dnb.de/sru/dnb?operation=explain&version=1.1)
for they are piped through, such as "dnb.sw" or "dnb.geo".

Search for names

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler](https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler)

Search for professions

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler%20and%20dnb.sw=Soziologe](https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler%20and%20dnb.sw=Soziologe)

Search for refined names:

[https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler%20and%20dnb.sw=Soziologe%20and%20dnb.geo=Heidelberg](https://ref.de.dariah.eu/pndsearch/pndquery.xql?ln=W%C3%B6hler%20and%20dnb.sw=Soziologe%20and%20dnb.geo=Heidelberg)

You can also get RDF or JSON responses using requests such as

[https://ref.de.dariah.eu/pndsearch/rdf?ln=W%C3%B6hler](https://ref.de.dariah.eu//pndsearch/rdf?ln=W%C3%B6hler)

[https://ref.de.dariah.eu/pndsearch/json?ln=W%C3%B6hler](https://ref.de.dariah.eu//pndsearch/json?ln=W%C3%B6hler)

## OpenAPI Documentation and SwaggerUI

is also available:
[https://ref.de.dariah.eu/doc/api/index.html](https://ref.de.dariah.eu/doc/api/index.html)
