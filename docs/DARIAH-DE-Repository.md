# DARIAH-DE Repository

last modified on Aug 11, 2023

The [DARIAH-DE Repository](https://de.dariah.eu/repository) is a central
component of the DARIAH-DE Research Data Federation Architecture. It
aggregates various services and applications which makes its use very
convenient. The repository allows for storing research data sustainably
and securely, providing them with metadata, and finding them through the
[Repository Search](https://repository.de.dariah.eu) as well as through
the [DARIAH-DE Generic Search](https://search.de.dariah.eu).

## [Organisational Infrastructure](https://repository.de.dariah.eu/doc/services/organizational-infrastructure.html)

...documents mission, organisational infrastructure, and long-term
preservation issues as well as the integration of the designated
community of the DARIAH-DE Repository.

## [Data Policies](https://repository.de.dariah.eu/doc/services/data-policies.html)

...deals with collection development and preservation policies, data
quality and re-use, and ethical and disciplinary norms.

## [Digital Object Management](https://repository.de.dariah.eu/doc/services/digital-object-management.html)

...gives an overview about the main technologies and procedures used for
an appropriate data management. Import and publish workflows are
descrribed and related to the repository architecture of the DARIAH-DE
Repository.

## Technical and API Documentation

Under [http://repository.de.dariah.eu/doc/](http://repository.de.dariah.eu/doc/) you can find the DARIAH-DE Repository technical and API documentation that is maintained with each service's source code.
