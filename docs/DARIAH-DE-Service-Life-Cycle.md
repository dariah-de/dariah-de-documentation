# DARIAH-DE Service Life Cycle

last modified on Sep 02, 2019

[![Zur DARIAH-DE Website](attachments/44771487/52729306.png "Zur DARIAH-DE Website")](https://de.dariah.eu)

This page highlights the integration of new or existing services
(softwares) into the DARIAH-DE research infrastructure. The DARIAH-DE
Service Life Cycle has set up the steps in the integration process,
which a new or existing service needs to follow to integrate it in the
DARIAH-DE research infrastructure.

*You might also want to check out DARIAH-DE's [Working Papers Nr. 11](http://webdoc.sub.gwdg.de/pub/mon/dariah-de/dwp-2015-11.pdf)* on
Discussion and Definition of a Research Data LifeCycle (only available
for DARIAH-DE and written in German) or *[Report 3.2.7](attachments/14651583/44827074.pdf), which is the basis of this page and it also contains a detailed description of the criteria of the states.*

## States in the Development of a New Service

During the development, a service will pass through a chain of stages
determining the service life cycle, see Figure 1: DARIAH-DE Service Life
Cycle States. A (formal) decision is required to transfer a service from
one state to another. These states are:

- PROPOSAL State
- DEVELOPMENT State
- TESTING State
- HANDOVER State
- PRODUCTION State
- GOOD IDEA'S ARCHIVE  
    ![DARIAH-DE Service Life Cycle States](attachments/44771487/49323431.png "DARIAH-DE Service Life Cycle States")

Figure 1: DARIAH-DE Service Life Cycle States

## PROPOSAL State

The Service is described and proposed in the PROPOSAL State. The
proposal will contain the following information:

- Proposal including affiliation and contact information (mandatory)
  - Title, partner, coordinator, DARIAH-DE contact
        person (mandatory)
  - Information about the project, if available (nice if available)
- Relation to DARIAH (mandatory)
  - DARIAH-DE is not an exclusive resources provider and cooperates
        with external research projects.
  - Cooperation and collaboration is the focal point and the
        objective is, that the results, research data, tools or services
        obtained from the research projects can be used sustainably and
        be used by the professional communities for a long period of
        time.
- At least one of the criteria should be met: Teaching, research,
    research data, technical infrastructure (mandatory)
  - The proposal is to be located in the arts and humanities
        (mandatory)
  - Fill in the templates with: disciplines, objectives, state of
        research, methods (mandatory)
- Description of the proposed service (mandatory)
  - Scientific disciplinal description of the proposed
        service (mandatory)
  - Is there a description of the function for the service, e.g.
        user manual? (nice if available)
  - Target audience and expected size of the target
        audience (mandatory)
  - Is technical information of the service, such as operating
        system, programming language and used libraries, already known?
    - If yes: detailed technical description (nice if available)
- Estimation of efforts and costs for development and
    operation (mandatory)
  - Does external funding for development, integration, and
        operation exist? (nice if available)
  - Which DARIAH resources will be required including estimation of
        efforts/costs for development, integration and operation? (nice
        if available)
  - Fill in the templates with: resources (provided by local
        data/compute center), select an offer from the selection (menu
        card), reusability (mandatory)
  - Dependencies on other services (nice if available)

Two DARIAH-DE Mentors will be assigned by the AG SLC to prepare the
decision, if the service may be integrated into the DARIAH-DE
infrastructure. The Mentors will escort the service during its life
cycle until it reaches either the PRODUCTION State or is being
terminated, i.e. moved to the “Good Idea’s Archive” State.

A decision by the Executive Board is required, whether the service is
worth being developed with support of the DARIAH infrastructure (→
DEVEPLOMENT) or not (→ “Good Idea’s Archive”).  AG SLC will prepare the
recommendation.

## DEVELOPMENT State

In the DEVELOPMENT State the service is iteratively being developed,
deployed locally in the development environment and tested. The Mentors
support the development process and are responsible for using and
integration of existing DARIAH-DE services.

Required documentation:

- Manual
- Fact Sheet
- User Guide
- Checklist for decommissioning the operation of the service
    containing
  - Criteria for decommissioning, e.g. low usage
  - Dependencies and other information which needs to be considered 

After meeting all requirements, the mentors decide the transfer to the
next state: TESTING State.

## TESTING State

During the TESTING State the service has to be deployed on the DARIAH-DE
Platform and the service is beta-tested by DARIAH communities, scholars
and researchers. After successful tests the Mentors may decide the
transfer to the HANDOVER State or, in case of required revisions, back
to the DEVELOPMENT State.

## HANDOVER State

In the HANDOVER State a quality assurance including the
information/requirements of the “hosting factsheet” is performed. The
service, the software and its documentation is reviewed by the
"DARIAH-DE Service Hosting Team", e.g. DeISU. If OK the service will be
recommended as a DARIAH-DE hosted software service to be deployed on
the Software Hosting Services.

The Executive Board will decide if the service will go into the
PRODUCTION State if recommended by the AG SLC. Otherwise the service
will go back into the DEVEPLOMENT State or further into the “Good Idea’s
Archive” State.

## End of a Service

Hand over following check list during the HANDOVER state

- When the service is not longer needed? (e.g. too little users)
- Which dependencies exist?
- At what expense, including cost estimation in cooperation with
    DelSU, can the service be archived and what is the process?

## PRODUCTION State

In the PRODUCTION State the service is deployed and hosted in the
Software Hosting Services and maintained by DARIAH, the "DARIAH Service
Hosting Team", e.g. DeISU. DARIAH is responsible for the service and its
marketing. Users use the service according to the DARIAH "Terms of
Use".

During maintenance the service may be further developed and transferred
back to the DEVELOPMENT State. The development team may be inside the
DARIAH consortium or the original developer team, if available.

## “GOOD IDEA'S Archive”

“Good Idea’s Archive” is a reformulation of the former state. Some of
the service ideas proposed might be used in further proposals.

Sustainability aspects

- Archiving by the way of bit preservation, possible fail-safe data
    storage and distributed backups
- Archiving of data and data-ecosystem

## Notes

### Decision Bodies

To transfer a service from one state to another a (formal) decision is
required.

### The Mentors

DARIAH Mentors will be selected and assigned by the AG SLC to prepare
the decision, if the service may be integrated into the DARIAH
infrastructure. They will guide the service during its life cycle until
it reaches either the PRODUCTION State or is being terminated, i.e.
moved to the “Good Idea’s Archive State.

The mentor's job will be:

- **Proposal State**: technical mentor checks feasibility, A+H mentor
    surveys usefulness for the humanities community
- **Development State**: technical mentoring
- **Testing State**: A+H mentor connects A+H experts to test the
    service
- **Handover State**: technical mentoring
- **Production State**: technical mentoring

For each new service the following conditions should be met:

- at least one infrastructure expert
- at least one A+H expert

## Criteria of Service Life Cycle States

Criteria for each state of a new or existing service life cycle (status
of the Report 3.2.7) are listed as follows. A reference to recent
developments on this toic can be found in the last chapter. 

### SLC: PROPOSAL  

#### Contact Information

- Proposal including affiliation and contact information  
- Have the responsibilities and contact persons been named?  
- Fill in the templates with: title, partners, coordinator, DARIAH
    contact person,project initiators, duration, website

#### Correlation with DARIAH

- DARIAH-DE is not an exclusive resources provider and cooperates with
    external research projects.
- Cooperation and collaboration is the focal point and the objective
    is, that theresults, research data, tools or services obtained from
    the research projectscan be used sustainably and be used by the
    professional communities for along period of time.  
- At least one of the criteria should be met: Teaching, research,
    research data,technical infrastructure (mandatory)  
- The proposal is to be located in the arts and humanities (mandatory)    
- Fill in the templates with: disciplines, objectives, state of
    research, methods(mandatory)  

#### Service Description  

- Scientific disciplinal description of the proposed service
    (mandatory)  
- Is there a description of the function for the service, e.g. user
    manual? (nice ifavailable)  
- Target audience and expected size of the target audience (mandatory)
- Is technical information of the service, such as operating system,
    programminglanguage and used libraries, already known?  
  -   If yes: detailed technical description (nice if available)  

#### Cost Estimation

- Estimation of efforts/costs for development and operation  
- Fill in the templates with: resources (provided by local
    data/compute center),select an offer from the selection (menu card),
    reusability  
- The reusability and sustainability of the plugin should be
    described, even better if it is verified  
- Dependencies on other services  

### SLC: DEVELOPMENT  

#### License  

- An open source license and source code is unconditionally necessary
    for plugins. Otherwise a reuse can not be guaranteed.  
- License: Source code (and any data required to run the software
    release under an open source license  

#### Standards  

- Support for standard file formats  
- Availability of an API  
- Plugins or APIs should be developed standard compliant and be
    described  

#### End Devices

- Web based tool with adaptive user interface  
- Desktop tool with mobile application  
- Desktop tool with cross-­platform support  

#### Accessibility  

- Import/export functionality  
- Localization: support for multiple locales (e. g. German, English,
    French, etc.)
- An internationalization framework is used  

#### Scaling  

- Is the reproducibility documented?  

#### Security  

- Network requirements  
- Security requirements  

### SLC: TESTING

#### Documentation  

- Test or verification suite should be available and well documented  

#### Monitoring

- An endpoint for functional monitoring should be available  

#### Check for usability criteria  

- Collection of the usability criteria: 1. TGIII R 2.2.2  
- See DARIAH II report: R 1.2.2  
- See DARIAH II report: R 1.2.3  

### SLC: HANDOVER  

The following documentation is needed at the end of the HANDOVER State.

#### Description of the service  

- Tasks, features, functions 
- Any reference installations 
- Expected number of users, expected number of instances  
- Institution/contact persons  

#### Documents for the end users  

- Include contact persons for the support  

#### Documents for the service administration  

- Specifications of run time environment and resources  
  - Software: operating system, required libraries and dependencies
        of the  service  
  - Hardware: Required resources: CPU, RAM, HDD, network (volume of 
        data transfer)
  - Dependencies on DARIAH-­DE services  
- Description of deployment process  
  - Step-­by-­Step guideline  
  - Example configurations, FAQ, etc.  
  - Description of operation functionalities: Include mechanism in
        case of reboot. The service should have the ability to restart
        in an automatic way and be functional.  
  - Indication of possible limitations or problems  
- Monitoring requirements: an endpoint for functional monitoring
    should be available.  
- Include contact person of administration support  

#### Any documentation for further development  

- Include contact person  

Besides the documentation to be delivered, the contract of service
provision or clarifying of sustainable operation or the funding should
be done with DeISU.  

### SLC: PRODUCTION  

The following documents are needed in the PRODUCTION State: 

- Documentation for the end users  
- Documentation for the developers  
- Documentation for the administrators  
- Documentation for the support/HelpDesk  
- Tutorials, FAQs  
- Application examples from the projects  

Besides, clarifying of responsibility, maintaining and updating of these
documents should also be documented.

## Further Development of the DARIAH-DE Service Life Cycle Specifications

The DARIAH-DE Service Life Cycle specification above represent the
status of Report 3.2.7 at the end of the second phase of DARIAH-DE.
These specifications are due to further refinements during the current
phase, which are currently reflected in a preparation document at
**DARIAH 3 Service Life Cycle (inkl. Kriterien + Mentorenaufgaben)**
that is currently only available for DARIAH-DE and in parts written in
German. Here also a couple of check lists (again currently only
available for DARIAH-DE and written in German), filled in for concrete
DARIAH-DE services, are included. A template of such a check list can
also be found at **Vorlage Checkliste DARIAH 3 Service Life Cycle**.
