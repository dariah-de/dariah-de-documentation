# DARIAH Self Service Documentation

last modified on Feb 01, 2021

The Self Service enables users and potential users to trigger requests
without having to contact the DARIAH support first. This page documents
and explains functions of the Self Service portal.

Self Service is available to registered as well as non-registered users.

## Non-Registered User

A non-registered user has the options to:

a\) Request an account
b\) Request a password reset
c\) Login to the system

### a) Account Request

Requesting an account here will trigger a ticket in OTRS and an entry in
the account request table for DARIAH admins.

Initial group can and is often left blank, the comment text box can be
used to specify further group membership requests.

It is important to use an institutional email address (if not, the
system will display a warning) to prove affiliation with the DARIAH
project.

![](attachments/44765170/44827019.png)

If an e-mail address is used that is either not affiliated with an
institution, or the affiliated institution is not yet known to the
system, the account won't be accepted immediately!

Instead, the DARIAH support will check manually, whether it does belong
to an institution:

a\) If it does, the account request is granted  
b) If it doesn't, the DARIAH support will find a solution with the user:

![](attachments/44765170/83233561.png)

### b) Password Reset

In case of a forgotten password, a user can request a rest link through
this function. The login is usually **FirstnameLastname**. If that is
unsuccessful, an admin can also manually trigger a password reset email.

![](attachments/44765170/44827020.png)

### c) Logging in

To access more functions, log in using your DARIAH or external
credentials. Push the "Login" button, and then select "DARIAH" for your
DARIAH account, or type (part of) your institution's name for your own
account.

![](attachments/44765170/44827021.png)

## Registered User

As a registered user, you can:

a\) View and modify your account
b\) Set a new password
c\) Manage and request group memberships

### a) Modify

Here you are given the option to review and edit some of your account
data. Note how some fields can not be empty and some can only be changed
by the Support Team.

![](attachments/44765170/44827022.png)

### b) Set a New Password

If a user still knows his password but wants to change it, he can do
that in the Self Service.

![](attachments/44765170/83233564.png)

### c) Manage Groups

German instruction as
PDF: [Gruppenmanagement-DARIAH-Self-Service.pdf](attachments/44765170/139460842.pdf)

This option lets you manage your group memberships and apply for new
ones which then have to be approved by the group owners.

If you wish to use other services or groups than those displayed in the
list, contact DARIAH support directly (register@dariah.eu).

![](attachments/44765170/83233570.png)

If you are the owner of a group, you can go to "Group Membership
Requests", look at the membership requests for your group and either
accept or deny them. 

![](attachments/44765170/83233572.png)

###  d) Manage Access Tokens for DARIAH Repository

This option lists the access tokens (comparable to secret keys) you have
issued to applications that are supposed to access your resources  the
DARIAH Repository. You can list them and delete individual tokens:

![](attachments/44765170/87982822.png)
