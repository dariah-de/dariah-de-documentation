# DARIAH User Administration Documentation

last modified on Feb 22, 2019

## Admin Portal

DARIAH's new admin portal offers improvements, but also brings changes
to established procedures. This document lists changes and documents
procedures required to successfully manage user accounts. Its focus will
be on the most common actions within the Admin Portal, such as account
and service requests.

### Login

The administration portal can be reached at
[https://auth.dariah.eu/cgi-bin/admin/ldapportal.pl](https://auth.dariah.eu/cgi-bin/admin/ldapportal.pl)

Login with your username and password and you will be redirected to the
landing page.

![](attachments/44765268/83233824.png) 

### Account Requests

As most account requests are triggered through the self service portal,
they will produce a ticket **and** an account request. In OTRS, those
tickets are recognizable by their email
[selfservice-no-reply@dariah.eu](mailto:selfservice-no-reply@dariah.eu).
To avoid doubling of accounts, it is important to always check Account
Requests before accepting them. You can accept or decline a request by
marking it and clicking on accept or decline. Confirm your action below.
Be aware that this will trigger an email in any case. Especially when
declining, give a reason for your decision in the text box.

![](attachments/44765268/44827008.png)

### New Account

Especially when an account is requested for a new colleague, it is
necessary to manually set it up via the New Account menu. To avoid
duplications, this always refers you back to the account search first.
Enter first and last name and click to search for already existing
accounts.

![](attachments/44765268/83233714.png)

If any accounts were found, you will have the option to either edit the
existing or to continue creating a new account.

![](attachments/44765268/44827011.png)

When creating a new account, please note that you can only choose *one*
initial group. However, especially when setting up accounts for research
groups you will often need more than one group. In this case you will
have to modify groups, as explained in the next section.

![](attachments/44765268/44827012.png)

You also have the option to activate or delete an account on a certain
date.

### Groups

Group membership is - other than the initial group, which is set when
creating an account - managed by selecting the group and adding members.
To do that, navigate to the Groups tab on your left.

![](attachments/44765268/44827013.png)

Search for the group you want to modify using the right hand text box
and click modify - be careful not to accidentally click delete.

![](attachments/44765268/44827014.png)

For a new user, click on add member, search for the user you would like
to add, select the user and add him/her to the group.

![](attachments/44765268/44827015.png)

To remove a user, search for the group and click the red minus button
next to the user's name to remove him/her from the group.

### Search and Complex Search

If you wish to completely delete an account, use Search or Complex
Search (includes login) to find the account. Select the desired option
from the drop down menu and click to to execute. You can also select
multiple entries.

![](attachments/44765268/44827016.png)

### Password Reset

When a user requests a password reset, use the search to find his/her
account. Then click new password to have the user receive an email with
a link to set a new password.

![](attachments/44765268/44827018.png)

### Workshop Accounts

If a project owner requests a certain amount of accounts for a workshop,
they can be created under "Workshop Accounts". It is important to set
automatic deletion data (ca. one week after the workshop, if no other
date is requested by the project owner) and ask for the workshop's data
(title,

organisation, quantity of accounts, e-mail address of the person in
charge).

![](attachments/44765268/83233830.png)

### Group Membership requests

Group Membership Requests are triggered by users requesting a group
membership via the self service portal. Access Group Membership requests
by clicking on the respective menu option.

Similarly to account requests, service requests can be approved or
denied, leading to a notification and group membership for the user.

![](attachments/44765268/83233721.png)

### Statistics

Here you can consult the LDAP statistics to check the overall number of
users, groups, federation people and organisations. You can also check
how many members each individual group has.

![](attachments/44765268/87982138.png)

### Logout

You can log out using the left menu. Remember to log out once you are
done working on LDAP data.

## User Identification

All services in the field of research, in particular the services in the
DFN-AAI have to be reserved for a „closed user group“ as the statute
requires. As value-added services, offered to the entire population,
they could be in direct competition with commercial offerings. For this
reason it is is of crucial importance that the services provided by
DARIAH are only accessible for people involved in the research
community.

Only for this reason we require a user authentication before using
services. Therefore, it is also necessary to identify the users and make
sure that they belong to the research community before they get an
account.

### DARIAH User Account Types

In general, we can differentiate 3 types of users:

1. Users who authenticate within the scope of the DFN-AAI (or other
    research networks of federations) using their home account. This
    kind of users are members of a research institution and therefore
    are definitely part of the research community. We can expect that
    the institution has securely identified the person.

2. Users who have or would like to receive an account from DARIAH, and
    requested the account via an e-mail address of a research
    institution. These kinds of account requests can also be
    automatically added to the research community. We can expect that
    the institution has securely identified the person. The assignment
    to the second user group occurs semi-automatically:

    1. A *Whitelist* of university mail domains is maintained in the
        LDAP. Users who use mail addresses from these domains will be
        automatically assigned to the group; corresponding account
        applications will be automatically approved.  

    2. A *Blacklist* of free-mail providers is also configured in LDAP.
        Users who use mail addresses from these domains will be referred
        to their assignment to group 3 and their related consequences
        when registering.  

    3. Manual decision in cases which fall into neither category a)
        or b) (“*Greylist*”). The users will be warned that they may be
        in group 3 and then refer to the necessary process. If the
        evidence is confirmed, the helpdesk carries out a manual check
        of the domain, whether it is assigned to the White- or
        Blacklist. The helpdesk carries out the assignment to the group
        2 or 3 accordingly and extends the White- or Blacklist.  

        ![](attachments/44765268/87982143.png)

3.  Users who apply for a DARIAH account via a non-academic e-mail
    address (eg. [xxx@web.de](mailto:xxx@web.de)). In this case the
    account administration has to check whether it is a person belonging
    to a research community. We can not assume that the provider has
    securely identified the person as a researcher.

### Validation of User Accounts

In case a user departs from his institution or if the personal data of a
user change, it results in orphaned or outdated accounts. At best, the
users themselves report these changes. In several productive federations
such as the DFN-AAI, certain requirements are placed on the actuality of
user accounts. Therefore, appropriate measures have to be implemented to
ensure that the accounts included in the DARIAH LDAP comply with these
requirements, e.g. by sending control mails to stored addresses.

The conditions and steps for a deactivation, reactivation and deletion
of accounts are outlined in the current part.

- For user groups 1 and 2 (see above), the control mail will be sent
    every 12 months. Only if this results in a mail server response like
    „user unknown“ or something similar, DARIAH will react.
  - The account gets the flag dariahDeactivated=TRUE
  - These accounts can‘t log in to the IdP either directly on using
        the LDAP (User Groups 2 +3)
  - Also no attributes queries of SPs will be answered by the IdP
        (user group 1) for accounts like these
- For user group 3 the mail will be sent every 90 days (according to
    DFN-AAI Basic requirements). The users have to reply to the mail
    actively, i.e. by clicking a link to confirm that they are still
    involved in the research process. If there is no response within 30
    days, the account will be also deactivated and the consequences are
    the same as for user group 1 and 2.

