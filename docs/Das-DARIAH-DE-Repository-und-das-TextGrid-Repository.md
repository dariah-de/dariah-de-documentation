# Das DARIAH-DE Repository und das TextGrid Repository

last modified on Aug 11, 2023

Mit dem DARIAH-DE Repository und dem TextGrid Repository steht Geistes- und Kulturwissenschaftler:innen ein umfassendes Angebot zur Verfügung, um ihre Forschungsdaten dauerhaft, nachnutzbar und referenzierbar zu speichern. Während das TextGrid Repository für XML/TEI-Formate sowie das editorische Arbeiten und die Publikation aus dem TextGrid Laboratory[^1] optimiert ist, unterstützt das DARIAH-DE Repository[^2] auch beliebige XML-fremde Datenformate und bietet einen Publikationsprozess über das DARIAH-DE Portal.

Beide Repositorien basieren auf dem gleichen technologischen Framework, bieten jedoch – je nach Datenformat und Repository – unterschiedliche Modelle der Datenpräsentation und -kuration sowie Schnittstellen, um eine spezifische Nachnutzung ermöglichen zu können, und beide Repositorien adressieren Wissenschaftler*innen an Universitäten und Forschungseinrichtungen, die in Forschungsprojekten entstandene Forschungsdaten langfristig im Rahmen einer Repository-Lösung speichern wollen.

## Die Infrastruktur

Die Infrastruktur für das TextGrid Repository und das DARIAH-DE Repository wird von DARIAH-DE zur Verfügung gestellt und basiert auf Tools und Diensten des DARIAH-DE Developer-Portals und dem Quellcode von TextGrid und DARIAH-DE Kernkomponenten[^3]. So werden zum Beispiel für die Entwicklung von Diensten beider Repositorys viele Tools für die Continuous Integration (GIT, Gitlab CI, Nexus, Puppet, Docker, Kybernetes, etc.), wie auch das Monitoring der verschiedenen Dienste, deren Dokumentation im Wiki hinterlegt ist. Für die Projektverwaltung und das Bugtracking werden ebenfalls Angebote von
DARIAH-DE genutzt.

Das Speicherbackend beider Repositorien besteht jeweils aus einem nicht-öffentlichen, zugriffsgeschützten und einem öffentlichen Bereich (OwnStorage/PublicStorage). Alle Dienste und Daten sind über die DARIAH Authentifizierungs- und Autorisierungs-Infrastruktur (AAI) abgesichert, so dass bestimmt und kontrolliert werden kann, wer welche Zugriffsrechte hat. Die Authentifizierung erfolgt via Shibboleth (eduGAIN), die Autorisierung via OpenLDAP/OpenRBAC. Die öffentlichen Daten beider Repositorien sind in der DARIAH-DE Collection Registry[^4] verzeichnet und über die Generische Suche[^5] auffindbar. Als zusätzliche Features stellt TextGrid eine Volltextsuche und eine rollenbasierte Autorisierung im OwnStorage über OpenRBAC bereit.

### TextGrid Repository

Die Daten im TextGrid Repository werden mit einem spezifischen TextGrid-Metadatenschema[^6] beschrieben. Der nicht-öffentliche Speicherbereich (OwnStorage) wird über ein Rollen- und Rechtemanagement administriert, der öffentlich zugängliche Speicherbereich (PublicStorage) enthält sämtliche publizierte Daten. Jede hier gespeicherte Datei erhält zu ihren dauerhaften Referenzierung einen Persistenten Identifikator (ePIC PID[^7]). Alle öffentlichen Daten des TextGrid Repositorys sind über die Webseite des TextGrid Repository[^8], über die TG-search API[^9] sowie in der DARIAH-DE Generischen Suche recherchierbar und mithilfe verschiedener Tools – wie zum Beispiel Voyant – visualisierbar.

Zentraler Bereich der gemeinsamen Weiterentwicklung von TextGrid[^10] und DARIAH-DE[^11] ist das TextGrid Repository, das als geisteswissenschaftliches
Forschungsdatenarchiv im Projekt TextGrid entwickelt wurde und nun von DARIAH-DE bereitgestellt wird. XML/TEI-Forschungsdaten können im TextGrid Laboratory als TextGrid-Objekte[^12] gemeinsam bearbeitet, ausgezeichnet, angereichert und schließlich als Kollektion oder Edition publiziert werden. Auch ein direktes Publizieren von Daten im TextGrid Repository ist möglich – ohne die Nutzung des TextGrid Laboratory –, und erlaubt ebenso eine dauerhafte und referenzierbare Speicherung sämtlicher Daten in unterschiedlichen Formaten, wobei insbesondere die Präsentation von XML/TEI unterstützt wird.

### DARIAH-DE Repository

Das DARIAH-DE Repository ermöglicht die dauerhafte und referenzierbare Speicherung von Forschungsdaten mithilfe standardisierter deskriptiver Metadaten. Ein Basis-Satz von Metadaten des Dublin Core Standards DC-Simple[^13] ist ausreichend, um Forschungsdatenobjekte zu beschreiben und in das Repository einzuspielen. Wie auch im TextGrid Repository können Gruppen von Objekten als Kollektion zusammengefasst werden. Über eine grafische Oberfläche im DARIAH-DE Portal – den Publikator[^14] – ist es möglich, Kollektionen zu
definieren, zu beschreiben und diese samt ihrer zugehörigen Dateien mit Metadaten auszuzeichnen. Anschließend kann die Kollektion in das DARIAH-DE Repository eingespielt werden. Hier erhält jedes Objekt einen DOI[^15] als Persistenten Identifikator und wird langfristig im Repository gespeichert. 
Die Kollektion wird in der Repository Suche[^16] nachgewiesen, und wenn die Kollektion in der DARIAH-DE Collection Registry registriert wurde, werden alle zu dieser Kollektion gehörigen Objekte auch in der DARIAH-DE Generischen Suche nachgewiesen.


[^1]: Download TextGrid Laboratory. <https://textgrid.de/download>
[^2]: DARIAH-DE Repository. <https://de.dariah.eu/repository>
[^3]: TextGrid – Übersicht. <https://gitlab.gwdg.de/dariah-de/textgridlab>
[^4]: DARIAH-DE Collection Registry. <https://colreg.de.dariah.eu>
[^5]: DARIAH-DE Generic Search. <https://search.de.dariah.eu>
[^6]: TextGrid Metadaten-Schema. <https://textgrid.info/namespaces/metadata/core/2010>
[^7]: ePIC – Persistent Identifiers for eResearch. <http://www.pidconsortium.eu>
[^8]: Suche – TextGridRep. <https://textgridrep.org>
[^9]: TG-search – TextGrid Repository Documentation. <https://textgridlab.org/doc/services/submodules/tg-search/docs/index.html>
[^10]: TextGrid: Digital edieren – forschen – archivieren. <https://textgrid.de>
[^11]: DARIAH-DE. Digitale Forschungsinfrastruktur für die Geistes- und Kulturwissenschaften. <https://de.dariah.eu>
[^12]: TextGrid Nutzerdokumentation – TextGrid-Objekte. <https://doc.textgrid.de/TextGrid-Objekte/>
[^13]: Dublin Core Metadata Element Set, Version 1.1. <http://www.dublincore.org/documents/dces>
[^14]: DARIAH-DE Publikator. <https://repository.de.dariah.eu/publikator>
[^15]: DataCite DOI. <https://www.datacite.org>
[^16]: DARIAH-DE Repository Search. <https://repository.de.dariah.eu/search>
