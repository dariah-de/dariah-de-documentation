# Das DARIAH-DE Repository

last modified on Mär 08, 2019

**Der User Guide zum Publikator und zum DARIAH-DE Repository ist
umgezogen!** Bitte schauen Sie auf der **[Dokumentations-Seite des DARIAH-DE Repositorys](https://repository.de.dariah.eu/doc)** bzw.
direkt auf den neuen [**User Guide zum Publikator**](https://repository.de.dariah.eu/doc/services/submodules/publikator/docs/index-de.html)!
