# Datasheet Editor Dokumentation

last modified on Mai 06, 2019

Die Dokumentation des Datasheet Editors finden SIe unter: **[https://geobrowser.de.dariah.eu/doc/de/datasheet.html](https://geobrowser.de.dariah.eu/doc/de/datasheet.html)**.

Die FAQ-Seite mit häufig gestellten Fragen zum Datasheet Editor finden Sie nun unter: **[https://geobrowser.de.dariah.eu/doc/de/datasheet-faq.html](https://geobrowser.de.dariah.eu/doc/de/datasheet-faq.html)**.
