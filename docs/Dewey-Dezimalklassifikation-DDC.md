# Dewey-Dezimalklassifikation (DDC)

last modified on Jan 28, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

Die **[Dewey-Dezimalklassifikation (DDC)](http://www.ddc-deutsch.de/Subsites/ddcdeutsch/DE/Home/home_node.html)** ist
eine Universalklassifikation, die mittlerweile von OCLC (Online Computer
Library Center) weiterentwickelt wird. Es gibt Übersetzungen in mehr als
30 Sprachen. DDC wird vor allem von Bibliotheken genutzt, ist aber auch
bei anderen Einrichtungen weit verbreitet und international
gebräuchlich. Diese Universalklassifikation eignet sich aufgrund ihres
Verbreitungsgrades, um auf einer weniger feingranularen Ebene über
heterogene Bestände eine multilinguale Interoperabilität der Metadaten
zu schaffen. Seit 2003 gibt es eine deutsche Übersetzung, die in einem
DFG-geförderten Projekt 2002–2005 von der FH Köln realisiert wurde. Die
zehn Hauptfacetten orientieren sich am Kanon der Wissenschaften:

- Informatik, Informationswissenschaft, allgemeine Werke
- Philosophie & Psychologie
- Religion
- Sozialwissenschaften
- Sprache
- Naturwissenschaften
- Technik, Medizin, angewandte Wissenschaften
- Künste und Unterhaltung
- Literatur
- Geschichte und Geografie
