# Digilib

last modified on Feb 06, 2019

Digilib steht als DARIAH-DE Service zur Verfügung und kann für die
Darstellung von Daten aus dem TextGrid und DARIAH-DE Repository genutzt
werden.

Technische Dokumentation finden Sie hier:

- TextGrid Repository:
    [https://textgridlab.org/doc/services/submodules/tg-digilib/docs_tgrep/index.html](https://textgridlab.org/doc/services/submodules/tg-digilib/docs_tgrep/index.html)
- DARIAH-DE Repository:
    [https://repository.de.dariah.eu/doc/services/submodules/tg-digilib/docs_dhrep/index.html](https://repository.de.dariah.eu/doc/services/submodules/tg-digilib/docs_dhrep/index.html)

...und allgemeinere Informationen dort:

- [http://iiif.io/api/image/1.1/](http://iiif.io/api/image/1.1/)
- [http://digilib.sourceforge.net/scaler-api.html](http://digilib.sourceforge.net/scaler-api.html)
