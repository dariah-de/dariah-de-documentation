# Empfehlungen für Forschungsdaten, Tools und Metadaten in der DARIAH-DE Infrastruktur

last modified on Apr 13, 2022

[![Zur DARIAH-DE Website](attachments/Empfehlungen-fuer-Forschungsdaten/159220083.png "Zur DARIAH-DE Website")](https://de.dariah.eu)  

## Grundsätzliches

Diese Seite soll einen Überblick schaffen über Forschungsdaten, Tools und Metadaten in der DARIAH-DE Infrastruktur. Sie richtet sich insbesondere an interessierte Geisteswissenschaftler\*innen.

Um Kollegen und kommenden Generationen die Beschäftigung mit Ihrer Forschung zu erleichtern, gelten folgende Grundsätze:

- Dokumentieren Sie ihre Arbeit.
- Verwenden Sie gängige Metadatenstandards.
- Verwenden Sie zur Ablage und Übergabe an externe Repositorien Standard-Dateiformate, die auch außerhalb ihrer Community in Gebrauch sind.
- Verwenden Sie nach Möglichkeit Normdaten und kontrollierte Vokabulare zur inhaltlichen Anreicherung.

Die bereitgestellten Informationen haben keinen Anspruch auf Vollständigkeit.

## Hinweis auf Gute Wissenschaftliche Praxis 

### Herstellung von öffentlichem Zugang zu Forschungsergebnissen

“Grundsätzlich bringen Wissenschaftlerinnen und Wissenschaftler alle
Ergebnisse in den wissenschaftlichen Diskurs ein. Im Einzelfall kann es
aber Gründe geben, Ergebnisse nicht öffentlich zugänglich (im engeren
Sinne in Form von Publikationen, aber auch im weiteren Sinne über andere
Kommunikationswege) zu machen; dabei darf diese Entscheidung nicht von
Dritten abhängen. Wissenschaftlerinnen und Wissenschaftler entscheiden
in eigener Verantwortung – unter Berücksichtigung der Gepflogenheiten
des betroffenen Fachgebiets –, ob, wie und wo sie ihre Ergebnisse
öffentlich zugänglich machen. Ist eine Entscheidung, Ergebnisse
öffentlich zugänglich zu machen, erfolgt, beschreiben
Wissenschaftlerinnen und Wissenschaftler diese vollständig und
nachvollziehbar. Dazu gehört es auch, soweit dies möglich und zumutbar
ist, die den Ergebnissen zugrunde liegenden Forschungsdaten, Materialien
und Informationen, die angewandten Methoden sowie die eingesetzte
Software verfügbar zu machen und Arbeitsabläufe umfänglich darzulegen.
Selbst programmierte Software wird unter Angabe des Quellcodes
öffentlich zugänglich gemacht. Eigene und fremde Vorarbeiten weisen
Wissenschaftlerinnen und Wissenschaftler vollständig und korrekt nach.”
(Kodex “Leitlinien zur Sicherung guter wissenschaftlicher Praxis”. DFG
2019, S. 18-19.)

## Basics

Allg. Informationen zu Leitlinien und Policies finden sich hier: [Forschungsdaten.info - Leitlinien und Policies](https://www.forschungsdaten.info/themen/ethik-und-gute-wissenschaftliche-praxis/leitlinien-und-policies/)

## Dateiformate für Langzeitarchivierung und Nachnutzung 

- Ein Kriterienkatalog zur besonderen Eignung von Dateiformaten zur Langzeitarchivierung findet sich zum Beispiel bei der [Library of Congress](http://www.digitalpreservation.gov/formats/sustain/sustain.shtml).
- Allg. Informationen zur LZA: [Forschungsdaten.info - Forschungsdaten langfristig erhalten](https://www.forschungsdaten.info/themen/veroeffentlichen-und-archivieren/langzeitarchivierung/)
- Allg. Informationen zu Formaten: [Forschungsdaten.info - Formate erhalten und Inhalte erhalten](https://www.forschungsdaten.info/themen/veroeffentlichen-und-archivieren/formate-erhalten/) 
    - Textformate
    - Tabellenformate
    - Statistische Umgebungen
    - Rastergraphikformate, Vektorgraphikformate, Multimediaformate
- Empfohlene Dateiformate für Langzeitarchivierung und Nachnutzung der [Library of Congress](https://www.loc.gov/preservation/resources/rfs/TOC.html)

## Metadatenstandards

- Allg. Informationen: [Forschungsdateninfo.de - Metadaten und Metadatenstandards](https://www.forschungsdaten.info/themen/beschreiben-und-dokumentieren/metadaten-und-metadatenstandards/)
- Liste von Metadatenstandards: [FAIRSharing.org](https://beta.fairsharing.org/)

## Tools und Verfahren für die digitalen Geisteswissenschaften

- [Digital Humanities Resources for project building](http://dhresourcesforprojectbuilding.pbworks.com/w/page/69244319/Digital%20Humanities%20Tools)

## Empfohlene Lizenzen

- Allgemeine Informationen: [Forschungsdaten.info - Open Data, Open Access und Nachnutzung](https://www.forschungsdaten.info/themen/finden-und-nachnutzen/open-data-open-access-und-nachnutzung/)
- Lizenzen Forschungsdatenmanagement der [HU Berlin](https://www.cms.hu-berlin.de/de/dl/dataman/teilen/rechtliche-aspekte/lizenzen)

## Datenmanagementpläne

Datenmanagementpläne nehmen v.a. bei der Antragstellung für Förderungen von Forschungsprojekten einen immer höheren Stellenwert ein.

- Allg. Informationen zu Datenmanagementplänen auf forschungsdaten.info: [Eine Wegbeschreibung für Daten](https://www.forschungsdaten.info/themen/informieren-und-planen/datenmanagementplan/)
- Die Göttinger [eResearch Alliance (eRA)](https://www.eresearch.uni-goettingen.de/de/) unterstützt Forschende mit Werkzeugen, Best-Practice-Schulungen und Beratung zum Forschungsdatenmanagement sowie den Angeboten der eRA.

Hier finden Sie eine Kurzübersicht von Tools, die bei der Erstellung eines Datenmanagementplans helfen können:

- [RDMO](https://rdmorganiser.github.io/) (Research Data Management Organiser): Institutionen und Forschende können hiermit das Forschungsdatenmanagement ihre Projekte strukturiert planen und durchführen.
- [GRO.plan](https://www.eresearch.uni-goettingen.de/de/services-and-software/gro-plan/): Dieses Tool der eRA (s.o.) unterstützt Forschende bei der Erstellung von Datenmanagementplänen mithilfe von Fragebögen. Forschende können zwischen unterschiedlichen Fragebögen für verschiedene Förderer und Disziplinen wählen. GRO.plan basiert auf RDMO.
- CLARIN-D: [Datenmanagementplan entwickeln](https://www.clarin-d.net/de/aufbereiten/datenmanagementplan-entwickeln): Auch ohne Login kann hier mithilfe einer Vorlage ein Datenmanagementplan entwickelt werden. **Hinweis**: Dies empfiehlt sich nur dann, wenn man seine Forschungsdaten in einem Repositorium von CLARIN ablegen möchte.

Grundsätzlich gilt, dass der Datenmanagementplan den Anforderungen der
Projektförderer entsprechen muss.
