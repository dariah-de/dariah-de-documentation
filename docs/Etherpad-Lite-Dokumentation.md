# Etherpad-Lite Dokumentation

last modified on Apr 30, 2015

## Startseite von Etherpad-Lite

'New Pad' legt ein neues Pad mit einem Zufallsnamen an. Im Textfeld in
der Mitte kann man entweder den Namen eines vorhandenen Pads zum Öffnen
eingeben, oder ein Neues mit einem bestimmten Namen anlegen. 'My Pads'
zeigt eine Liste aller Pads an, an denen der Benutzer mitgewirkt hat.
(Abbildung 1)

![](attachments/38080828/39747738.png)

Abbildung 1

## My Pads

Der Padname ist ein Link, der direkt zum Dokument führt. In dieser
Übersicht kann man Pads auch löschen. (Abbildung 2)

![](attachments/38080828/39747739.png)

Abbildung 2

## Editier-Fenster

Im Eingabefenster (Abbildung 3) hat der Autor im Menübereich links-oben
verschiedene Möglichkeiten den Text zu formatieren. Dazu zählen Fett-,
Kursivschrift sowie Aufzählungsarten. Über das Style-Dropdownmenü lassen
sich Überschrieftsebenen realisieren.

![](attachments/38080828/39747740.png)

Abbildung 3

## Import/Export

Auf der rechten Seite befinden sich mehrere Buttons zur Menüführung.
Über Import/Export (Abbildung 4) lassen sich Dokumente hochladen bzw. in
unterschiedlichen Formaten abspeichern. Unterstützt werden u.a. .txt,
.doc, .docx, .pdf, .odt, .html, .htm-Dateien. Der Import von
Binärdateien wie docx oder pdf wird nur teilweise unterstützt.
Formatierungen können verloren gehen. Diese Version von Etherpad-Lite
ermöglicht es, Dokumente in Markdown zu erstellen und zu exportieren.
Mit dieser Markup-Language können diverse Formatierungen und Fußnoten
erstellt werden. Hierzu muß ein Häckchen bei 'document is markdown'
gesetzt werden. Ein Beispiel, das die Möglichkeiten aufzeigt, befindet
sich weiter unten.

![](attachments/38080828/39747741.png)

Abbildung 4

## Timeslider

Über den Timeslider-Button können Texte zu einem bestimmten Zeitpunkt
wiederhergestellt werden. So lässt sich die Entwicklung eines Dokuments
nachvollziehen. Änderungen werden automatisch gespeichert. Zusätzlich
hat man die Möglichkeit über 'Save Revision' den aktuellen Stand des
Pads zu sichern.

## Settings

Über 'Settings' (Abbildung 5) kann man z.B. einstellen, ob das
Chatfenster sichtbar sein soll, ob Text von verschiedenen Autoren
farblich unterlegt sein soll oder Zeilennummern angezeigt werden. Über
'Set Pad Password' kann ein Pad mit einem Kennwort gegen unberechtigten
Zugriff geschützt werden. Das Setzen eines leeren Passworts entfernt
dieses wieder. Weiterhin können einfache Markdown-Elemente im Text
sichtbar gemacht werden.

![](attachments/38080828/39747742.png)

Abbildung 5

## Share

Über 'Share' (Abbildung 6) wird der direkte Link des Pads angezeigt.
'Embed URL' zeigt HTML-Code an, um ein Pad mittels iframe auf einer
Webseite sichtbar zu machen. Weiterhin besteht die Möglichkeit
Einladungen per Mail zu verschicken.

Die restlichen Knöpfe zeigen alle User an, die momentan an diesem Pad
arbeiten bzw. dienen der Navigation.

![](attachments/38080828/39747743.png)

Abbildung 6

## Überschriften

In Abbildung 7 ist zu sehen wie Überschriften in Markdown erstellt
werden können. Dabei wird der Text mit === unterstrichen.

![](attachments/38080828/39747863.png)

Abbildung 7

## Wie erstelle ich Fußnoten?

Dieses Beispiel zeigt wie mit Etherpad-Lite Fussnoten erstellt werden
können.

``` syntaxhighlighter-pre
Dies ist eine Fußnote [^1].

[^1]: Fußnotentext befindet sich hier.
```

Das Ergebnis als pdf-Dokument kann
[*hier*](https://dev2.dariah.eu/wiki/download/attachments/38080828/fussnote.pdf?api=v2)angesehen
werden. Ein umfangreiches Beispiel ist hier zu finden:
[*http://www.unexpected-vortices.com/sw/rippledoc/quick-markdown-example.html*](http://www.unexpected-vortices.com/sw/rippledoc/quick-markdown-example.html)
