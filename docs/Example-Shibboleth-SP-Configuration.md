# Example Shibboleth SP Configuration

last modified on Mär 12, 2020

These example configuration files should help you get started with
configuring a Shibboleth SP v2 or v3 to connect to the DARIAH AAI.

## Main configuration file

**/etc/shibboleth/shibboleth2.xml**

``` syntaxhighlighter-pre
<SPConfig xmlns="urn:mace:shibboleth:2.0:native:sp:config"
    xmlns:conf="urn:mace:shibboleth:2.0:native:sp:config"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"    
    xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
    clockSkew="180">
    
    <!-- change your SP's entityID here and make sure that eppn is set as REMOTE_USER -->
    <ApplicationDefaults entityID="https://your-domain.edu/shibboleth"
                         REMOTE_USER="eppn unique-id">

        <Sessions lifetime="28800" timeout="3600" relayState="ss:mem"
                  checkAddress="false" handlerSSL="false" cookieProps="http">
            
            <!-- All eduGAIN IdPs including the DARIAH homeless IdP are available through the AAI Proxy -->
            <SSO entityID="https://aaiproxy.de.dariah.eu/idp">
                SAML2
            </SSO>
 
            <Logout>SAML2 Local</Logout>

            <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

            <Handler type="Status" Location="/Status" acl="127.0.0.1 ::1"/>

            <Handler type="Session" Location="/Session" showAttributeValues="false"/>

            <Handler type="DiscoveryFeed" Location="/DiscoFeed"/>
        </Sessions>

        <Errors supportContact="root@localhost"
            helpLocation="/about.html"
            styleSheet="/shibboleth-sp/main.css"/>
        
        <MetadataProvider type="XML" url="https://aaiproxy.de.dariah.eu/idp" backingFilePath="dariah-aai-proxy-metadata.xml" maxRefreshDelay="7200" />

        <AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map.xml"/>
        
        <AttributeResolver type="Query" subjectMatch="true"/>

        <AttributeFilter type="XML" validate="true" path="attribute-policy.xml"/>

        <CredentialResolver type="File" key="sp-key.pem" certificate="sp-cert.pem"/>

    </ApplicationDefaults>
    
    <SecurityPolicyProvider type="XML" validate="true" path="security-policy.xml"/>

    <ProtocolProvider type="XML" validate="true" reloadChanges="false" path="protocols.xml"/>

</SPConfig>
```

-  ***Then make sure you download your SP's metadata from [https://your-domain.edu/Shibboleth.sso/Metadata](https://your-domain.edu/Shibboleth.sso/Metadata)***,
    have a look at them (Is the entityID correct? The locations? All
    HTTPS? The embedded certificate?) and send them to
    [register@dariah.eu](mailto:register@dariah.eu) with a request for
    entering them at the AAI proxy. Please state whether this service is
    a test or a production instance.
- In the other direction, the Proxy's metadata will be downloaded
    dynamically from
    [https://aaiproxy.de.dariah.eu/idp](https://aaiproxy.de.dariah.eu/idp/shibboleth),
    see above.

## Accept all common DARIAH attributes

**/etc/shibboleth/attribute-map.xml**

``` syntaxhighlighter-pre
<Attributes xmlns="urn:mace:shibboleth:2.0:attribute-map" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.13" id="unique-id">
        <AttributeDecoder xsi:type="ScopedAttributeDecoder"/>
    </Attribute>

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.6" id="eppn">
        <AttributeDecoder xsi:type="ScopedAttributeDecoder"/>
    </Attribute>

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.9" id="affiliation">
        <AttributeDecoder xsi:type="ScopedAttributeDecoder"/>
    </Attribute>

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.1" id="unscoped-affiliation"/>

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.1.1.7" id="entitlement"/>

    <Attribute name="urn:oid:2.5.4.3" id="cn"/>

    <Attribute name="urn:oid:2.5.4.4" id="sn"/>

    <Attribute name="urn:oid:2.5.4.42" id="givenName"/>

    <Attribute name="urn:oid:2.16.840.1.113730.3.1.241" id="displayName"/>

    <Attribute name="urn:oid:0.9.2342.19200300.100.1.3" id="mail"/>

    <Attribute name="urn:oid:2.16.840.1.113730.3.1.39" id="preferredLanguage"/>

    <Attribute name="urn:oid:2.5.4.10" id="o"/>

    <Attribute name="urn:oid:1.3.6.1.4.1.25178.1.2.5" id="schacCountryOfCitizenship"/>

    <Attribute name="urn:oid:1.3.6.1.4.1.5923.1.5.1.1" id="isMemberOf"/>

    <Attribute name="urn:oid:1.3.6.1.4.1.10126.1.52.5.2" id="dariahRole"/>

    <Attribute name="urn:oid:1.3.6.1.4.1.10126.1.52.4.15" id="dariahTermsOfUse"/>

</Attributes>
```

## Make the SP accept Scoped Attributes issued by other IdPs

**/etc/shibboleth/attribute-policy.xml**

``` syntaxhighlighter-pre
<afp:AttributeFilterPolicyGroup
    xmlns="urn:mace:shibboleth:2.0:afp:mf:basic"
    xmlns:saml="urn:mace:shibboleth:2.0:afp:mf:saml"
    xmlns:basic="urn:mace:shibboleth:2.0:afp:mf:basic"
    xmlns:afp="urn:mace:shibboleth:2.0:afp"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <!-- Shared rule for affiliation values. -->
    <afp:PermitValueRule id="eduPersonAffiliationValues" xsi:type="OR">
        <Rule xsi:type="AttributeValueString" value="faculty"/>
        <Rule xsi:type="AttributeValueString" value="student"/>
        <Rule xsi:type="AttributeValueString" value="staff"/>
        <Rule xsi:type="AttributeValueString" value="alum"/>
        <Rule xsi:type="AttributeValueString" value="member"/>
        <Rule xsi:type="AttributeValueString" value="affiliate"/>
        <Rule xsi:type="AttributeValueString" value="employee"/>
        <Rule xsi:type="AttributeValueString" value="library-walk-in"/>
    </afp:PermitValueRule>
    
    <!--
    Shared rule for all "scoped" attributes, but you'll have to manually apply it inside
    an AttributeRule for each attribute you want to check.
    -->
    <afp:PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="AttributeValueRegex" regex="@"/>
        </Rule>
        <Rule xsi:type="saml:AttributeScopeMatchesShibMDScope"/>
    </afp:PermitValueRule>

    <afp:AttributeFilterPolicy>
        <!-- This policy is in effect in all cases. -->
        <afp:PolicyRequirementRule xsi:type="ANY"/>

        <!-- Filter out undefined affiliations and ensure only one primary. -->
        <afp:AttributeRule attributeID="affiliation">
    <!-- make sure this policy accepts all scopes for (scoped)affiliation-->
            <afp:PermitValueRuleReference ref="eduPersonAffiliationValues"/>
        </afp:AttributeRule>
        <afp:AttributeRule attributeID="unscoped-affiliation">
            <afp:PermitValueRuleReference ref="eduPersonAffiliationValues"/>
        </afp:AttributeRule>
        <afp:AttributeRule attributeID="primary-affiliation">
            <afp:PermitValueRuleReference ref="eduPersonAffiliationValues"/>
        </afp:AttributeRule>
        
    <!-- make sure this policy accepts all scopes for eppn (i.e. comment completely)-->

    <!-- add scope check for eduPersonUniqueID -->
        <afp:AttributeRule attributeID="unique-id">
            <afp:PermitValueRuleReference ref="ScopingRules"/>
        </afp:AttributeRule>

        <!-- Catch-all that passes everything else through unmolested. -->
        <afp:AttributeRule attributeID="*" permitAny="true"/>
        
    </afp:AttributeFilterPolicy>

</afp:AttributeFilterPolicyGroup>
```
