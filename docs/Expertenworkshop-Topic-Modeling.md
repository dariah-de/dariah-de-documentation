# M 5.3.2 Expertenworkshop 'Topic Modeling', Göttingen, 20.05.16

last modified on Nov 28, 2016

## Ort und Zeit

    Medienraum (Raum 1.245) des Seminars für deutsche Philologie
    Käte-Hamburger-Weg 3
    37073 Göttingen

20.05.16, 10-17h

## Teilnehmende

- Fotis Jannidis (UWÜ)
- Steffen Pielström (UWÜ)
- Keli Du (UWÜ)
- Michael Huber (UWÜ)
- Christof Schöch (UWÜ)
- Simone Winko (Uni Göttingen)
- Gerhard Lauer (Uni Göttingen)
- Berenike Herrmann (Uni Göttgingen)
- Peer Trilke (Uni Potsdam)
- Thomas Weitin (TU Darmstadt)  

## Agenda

- Begrüßung und organisatorisches (Jannidis, Pielström)
- Topic Modeling: Theoretische Einführung (Jannidis)
- Vorstellung der Use Cases:
  - Emotionen in Lyrik-Anthologien um 1900 (Winko)
  - Korpus der Literarischen Moderne (Lauer, Herrmann)
  - Digitale Netzwerkanalyse dramatischer Texte (Trilke, Göbel,
        Kampkaspar)
  - Der „Deutsche Novellenschatz“ (Weitin)
  - Geschichte des Deutschsprachigen Romans (Jannidis)
  - Untergattungen des französischen Romans (Schöch)  
- Installation und Test eines Prototypen (Pielström, Huber)
- Diskussion und Bedarfsanalyse (Jannidis, Pielström)

## Protokoll

**Protokollführung: **Michael Huber

1. Begrüßung und Organisatorisches (Fotis Janidis)
2. Topic Modelling - Eine theoretische Einführung (Fotis Janidis)

- Semantik ist problematisch für einen Computer abzubilden.
- Beispiele
  - David Blei's Artikel 2011
        ([https://www.cs.princeton.edu/\~blei/papers/Blei2011.pdf](https://www.cs.princeton.edu/~blei/papers/Blei2011.pdf))
  - Martha Ballard's Tagebuch
        ([http://www.cameronblevins.org/posts/topic-modeling-martha-ballards-diary/](http://www.cameronblevins.org/posts/topic-modeling-martha-ballards-diary/))
  - Micki Kaufman „Quantifying Kissinger“
        ([http://www.mickikaufman.com/qk/](http://www.mickikaufman.com/qk/))
  - Blei/Ng/Jordan 2003
        ([http://ai.stanford.edu/\~ang/papers/nips01-lda.pdf](http://ai.stanford.edu/~ang/papers/nips01-lda.pdf))
- Grafiken zur Erklärung der theoretischen Grundlage zu finden in Blei
    2012
    ([https://www.cs.princeton.edu/\~blei/papers/Blei2012.pdf](https://www.cs.princeton.edu/~blei/papers/Blei2012.pdf))
- Vorannahmen des LDA-Algorithmus:
  - Die Reihenfolge der Wörter ist irrelevant
  - Die Reihenfolge der Dokumente ist irrelevant (oder auch nicht
        vgl. Verschiebung der Topics auf einer chronologischen Achse)
  - (nicht vollständig vgl. Vortragsfolien)
- Topic modelling generiert nicht ausschließlich Topics, sondern ein
    Verteilung über Worte (bspw. Werden auch rhetorische Strukturen in
    Topics zusammengefasst)

1. Emotionen in Gedichten (Isabel Chlie, Lena Walter, Simone Winko)

- Diskrepanz bei zeitgenössischer und aktueller Einordnung was
    „moderne Lyrik“ ist und wie Emotionen in dieser Literatur
    dargestellt werden.
- Überprüfen der hermeneutischen Ansätze mit Hilfe maschineller
    Verfahren
- Beispiele der hermeneutisch gewonnenen Thesen
  - Emitionen werden um 1900 sind Mischemotionen (verschiedene
        basale Emotionen überlagern sich)
  - es gibt um diese Zeit traditionelle Muster zur Darstellung von
        Emotionen
- Kriterien zur Korpusbildung (nicht vollständig vgl. Vortragsfolien)
  - erschienen um 1900 (1880-1910)
  - „Anthologien zeitgenössischer „moderner Gedichte
  - Auschschluss von Sammlungen mit Rezeptionsrestriktionen
  - breiter Adressatenkreis
  - Naturalistische Sammlung „Moderene Dichter-Charaktere“(Wilhelm
        Arent)
  - ...
- Auszeichnung der OCR gescannten Texte mit TEI-XML
- Erste maschinelle Ansätze: Voyant
    ([http://voyant-tools.org/](http://voyant-tools.org/))
- Anmerkung von CS: Benutzung von germanet (lexname=feeling)
    ([http://www.sfs.uni-tuebingen.de/GermaNet/](http://www.sfs.uni-tuebingen.de/GermaNet/))

1. Korpus der Literarischen Moderne – KOLIMO/Q-LIMO (Gerhard Lauer)

- psychologische/autorenorientierte Ausrichtung
- Erstellung eines literarischen Korpus der Moderne (1880-1930)
- Kombination quantitative und qualitative Analyse
- ganz wichtig: Kafka-Airport
    ([https://www.youtube.com/watch?v=gEyFH-a-XoQ](https://www.youtube.com/watch?v=gEyFH-a-XoQ))
- Aufbau eines Kafka- und eines Referenzkorpus (literarische und
    nichtliterarische Schriften)

1. (Kurze) Vorstellung des DkProWrappers

- [https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper](https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper)
- best tutorial
    ever: [https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper/blob/master/doc/tutorial.adoc](https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper/blob/master/doc/tutorial.adoc)

1. Vorstellung eines Topic Modelling-prozesses mit Hilfe eines ipython
    notebooks

- Diskussion, ob eine GUI gebraucht wird oder nicht:

    SP: anstatt einer GUI lieber ca. 5 gute dokumentierte Funktionen /
    bash-Befehle

    PT: funktioniert gut, wenn gut dokumentiert. GUI aber für
    didaktische Sachen auch ausserhalb der DH gut.

    GL: fehlende GUI hilft sich weiter in die Materie einzuarbeiten

    FJ: BA studenten wenig belastbar.

    CS: Kommandozeile schien auf einer Konferenz weniger das Problem

    SW: Warum nicht zweigleisig fahren?

    „Die Germanisten“: Die Benutzung der Kommandozeile ist durchaus
    erlernbar, aber nicht jeder möchte sich so stark hereinarbeiten
    müssen

    GL: Die Datenstruktur muss bei einer Demonstration sehr gut
    beschrieben werden.

- → leichtgewichtige GUI, Weiterentwicklung an der GUI-losen Version

1. Peer Trilcke - Universität Potsdam

1.  Dramenkorpus (Textgriddaten) 1730 -1930

- manuelle Aufbereitung
- daraus wurden Strukturdaten extrahiert und in ein Korpus umgewandelt

1. automatisierte Extraktion philologisch relevanter Daten und
    Visualisierung in Netzwerken
2. Wie kann man TM darauf anwenden? Was macht TM mit dem Text?

3. Exkurs: FJ Metadaten zur Segmentierung

    PT: Frage nach einem einheitlichen Austauschformat

    FJ: 1-2 Treffen im Jahr notwendig um Teilprobleme zu identifizieren

    Generell: Es soll ein Emailverteiler eingerichtet werden Schlagwort
    „Dariah-Topics“. Google-Mailingliste einrichten um ein Archiv
    mitgeliefert werden.

4. Thomas Weitin(TU Darmstadt): DARIAH Topic Modeling Auftakt
    1. Korpus

- Heyse, Kurz: Deutscher Novellenschatz
- 86 Novellen, 82 Autoren
- 1840: 14 Novellen, 1850: 22, 1860:18
- Cluster, ConsensusTree, Netzwerkanalyse (Basierend auf Delta,
    Vorgehen nach Eder) Dabei verwendet: Visone
    ([https://visone.info/](https://visone.info/))

1. Fragen und Analysen

- Typologie von Durschnittlichkeit: lokal/global
- Novellenschatz als Data Frame
- (nicht vollständig vgl. Vortragsfolien)

1. Exkurs: FJ wie kann man Ergebnisse von TM auf segmentierte Dokumente
    wieder zusammenführen. Unterschiedliches Vorgehen von CS und FJ.

    Ausserdem: literarische Texte nehmen einen besonders besonderen
    Stellenwert ein, weshalb unsere Gruppe so wichtig ist :)

2. Vortrag von Christof Schöch + dazugehörige Funktion:

    Folgendes Vorgehen:

- Mastermatrix auch für den Gensimoutput
- Bündelung aller Visualisierungen in ein Visualisierungspaket (CS
    Vorschlag: pygal)
- preprozessieren → Entscheidung TMW oder Mallet für TM →
    Visualiserungspaket nutzen
