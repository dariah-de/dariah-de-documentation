# FAQs zum DARIAH-DE Repository

last modified on Jan 22, 2018

Die FAQs (Frequently Asked Questions) sollen grundlegende Fragen in
knapper Form erklären und ggf. auf weitere Informationen verweisen.

Wenn Sie eine Frage zu den Modulen der DFA haben, stellen Sie uns diese!
Wir antworten darauf und werden sie anschließend hier mit aufführen –
die FAQs werden also über die Zeit wachsen.

Sie erreichen uns unter
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zum%20DARIAH-DE%20Publikator).

## Wer kann das Repository nutzten und was kostet das?

Das DARIAH-DE Repository steht allen Forscherinnen und Forschern der
Geistes- und Kulturwissenschaften zur Verfügung. Zur Nutzung ist ein
institutioneller Account oder ein [DARIAH Account](https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg)
nötig.

Die Speicherung im DARIAH-DE Repository ist grundsätzlich kostenlos. Bei
Datenmengen von mehr als 100 GB bitten wir um vorherige Kontaktaufnahme
unter [info@de.dariah.eu](mailto:info@de.dariah.eu).

## Warum sollen Daten im DARIAH-DE Repository gespeichert werden?

Das DARIAH-DE Repository ist ein fachspezifisches Repositorium für
geistes- und kulturwissenschaftliche Forschungsdaten. Die Richtlinien
vieler Wissenschaftsorganisationen und Institutionen empfehlen das
Speichern der Daten in solchen dediziert fachspezifischen Repositorien.
Dadurch wird u.a. das Suchen und Finden in wissenschaftlichen, fachlich
relevanten Beständen ermöglicht.

Der Betrieb des DARIAH-DE Repository wird durch das Humanities Data
Centre (HDC) sichergestellt. Das HDC ist eine Kooperation der
Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen
(GWDG) und der Niedersächsischen Staats- und Universitätsbibliothek
Göttingen (SUB). Beide Einrichtungen sichern so gemeinsam die
Nachhaltigkeit der Forschungsdaten auch langfristig ab.

## Wo werden meine Dateien gespeichert?

Ihre Daten werden im DARIAH-DE Storage gespeichert. Dieser wird
betrieben auf den Servern des [Humanities Data Centre](https://humanities-data-centre.de/) in Göttingen.

Zunächst laden Sie Ihre Daten über den
[Publikator](FAQs-zum-Publikator.md) in den sogennanten
DARIAH-DE OwnStorage, auf den nur Sie selbst Lese- und Schreibzugriff
haben. Während des Publikationsvorgangs werden Ihre Daten in den sog.
DARIAH-DE PublicStorage kopiert, so dass nun weltweit öffentlich darauf
zugegriffen werden kann. Nach der Publikation Ihrer Daten können Sie
diese aus dem OwnStorage wieder löschen, sie bleiben weiterhin über den
PublicStorage erreichbar.

## Warum sollte ich das DARIAH-DE Repository nutzen und nicht das viel bekanntere Git?

Das DARIAH-DE Repository ist ein Repositorium für Forschungsdaten der
Geistes- und Kultuwissenschaften. Individuelle ForscherInnen können hier
ihre Daten publizieren, um sie nachhaltig und persistent zu speichern
und adressieren. Zu diesem Zweck werden die Daten im DARIAH-DE
Repository durch die betreibenden Öffentlichen Einrichtungen mindestens
zehn Jahre gespeichert und sind über DOIs referenzierbar.

Auch wenn Versionskontrollsoftware wie [git](https://git-scm.com/),
[svn](https://subversion.apache.org/) und andere denselben Begriff
verwenden, haben sie eine gänzlich andere Funktion: Sie erlauben die
Speicherung und Verwaltung von Änderungen an (gemeinsam) gepflegtem
Source Code für Software. Auf kommerziellen Entwickler-Plattformen wie
[Bitbucket](https://bitbucket.org), [GitHub](https://github.com/) und
[Gitlab](https://gitlab.com/) kann dieser Code öffentlich geteilt
werden, für die langfristige Speicherung empfehlen aber auch diese die
Speicherung von Release-Versionen in [(generischen) Forschungsdatenrepositorien](https://guides.github.com/activities/citable-code/).

## Können im DARIAH-DE Repository Websites gespeichert werden?

Für die nachhaltige Speicherung der Forschungsdaten und -ergebnisse
bietet sich das DARIAH-DE Repository an. Der langfristige Betrieb einer
vollständigen und spezialisierten Internetplattform kann über DARIAH-DE
hingegen nicht abgebildet werden, da sie dynamische Elemente (z.B.
Suchfunktion) beinhalten.

Das HDC hat aufgrund konkreten Bedarfs das Konzept der sogenannte
Anwendungskonservierung entwickelt. Dabei geht es im Wesentlichen um die
statische, vollständige Kopie eines Anwendungssystems in eine VM hinter
eine Proxy-Wand. Solche Lösungen könnten eine Sicherung von ca. 5-10
Jahre ermöglichen. Das Vorgehen hat einen Proof-of-Concept-Status,
derzeit also noch keine Produktionsreife.

Interessenten können sich an DARIAH-DE oder das HDC wenden, um die
Möglichkeit einer entsprechenden Vereinbarung zu einem derartigen
Betrieb zu eruieren.

## Wie lange werden meine Dateien gespeichert?

Das [Humanities Data Centre](https://humanities-data-centre.de/)
garantiert die Speicherung für 10 Jahre ab Einspielung.

## Kann ich meine Daten auch wieder löschen?

Nachdem Sie Ihre Daten publiziert haben, ist eine Löschung nicht mehr
möglich.

## Was ist dieser Publikator, von dem ich überall lese?

Der Publikator ist ein Webinterface, über das Sie Ihre Daten hochladen
und publizieren können, siehe auch die [FAQ des Publikators](FAQs-zum-Publikator.md) und die
[Dokumentation](Das-DARIAH-DE-Repository.md).

## Wen kann ich bei Missbrauch kontaktieren?

Bitte wenden Sie sich an [info@de.dariah.eu](mailto:info@de.dariah.eu).
