# FAQs zum Data Modelling Environment

last modified on Nov 29, 2017

Die FAQs (Frequently Asked Questions) sollen grundlegende Fragen in
knapper Form erklären und ggf. auf weitere Informationen verweisen.

Wenn Sie eine Frage zu den Modulen der DFA haben, stellen Sie uns diese!
Wir antworten darauf und werden sie anschließend hier mit aufführen –
die FAQs werden also über die Zeit wachsen.

Sie erreichen uns unter
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zum%20Data%20Modelling%20Environment).

## Was kann das DME?

## Wo finde ich eine Dokumentation der Grammatik / wie müssen die Transformationen verfasst werden?

## Was ist ein Mapping?

## Ich würde gerne Daten mappen, brauche aber Hilfe - wohin kann ich mich wenden?

## Werden meine (Meta-)Daten beim Mapping verändert?
