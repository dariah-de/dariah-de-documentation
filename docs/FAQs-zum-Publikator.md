# FAQs zum Publikator

last modified on Dez 05, 2017

Die FAQs (Frequently Asked Questions) sollen grundlegende Fragen in
knapper Form erklären und ggf. auf weitere Informationen verweisen.

Wenn Sie eine Frage zu den Modulen der DFA haben, stellen Sie uns diese!
Wir antworten darauf und werden sie anschließend hier mit aufführen –
die FAQs werden also über die Zeit wachsen.

Sie erreichen uns unter
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zum%20DARIAH-DE%20Publikator).

## Werden meine Daten durch das Hochladen (Datei hinzufügen) veröffentlicht?

Nein. Die Daten werden erst durch das Publizieren veröffentlicht.
Solange sie sich im Entwurfsstadium befinden, sind die Daten nur für Sie
zugänglich. Daten im Entwurfsstadium können auch ausgetauscht und wieder
gelöscht werden.  

## Wann werden meine Daten publiziert?

Der Publikationsvorgang wird erst durch Anklicken des Buttons
**Kollektion publizieren** angestoßen. Vorher müssen Sie noch
bestätigen, im Besitz aller Rechte zu sein, um Ihre Kollektion inkl.
aller Daten und Metadaten zu veröffentlichen und die Nutzung durch
Dritte zu ermöglichen und Sie müssen
den [Nutzungsbedingungen](https://hdl.handle.net/21.T11991/0000-0005-8FB8-B@data)
des DARIAH-DE Repositorys zustimmen.

## Wie kann ich meine Daten publizieren?

Der Button **Kollektion publizieren** befindet sich in der Übersicht
über Ihre Kollektionen.

Befinden Sie sich im Modus **Kollektion bearbeiten**, klicken Sie erst
auf **zur Übersicht**.

![Kollektion Bearbeiten](attachments/58498547/58504674.png)

Anschließend wählen Sie die Kollektion aus, die publiziert werden soll.
Nun erscheint der Button **Kollektion publizieren**.

![Kollektion publizieren](attachments/58498547/58504676.png)  

## Wie groß kann eine einzelne Datei maximal sein?

Das Limit beträgt momentan 4 GB pro Datei. Sollten Sie größere Datei
benötigen, so wenden Sie Sich bitte an
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zum%20DARIAH-DE%20Publikator).

## Ich habe meine Kollektion bearbeitet. Wo ist der Speichern Knopf?

Die Änderungen werden automatisch gespeichert, es ist nicht notwendig,
manuell zu speichern. Sie sehen neben dem "Unterkollektion erstellen"
Button den aktuellen Speicherstatus. Solange das Speichern noch nicht
abgeschlossen wurde, ist der "zur Übersicht" Button deaktiviert.  

## Ich habe auf publizieren geklickt und aus Versehen meinen Browser geschlossen. Muss ich erneut publizieren? 

Nein, Ihre Kollektion wird weiterhin im Hintergrund publiziert. Melden
Sie sich einfach wieder am Publikator an und Sie sehen den
Publikationsfortschritt.  
