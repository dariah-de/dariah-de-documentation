# FAQs zur Collection Registry

last modified on Dez 05, 2017

Die FAQs (Frequently Asked Questions) sollen grundlegende Fragen in
knapper Form erklären und ggf. auf weitere Informationen verweisen.

Wenn Sie eine Frage zu den Modulen der DFA haben, stellen Sie uns diese!
Wir antworten darauf und werden sie anschließend hier mit aufführen –
die FAQs werden also über die Zeit wachsen.

Sie erreichen uns unter
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zur%20Collecrtion%20Registry).

## Für wen sind meine Daten in der Collection Registry sichtbar?

Das kommt darauf an, in welchem Stadium sich Ihre Sammlungsbeschreibung
befindet:

1. Ist sie als **Entwurf** gespeichert, weil Sie noch nicht fertig sind
    mit der Bearbeitung, oder an einem späteren Zeitpunkt weitere Daten
    ergänzen möchten, ist die Beschreibung **lediglich für Sie selbst
    sichtbar**, sobald Sie sich an der Collection Registry angemeldet
    sind.
2. Ist Ihre Sammlungsbeschreibung dagegen bereits **veröffentlicht**,
    ist sie **für alle (auch nicht angemeldeten) Nutzer sichtbar**. Für
    **angemeldete User** ist sie sogar **bearbeitbar**.

Sie selbst können ebenfalls alle veröffentlichten
Sammlungsbeschreibungen einsehen und nach erfolgreicher Anmeldung
bearbeiten.

## Wie viele Sammlungsbeschreibungen darf ich in der Collection Registry anlegen?

Sie dürfen gerne so viele Beschreibungen und Entwürfe anlegen, wie Sie
möchten; es sind keine Grenzen gesetzt. Natürlich freut sich DARIAH-DE
über möglichst viele **veröffentliche** und ausführlich ausgezeichnete
Sammlungsbeschreibungen!

## Welche Art von Sammlungen darf ich beschreiben?

Generell dürfen Sie gerne jede Art von wissenschaftlicher Sammlung in
der Collection Registry beschreiben.

## Es liegt keine Schnittstelle zu meinen Daten vor. Was sind die Möglichkeiten?

Auch wenn keine Schnittstelle zu den beschriebenen Objekten vorliegt,
empfiehlt es sich, eine Sammlungsbeschreibung in der Collection Registry
anzulegen. Vielleicht haben Sie Ihre Daten trotzdem zugänglich gemacht,
bspw. über eine eigene Homepage. Dann können Sie sehr gerne die
Zugangsdaten in dieser Form angeben und so anderen Fachwissenschaftlern
und Fachwissenschaftlerinnnen den Zugang zu den Daten erleichtern.

Sollte überhaupt kein Zugang zu den beschriebenen Daten vorhanden sein,
ist es trotzdem von Vorteil, Ihre Sammlung zumindest sichtbar zu machen,
indem Sie sie beschreiben. So ist sie zumindest auffindbar und
Interessierte können sich an die von Ihnen angegebenen Kontaktdaten oder
Standorte wenden. In diesem Fall ist es von Vorteil, möglichst
umfassende Metadaten zu der Sammlungsbeschreibung mitzuliefern und so
bspw. Thematik, Inhalt, Standort und weitere Informationen
bereitzustellen.

Sie können Ihre Daten auch im [DARIAH-DE Repository](Das-DARIAH-DE-Repository.md) veröffentlichen. Es
steht allen Forscherinnen und Forschern der Geistes- und
Kulturwissenschaften zur Verfügung  

## Wie kann man bereits eingetragene Beschreibungen bearbeiten?

Um bereits eingetragene und veröffentlichte Sammlungsbeschreibungen
bearbeiten zu können, muss man sich an der Collection Registry mit einem
DARIAH Account anmelden.  

## Ich bin unsicher, wie manche Felder der Collection Registry auszufüllen sind. Wo finde ich Hilfe?

Die Metadaten-Felder der Collection Registry sind relativ umfangreich.
Manche der Felder sind evtl. schwer verständlich oder missverständlich.
Erste Informationen erhalten Sie, indem Sie oben links die
**Editor-Optionen** einblenden. Wie dies geht, können Sie in unseren
[User-Guides](Die-Collection-Registry.md) nachschlagen.

Als Schema für die Metadaten-Felder dient ein eigens entwickeltes Model,
das [DARIAH Collection Description Data Model (DCDDM)](DARIAH-Collection-Description-Data-Model-DCDDM.md).
Es ist angelehnt an die Dublin Core Simple Standards, umfasst aber
zusätzliche und zum Teil spezifischere Angaben. Sie können sich zu jedem
Feld in der Collection Registry detailliert im DCDDM informieren.

Sollten Sie dennoch Fragen haben oder Probleme auftauchen, wenden Sie
sich gerne an [info@de.dariah.eu](mailto:info@de.dariah.eu)  

## Wo finde ich weiterführende Informationen zum Thema wissenschaftliche Sammlungen, Metadaten und dem Umgang mit Forschungsdaten?

In unserem [Public Wiki](index.md) und auf der [DARIAH-DE Website](http://de.dariah.eu) finden Sie
umfangreiche Informationen rund um das Thema Digital Humanities und
somit natürlich auch zu wissenschaftlichen Sammlungen, Metadaten und dem
richtigen Umgang mit Forschungsdaten. Neben einer umfangreichen
Bibliographie und Informationen zu den verschiedenen Angeboten von
DARIAH-DE finden Sie bspw. auch aktuell stattfindene Veranstaltungen
oder Workshops.
