# FAQs zur Datenföderationsarchitektur (DFA)

last modified on Dez 04, 2017

Die FAQs (Frequently Asked Questions) sollen grundlegende Fragen in
knapper Form erklären und ggf. auf weitere Informationen verweisen.

Wenn Sie eine Frage zu den Modulen der DFA haben, stellen Sie uns diese!
Wir antworten darauf und werden sie anschließend hier mit aufführen –
die FAQs werden also über die Zeit wachsen.

Sie erreichen uns unter
[support@de.dariah.eu](mailto:support@de.dariah.eu?subject=Eine%20Frage%20zur%20Datenföderationsarchitektur).

## [FAQszum Publikator](FAQs-zum-Publikator.md)

## [FAQszum DARIAH-DE Repository](FAQs-zum-DARIAH-DE-Repository.md)

## [FAQszur Collection Registry](FAQs-zur-Collection-Registry.md)
