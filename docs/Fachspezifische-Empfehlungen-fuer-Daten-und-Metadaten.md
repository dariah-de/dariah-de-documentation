# Fachspezifische Empfehlungen für Daten und Metadaten

last modified on Jan 25, 2016

**Arbeitspaket:** 3.2 Fachspezifische Standards und Empfehlungen

**Verantwortlicher Partner:** DT/PB

**Projekt:** DARIAH-DE: Aufbau von Forschungsinfrastrukturen für die
e-Humanities

**BMBF Förderkennzeichen:** 01UG1110A bis M

**Laufzeit:** März 2011 bis Februar 2014

**Kontakt:** [info@de.dariah.eu](mailto:info@de.dariah.eu)

**Autoren (AP 3.2):**  

Anna Aurast, IEG
Nikolaos Beer, DT/PB
Marcus Held, IEG
Kristin Herold, DT/PB
Wibke Kolbmann, DAI
Thomas Kollatz, STI
Kristina Richts, DT/PB
Stefan Schmunk, IEG
Joachim Veit, DT/PB

**Unter weiterer Mitarbeit von:**

Johannes Kepper (Musikcodierung), Harald Lordick (externe Verwaltung von
Metadaten), Sebastian Rose (Diplomatik), Philipp Vanscheidt
(Kodikologie)

**Revisionsverlauf:**

|            |                                                                                                         |                                                                          |
|------------|---------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
| Datum      | Autor                                                                                                   | Kommentare                                                               |
| 10.10.2012 | Nikolaos Beer, Kristin Herold, Wibke Kolbmann, Thomas Kollatz, Stefan Schmunk, Joachim Veit             | Essentials für fachspezifische Empfehlungen (R 3.2.1 - V. 1.0)           |
| 28.02.2014 | Anna Aurast, Marcus Held, Kristin Herold, Wibke Kolbmann, Thomas Kollatz, Kristina Richts, Joachim Veit | Fachspezifische Empfehlungen für Daten und Metadaten ( R 3.2.2 - V. 1.0) |
|            |                                                                                                         |                                                                          |
