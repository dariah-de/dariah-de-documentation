# Geo-Browser Dokumentation

last modified on Mai 06, 2019

Die Dokumentation des Geo-Browsers finden Sie unter: **[https://geobrowser.de.dariah.eu/doc/de/geobrowser.html](https://geobrowser.de.dariah.eu/doc/de/geobrowser.html)**.

Die FAQ-Seite mit häufig gestellten Fragen zum Geo-Browser finden Sie unter:
**[https://geobrowser.de.dariah.eu/doc/de/geobrowser-faq.html](https://geobrowser.de.dariah.eu/doc/de/geobrowser-faq.html)**.
