# Gesamtgeschäftsmodell für DARIAH-DE

last modified on Mär 06, 2017

Das Gesamtgeschäftsmodell für DARIAH-DE ist mit Stand März 2017 als
Report dokumentiert. Dieser Report ist auf Nachfrage einsehbar und wird
an dieser Stelle nicht öffentlich bereitgestellt. Falls Sie Einsicht
wünschen, wenden Sie sich bitte
an [info@de.dariah.eu](mailto:info@de.dariah.eu) 
