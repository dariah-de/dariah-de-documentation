# Gremien

last modified on Feb 19, 2018

## [Wissenschaftlicher Beirat](Wissenschaftlicher-Beirat.md)

Der Wissenschaftliche Beirat aus FachwissenschaftlerInnen und
IT-SpezialistInnen begleitet die Entwicklung von DARIAH-DE. Dieses
Gremium mit seinen siebzehn Mitgliedern aus dem deutschsprachigen Raum
tagt circa einmal im Jahr, um das Projekt und den Aufbau einer digitalen
Forschungsinfrastruktur aktiv zu begleiten und zu fördern. Zu diesem
Zweck erhalten die Beiratsmitglieder einen privilegierten Zugriff auf
Informationen über das Projekt und dessen Ergebnisse. Hierfür ist eine
E-Mail-Liste über aktuelle Themen eingerichtet worden.

## [Technical Advisory Board (TAB)](Technical-Advisory-Board.md)

Das Technical Advisory Board begleitet die Entwicklung der technischen
Infrastruktur der Projekte CLARIN-D und DARIAH-DE. Dieses Gremium ist
international, mit sechs IT-SpezialistInnen besetzt, die die (Weiter-)
Entwicklung der technischen Komponenten der Forschungsinfrastruktur
fördern. Dazu erhalten die Beiratsmitglieder einen privilegierten
Zugriff auf Informationen über und Ergebnisse aus dem Projekt. Zudem ist
eine E-Mail-Liste über aktuelle Themen eingerichtet worden.

## [Stakeholdergremium "Wissenschaftliche Sammlungen"](Stakeholdergremium-Wissenschaftliche-Sammlungen.md)
