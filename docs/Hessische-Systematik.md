# Hessische Systematik

last modified on Jan 14, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

Die **[Hessische Systematik](http://museum.zib.de/museumsvokabular/documents/systematik-hessen-original-2003.pdf)** ist
eine deutschsprachige Systematik, die von und für kulturhistorische
Museen erstellt wurde und vom Hessischen Museumsverband gepflegt wird.
Die
[Dokumentation](http://museum.zib.de/museumsvokabular/index.php?main=download&ls=9&co=we&ln=de)
ist online abrufbar. Sie besteht aus 18 Hauptgruppen:

- Architektur
- Bildwerke
- Brauch und Fest
- Forstwirtschaft/Jagd/Fischerei
- Freizeit/Unterhaltung/Genuss
- Gesundheit
- Handwerk/Industrie/Handel
- Hauswirtschaft
- Kleidung
- Kommunikation
- Landwirtschaft
- Messen und Wiegen
- Öffentlichkeit und Gemeinwesen
- Religion und Glaube
- Schriftgut
- Spielzeug/Spiel/Sport
- Transport und Verkehr
- Wohnen
