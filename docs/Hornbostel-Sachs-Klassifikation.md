# Hornbostel-Sachs-Klassifikation

last modified on Feb 17, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

Die **Hornbostel-Sachs-Klassifikation** wird in der Musikwissenschaft
für Musik-Ikonographien eingesetzt. Es handelt sich um ein
Klassifikationssystem für Musikinstrumente, das 1914 entwickelt wurde.
Diese Klassifikation wird beispielsweise im [Répertoire International d'Iconographie Musicale (RIdIM)](http://www.ridim-deutschland.de/ridim/index.php?pcontent=startseite) genutzt.
Aber auch jüngere Projekte greifen auf diese Klassifikation zurück,
jedoch indem sie (teilweise sehr stark)
modifiziert([^1]) wird, so im [Musical Instrument Museums Online (MIMO)](http://www.mimo-international.com/).


[^1]: Vgl.
dazu: [http://www.mimo-international.com/documents/results/MIMO_Deliverable_3.3.pdf](http://www.mimo-international.com/documents/results/MIMO_Deliverable_3.3.pdf).
