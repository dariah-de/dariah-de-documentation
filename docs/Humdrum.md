# Humdrum

last modified on Feb 23, 2014

[3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)

Das von David Huron um 1990 am CCARH entwickelte
**[Humdrum](http://musiccog.ohio-state.edu/Humdrum/guide02.html)**([^1]) dient
nicht nur der Auszeichnung des Notentextes, sondern bietet mit dem
darauf aufsetzenden *Humdrum Toolkit*, einer Sammlung von Auswertungs-
und Bearbeitungsskripten, auch vielfältige Möglichkeiten **musikalischer
Analyse**. Diese steht konsequent im Fokus des Formats, während die
Unterstützung satzspezifischer Fragestellungen bestenfalls rudimentär
ist.

Das ***Humdrum Toolkit*** beinhaltet darüber hinaus eine Reihe von
Skripten, die Noten aus anderen Dateiformaten (u.a. MuseData, MIDI,
Finale, Guido, MusicXML) nach Humdrum konvertieren bzw. sie teils auch
in diese Formate exportieren können, so dass das Format zu den bislang
attraktivsten im musikwissenschaftlichen Umfeld gehört.

Humdrum unterscheidet für die Angabe zusätzlicher, über die reine
Codierung des Notentexts hinausgehender Daten zwischen ***Comment
Records*** und ***Reference Records***. *Comment Records* werden mit ein
oder zwei Ausrufezeichen gekennzeichnet; ein Ausrufezeichen leitet
lokale Kommentare ein, zwei Ausrufezeichen werden dagegen für globale
Kommentare genutzt, die beispielsweise die Quelle und den Titel des
Stücks beschreiben. Die mit drei vorangestellten Ausrufezeichen
markierten Einträge, die als *Reference Records* bezeichnet werden,
beschreiben laut Humdrum Website Informationen, wie sie üblicherweise in
bibliothekarischen Angaben bzw. bei der Katalogisierung begegnen. Als
Beispiel folgt eine Auszeichnung der Metadaten des Lieds *[Der Abendstern](attachments/20059205/22118459.jpg)* von Robert Schumann. Die
Akronyme sind in einer begleitenden Liste festgehalten (hier ab Zeile 6:
Composer, Composer's Dates, Date of Composition, Title in German,
Publication Status, Date and Owner of Electronic Copyright, Genre
Designation, Style-period, Metric Classification, Instrumentation).

**Beispiel für die Codierung von Metadaten in Humdrum** Quelle erweitern

``` syntaxhighlighter-pre
!!    Liederalbum für die Jugend
!!  Transcribed by Kristin Herold
!!  No. 79/1    "Der Abendstern"
**kern          **lyrics
!               in german
 …              …
!!!COM:         Schumann, Robert
!!!CDT:         1810/6/8/-1856/7/29
!!!ODT:         1849
!!!OTL@@GER:    Der Abendstern
!!!PUB:         Breitkopf & Härtel
!!!YEC:         Copyright 1991 by Breitkopf & Härtel
!!!AGN:         Lied
!!!AST:         Romantic
!!!AMT:         simple
!!!AIN:         Ipiano Ivox
```

Humdrum bietet über die im vorstehenden Beispiel sichtbaren einzelnen
Zeilen (spines) hinaus eine Reihe zusätzlicher Metadatenzeilen für
Unterrubriken der Kategorien Authorship, Recording, Performance, Work
Identification, Group Information, Imprint, Copyright, Analytic
Information, Representation und Electronic Citation. Dies geht weit über
die Möglichkeiten hinaus, die etwa das Codierungsformat MuseData bietet.

Das Format erweist sich durch die Aufteilung in mehrere Bereiche als
**sehr flexibel**. Die Besonderheit des Formats liegt in der
**sequentiellen Darstellung** der codierten Daten. Aus diesem Grund muss
die Abfolge der Inhalte (zeitlich, räumlich etc.) nachvollziehbar sein.
Die **Codierung des Notentextes** erfolgt in mehreren **spines**
(Spalten), die durch Tabulatoren voneinander abgegrenzt werden. Die
entstehenden Spalten werden **records** genannt. Zeitlich aufeinander
folgende Ereignisse werden untereinander codiert, zeitgleiche Ereignisse
jedoch nebeneinander. Die einzelnen Stimmen eines Werkes werden dabei
üblicherweise aufsteigend codiert, beginnend mit der tiefsten
Stimme. Die zunächst einzeln erfolgenden Codierungen der Stimmen einer
Partitur können in einer gemeinsamen Datei abgelegt werden, wodurch ein
sehr breiter Quelltext entstehen kann.

Die Codierung selbst beschränkt sich auf tatsächliche musikalische
Bestandteile einer Partitur. Es ist eine Vielzahl eigens für Humdrum
entwickelter **Datenstrukturen** (representations) vorhanden, mit denen
Musik auf unterschiedliche Weise dargestellt werden kann. Die wohl
bekannteste Struktur ist **\*\*kern**, mit dem sich etwa die Tonhöhe und
Tondauer einer Note darstellen lässt, aber auch weitere musikalische
Phänomene wie etwa Bogensetzungen oder Balkungen.

Die nachfolgenden beiden Codierungsbeispiele zeigen den Auftakt und den
ersten Takt des Liedes *Der Abendstern*. Die Codierungen der Singstimme
und des Pianofortes erfolgen separat. Beiden Beispielen ist der
Kopfbereich gemein, der die wichtigsten Metadaten des Werkes beinhaltet
und daher auch bei beiden Dateien gleich ist. Auch hier werden die
Metadaten mit drei vorangestellten Ausrufezeichen erfasst (vgl. Z. 1 bis
6), im Einzelnen hier der Komponist (Z. 1), der Titel des codierten
Werkes (Z. 2), der Titel des übergeordneten Werkes (Z. 3), die
Originalsprache des Werkes (@-Zeichen in Z. 2 und 3), die Opuszahl des
Werkes (Z. 4) sowie die Nummer des Stückes innerhalb des Werkes (Z. 5).
Das Kürzel EFL in Zeile 6 bezeichnet die Dateinummer innerhalb einer
zusammengehörigen Gruppe von Dateien. Hier können allerdings keine
Verweise zwischen den zusammengehörigen Dateien einer Gruppe gespeichert
werden. Der mit lediglich zwei Ausrufezeichen gekennzeichnete Eintrag in
Zeile 7 beinhaltet einen Kommentar, der für alle Inhalte der Codierung
gilt. So besagt Zeile 7, dass es sich hier um die Codierung der
Sopranstimme handelt. Weiterhin gibt es noch den (in den unten
aufgeführten beiden Beispielen nicht vorkommenden) Fall, dass einem
Eintrag lediglich ein Ausrufezeichen vorangestellt ist. Hierbei handelt
es sich um Kommentare.

Die in Zeile 8 mit jeweils zwei \*\* gekennzeichneten Einträge (\*\*kern
und \*\*silbe) definieren die im nachfolgenden Bereich der Datei
verwendeten representations. Die innerhalb einer Zeile (record)
auftretenden "Ereignisse" finden jeweils gleichzeitig statt. So
beschreibt etwa der Eintrag "8a/" in Zeile 14 den Auftakt der Singstimme
(ein a' mit dem Wert einer Achtel, Halsrichtung nach oben – ein nach
unten gerichteter Notenhals würde entsprechend mit einem Backslash
codiert werden). Die rechts daneben aufgeführten records bezeichnen die
vier Tonsilben, die in den einzelnen Versen auf dieser Achtel erklingen
sollen. Taktstriche werden mit einem Gleichheitszeichen gekennzeichnet.
Die in den Zeilen 10 bis 13 mit einem vorangestellten \*
gekennzeichneten Informationen beziehen sich auf das System, das codiert
wird (staff 1), die Schlüsselung (hier ein G-Schlüssel, der auf der
zweiten Notenlinie von unten steht), die Vorzeichnung (drei
Kreuzvorzeichen) und die Taktart (2/4-Takt).

**Beispiel für die Codierung des Notentextes (Singstimme) in Humdrum** Quelle erweitern

``` syntaxhighlighter-pre
!!!COM: Schumann, Robert
!!!OTL@@GER: Der Abendstern
!!!OPR@@GER: Liederalbum für die Jugend
!!!OPS: 79
!!!OMN: 1
!!!EFL: 1/2
!!Soprano
**kern      **silbe     **silbe     **silbe     **silbe
*           *LDeutsch   *LDeutsch   *LDeutsch   *LDeutsch
*staff1     *staff1     *staff1     *staff1     *staff1
*clefG2     *           *           *           *
*k[f#c#g#]  *           *           *           *
*M2/4       *           *           *           *
8a/         Du          Wie         So          Wie
=           =           =           =           =
4c\         lieb-       lieb'       blick'      nickst
8b\         -li-        ich         ich         du
8a/         cher        doch        nach        mir
=           =           =           =           =
```

Die Codierung der Klavierstimme wurde hier noch hinzugefügt, weil sich
daran die einfache Erweiterbarkeit des Formats deutlich erkennen lässt.
So kann die Anzahl der spines beliebig erweitert werden. Erweiterungen
werden mit einem \*^ gekennzeichnet (vgl. etwa Z. 13). Das Beispiel
zeigt zudem das Prinzip der Bogensetzung in Humdrum. Der Beginn eines
Bogens wird dabei mit einem "(" (vgl. etwa Z. 19) hinter der codierten
Note beschrieben, das Bogenende mit einem ")" (vgl. Z. 23). Die
Balkensetzung erfolgt in Humdrum mit Hilfe der Buchstaben "L" (Beginn)
und "J" (Ende) (vgl. Z. 22 und 23). Zwei Balken würden mit "LL" und "JJ"
codiert.

**Beispiel für die Codierung des Notentextes (Klavierbegleitung) in Humdrum** Quelle erweitern

``` syntaxhighlighter-pre
!!!COM: Schumann, Robert
!!!OTL@@GER: Der Abendstern
!!!OPR@@GER: Liederalbum für die Jugend
!!!OPS: 79
!!!ONM: 1
!!!EFL: 2/2
!!Pianoforte
**kern      **kern
*staff3     *staff2
*clefF4     *clefG2
*k[f#c#g#]  *k[f#c#g#]
*M2/4       *M2/4
*^          *^
*^          *           *^          *
*           *^          *^          *^
*           *           *           *           *           *           *
8A\         .           .           8a/(        .           .           .
=           =           =           =           =           =           =
4AA\        4E\         4A\         4c/         4e/         4a/         4cc/
8BB\L       8E\         .           8d/         8e/         8g/         8b/L
8C\J        8E\         8A\J        .           8e\)        .           8a/J
=           =           =           =           =           =           =
```

Humdrum bietet keine Möglichkeiten, graphische Aspekte zu codieren.
Aufgrund der sequentiellen Darstellung der codierten Informationen ist
ebensowenig eine Erfassung von Varianten möglich. Daher ist das Format
für den Einsatz zu editorischen Zwecken nicht geeignet.

Als positiv hervorzuheben ist die umfassende Dokumentation des Formats,
die auf der Website des [Center for Computer Assisted Research in the Humanities](http://humdrum.ccarh.org) (CCARH) der [Standford University](http://www.stanford.edu) zur Verfügung steht.  


[^1]: Einen Überblick über die Reference Records gibt: [http://musiccog.ohio-state.edu/Humdrum/guide.append1.html#AIN;](http://musiccog.ohio-state.edu/Humdrum/guide.append1.html#AIN;) eine kritische Bewertung bei Kepper, a.a.O., S. 324–337.
