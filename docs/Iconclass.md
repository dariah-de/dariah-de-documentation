# Iconclass

last modified on Jan 28, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

**[Iconclass](http://www.iconclass.nl/about-iconclass/what-is-iconclass)** wird
vom Rijksbureau voor Kunsthistorische Documentatie in Den Haag im
Projekt [Mnemosyne](http://www.mnemosyne.org) mit internationaler
Beteiligung weiterentwickelt. Es bietet eine Klassifikation insbesondere
für die ikonogrpahische Beschreibung von Bildinhalten und wird deshalb
meist für die inhaltliche Erschließung von kunsthistorischen Sammlungen
eingesetzt. Als Beispiel für die Verwendung in der Musikikonographie sei
hier auf das [Répertoire International d'Iconographie Musicale (RIdIM)](http://www.ridim-deutschland.de/ridim/index.php?pcontent=startseite) verwiesen,
das für die ikonographische Beschreibung bzw. die Erfassung der
allgemeinen Bildinhalte sowie der musikalischen Kontexte dem
internationalen ICONCLASS-System folgt. Iconclass ist nicht in allen
Sprachen vollständig übersetzt, umfasst aber Terme in Englisch, Deutsch,
Französisch und Italienisch. Der Thesaurus weist zehn Hauptfacetten
auf([^1]):

- abstrakte, ungegenständliche Kunst
- Religion und Magie
- Natur
- der Mensch (allgemein)
- Gesellschaft, Zivilisation, Kultur
- abstrakte Ideen und Konzeptionen
- Geschichte
- Bibel
- Literatur
- Klassische Mythologie und Antike Geschichte


[^1]: Die deutsche Übersetzung wird vom Bildarchiv Marburg und der Herzog
August Bibliothek in Wolfenbüttel
betreut: [http://www.hab.de/bibliothek/kataloge/image-browser.htm](http://www.hab.de/bibliothek/kataloge/image-browser.htm)
