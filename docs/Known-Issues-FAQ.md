# DARIAH-DE public : Known Issues / FAQ

last modified on Mai 18, 2017

The purpose of this page is to address common problems or errors occuring in various components of the DARIAH AAI.

## 1. ELException when trying to login at the DARIAH IdP

**Issue:** When trying to login at the DARIAH IdP, the following error occurs:

``` syntaxhighlighter-pre
org.springframework.binding.expression.EvaluationException: An ELException occurred getting the value for expression 'authenticationContext.setAttemptedFlow(thisFlow)' on context [class org.springframework.webflow.engine.impl.RequestControlContextImpl]
```

![](attachments/Known-Issues-FAQ/55515878.png)

**Explanation**: This is a known issue with the Shibboleth IdP software,
the DARIAH IdP is based on. We reported this issue in April 2017 and
currenctly there is an open bug regarding this error
(cf. [https://issues.shibboleth.net/jira/projects/IDP/issues/IDP-1163](https://issues.shibboleth.net/jira/projects/IDP/issues/IDP-1163)).

**Workaround:** Until this bug is fixed, the only solution is to reload
the page. Since this error happens very rarely, it should be possible to
login properly now.
