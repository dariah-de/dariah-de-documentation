# MEI

last modified on Feb 23, 2014

[3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)

Zur Entstehungsgeschichte des Standards **MEI** heißt es auf der
Homepage der **[Music Encoding Initiative](http://music-encoding.org)**:

  *Seeing the need for a comprehensive markup language in the academic music community, in 1999 Perry Roland of the University of Virginia set about creating an XML schema (DTD) for the representation of music notation. Eventually this DTD became known as MEI because it was influenced by the same principles that guided the creation of the Text Encoding Initiative (TEI)."*([^1])

Seit 2007 wird die Entwicklung des Formats von einer internationalen
Gruppe von Musikwissenschaftlern, -bibliothekaren, Editoren und
Informatikern, dem sogenannten MEI Council, gesteuert. Dieses hat im Mai
2013 eine neue, durch Nutzung des gleichen technischen Unterbaus
unmittelbar mit TEI kombinierbare Version **MEI 2013** vorgelegt. Auch
daran wird die ausdrückliche (aber nicht ausschließliche) Ausrichtung
des Codierungsformats auf den wissenschaftlichen Bereich hin deutlich;
zugleich soll mit der modularen Struktur des Formats ein breites
Spektrum des schriftlich überlieferten Repertoires sowie
wissenschaftlicher Einsatzszenarien abgedeckt werden. Neben dem
starken **Fokus auf digitale Editionen** und entsprechenden
Auszeichnungsmöglichkeiten zur formalisierten Erfassung editorischer
Sachverhalte ist MEI ausdrücklich auch auf eine **musikbibliothekarische
Nutzung** hin konzipiert. Einen ersten Eindruck der auch in diesem
Bereich sehr umfangreichen Möglichkeiten der Metadatenauszeichnung in
MEI kann das folgende, aus Gründen besserer Übersichtlichkeit stark
gekürzte Beispiel zu Schumanns Lied [*Der
Abendstern*](attachments/20059251/22118460.jpg), vermitteln (eine
vollständige Codierung des Liedes findet sich im Anhang).([^2])

Die **Metadaten** werden dabei in einem eigenen Bereich, dem **meiHead** ausgezeichnet:

**Beispiel für die Struktur der Metadaten in MEI** Quelle erweitern

``` syntaxhighlighter-pre
<mei xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.music-encoding.org/ns/mei"
    meiversion="2012">
   <meiHead>
      <fileDesc>
         <titleStmt>
            <title>Der Abendstern: an electronic transcription</title>
            <respStmt>
               <resp>Composed by:</resp>
               <persName role="composer" authority="GND" authURI="http://d-nb.info/gnd"
                         dbkey="118611666">Robert Schumann</persName>
               <resp>Machine-readable transcription by:</resp>
               <persName role="encoder">Kristina Richts</persName>
            </respStmt>
         </titleStmt>
         <pubStmt>
            <unpub/>
         </pubStmt>
         <sourceDesc>
            <source n="1" xml:id="op79">
               <titleStmt>
                  <title type="main">Lieder-Album für die Jugend für Singstimme(n) und Klavier op. 79</title>
                  <title type="uniform">Lieder für die Jugend</title>
                  <respStmt>
                     <persName role="composer" authority="GND" authURI="http://d-nb.info/gnd"
                               dbkey="118611666">Robert Schumann</persName>
                     <resp>Herausgegeben von:</resp>
                     <persName role="editor" authURI="http://d-nb.info/gnd" authority="GND"
                               dbkey="10400097X">Ulrich Malert</persName>
                  </respStmt>
               </titleStmt>
               <editionStmt>
                  <edition>Reprint der Erstausgabe Leipzig 1849</edition>
               </editionStmt>
               <pubStmt>
                  <respStmt/>
                  <address/>
                  <identifier type="ordernumber">Breitkopf & Härtel 8307</identifier>
                  <availability>
                     <useRestrict>Copyright 1991 by Breitkopf & Härtel, Wiesbaden</useRestrict>
                  </availability>
               </pubStmt>
               <physDesc/>
               <seriesStmt/>
               <classification/>
               <contents/>
            </source>
         </sourceDesc>
      </fileDesc>
      <workDesc/>
   </meiHead>
   <music>
      <!--- Codierung des eigentlichen Notentexts -->
   </music>
</mei>      
```

Mittlerweile existieren eine Reihe von **Mappingmöglichkeiten** zwischen
MEI und anderen Datenformaten, so etwa von MARC nach MEI, von Humdrum
\*\*kern nach MEI, von MEI nach ABC, von MEI nach MusicXML oder von MEI
nach Dublin Core. Diese Mappings befinden sich teilweise noch in der
Entwicklung und sind daher noch nicht vollständig über die MEI-Website
abrufbar.

Die **Codierung der Musik** erfolgt im **music**-Bereich. MEI bietet
hier mit Hilfe der Elemente \<facsimile\>, \<performance\>, \<front\>,
\<body\>, \<back\> und \<group\> verschiedene Möglichkeiten der
strukturierten Erfassung von musikalischen Daten. Dabei dienen die
Elemente \<facsimile\> und \<performance\> zur Einbindung bzw.
Verlinkung auf externe Quellen wie etwa Faksimiles oder Aufführungen.
(front/back) Mit Hilfe des Elements \<group\> lassen sich mehrere
musikalische Einheiten zusammenfassen. Dabei bleibt offen, wie eine
solche Einheit zu definieren ist. Die Erfassung der eigentlichen
musikalischen Daten (also der musikalischen Notate) erfordert das
\<body\>-Element, das wiederum ein oder mehrere musical divisions
(**\<mdiv/\>**) beinhalten kann. Innerhalb einer solchen musical
division ist es dem Nutzer erlaubt, einzelne Stimmen (**\<parts/\>**)
oder ganze Partituren (**\<score/\>**) zu codieren. Angaben zum
Schlüssel sowie zur Tonart und Taktart des musikalischen Stückes werden
vor Beginn der Notenauszeichnung in einer sogenannten score definition
(**\<scoreDef/\>**) codiert (vgl. Z.5–17). Innerhalb einer einzigen
Datei können sowohl die Partitur als auch die einzelnen Stimmen erfasst
werden. Innerhalb des \<score/\>-Elements repräsentieren mehrere
**\<staff/\>**-Elemente die einzelnen Stimmen. Zu jeder Stimme gehört
eine eigene staff definition (**\<staffDef/\>**), die die
Basisinformationen zur Taktart, zum Schlüssel (Art und Platzierung des
Schlüssels) sowie die Vorzeichnung definiert. Einzelne staff definitions
lassen sich zu Gruppen zusammenfassen, also etwa die zwei Systeme einer
Klavierbegleitung oder die Streicherstimmen.

Die einzelnen Takte eines Notentextes erfordern zunächst ein
**\<section\>**-Element (vgl. Z. 18), mit dem der Text in ein oder
mehrere Einheiten eingeteilt werden kann. Grundsätzlich werden
Notentexte in MEI taktweise und innerhalb der einzelnen Takte
stimmenweise erfasst. Jedes \<section\>-Element beinhaltet daher eine
bestimmte Anzahl an **\<measure\>**-Elementen. Die einzelnen Takte
können mit einer Zählung versehen werden (vgl. etwa Z. 53: \<measure
n="1"\>). Gleiches gilt für die einzelnen Systeme, von denen hier drei
in den **\<staff\>**-Elementen n="1" bis n="3" erfasst werden. Das
\<layer\>-Element (vgl. etwa Z. 21) dient zur Codierung einzelner
Stimmen innerhalb eines Systems. Dabei ist mindestens ein
**\<layer\>**-Element erforderlich, beliebig viele weitere können
hinzugefügt werden. Im \<layer\>-Element enthalten sind die Codierungen
der einzelnen Noten (\<note/\>), die jeweils durch Attribute
spezifiziert werden. So enthält ein **\<note\>**-Element standardmäßig
meist ein Attribut *@pname* für den Namen des Tons, ein Attribut *@oct*
für die Tonhöhe oder ein Attribut *@dur* für den Notenwert. Es sind noch
eine Reihe weiterer Attribute zur näheren Beschreibung einer Note
vorhanden, doch sollen diese aus Gründen der Übersichtlichkeit hier
nicht näher beschrieben werden. Wie sich etwa an den Zeilen 102 bis 107
erkennen lässt, können in MEI z.B. Akkorde sehr einfach erfasst werden,
indem ein **\<chord\>**-Element um die zum Akkord gehörigen Noten
geschachtelt wird. Dieses Prinzip greift u.a. auch bei der Balkensetzung
(**\<beam/\>**) (vgl. dazu Z. 108 bis 119). Textunterlegung kann in MEI
auf verschiedene Weisen erfolgen, von denen im nachfolgenden Beispiel
lediglich eine aufgeführt wird. Durch die Codierung eines
\<verse\>-Elements innerhalb eines \<note\>-Elements kann der Text hier
direkt einer Note zugewiesen werden. Das **\<verse\>**-Element kann je
nach Verszahl beliebig oft wiederholt werden. Die einzelnen Silben
werden innerhalb eines \<syl\>-Elements im \<verse\>-Element codiert
(vgl. dazu etwa die Z. 22 bis 35). Im Bereich der CMN kann MEI so
ziemlich jedes musikalische Phänomen erfassen. So sind etwa
differenzierte Codierungsmöglichkeiten für die Erfassung von
Bogensetzung, Dynamikangaben oder Crescendo- und Decrescendo-Gabeln
vorhanden (vgl. dazu etwa Z. 48 bis 51). Darüber hinaus gibt es
Layout-Parameter, die dafür vorgesehen sind, musikalische Phänome
(sofern erforderlich) genau zu positionieren.

**Beispiel für die Codierung des Notentextes in MEI** Quelle erweitern

``` syntaxhighlighter-pre
<music>
   <body>
      <mdiv n="1">
         <score>
            <scoreDef>
               <pgHead>
                  <title>KINDERLIEDER<lb/>(<persName authority="GND" authURI="http://d-nb.info/gnd" dbkey="118552589">August Heinrich Hoffmann von Fallersleben</persName>.)</title>
                  <title>1.<lb/>DER ABENDSTERN</title>
               </pgHead>
               <staffGrp symbol="line">
                  <staffDef n="1" label="Singstimme" lines="5" meter.count="2" meter.unit="4" clef.shape="G" clef.line="2" key.sig="3s"/>
                  <staffGrp label="Pianoforte" symbol="brace">
                     <staffDef n="2" lines="5" meter.count="2" meter.unit="4" clef.shape="G" clef.line="2" key.sig="3s"/>
                     <staffDef n="3" lines="5" meter.count="2" meter.unit="4" clef.shape="F" clef.line="4" key.sig="3s"/>
                  </staffGrp>
               </staffGrp>
            </scoreDef>
            <section>
               <measure n="0" metcon="false">
                  <staff n="1">
                     <layer>
                        <note pname="a" oct="4" dur="8">
                           <verse n="1" label="V. 1.">
                              <syl>1. Du</syl>
                           </verse>
                           <verse n="2" label="V. 2.">
                              <syl>Wie</syl>
                           </verse>
                           <verse n="3" label="V. 3.">
                              <syl>So</syl>
                           </verse>
                           <verse n="4" label="V. 4.">
                              <syl>Wie</syl>
                           </verse>
                        </note>
                     </layer>
                  </staff>
                  <staff n="2">
                     <layer>
                        <note pname="a" oct="4" dur="8"/>
                     </layer>
                  </staff>
                  <staff n="3">
                     <layer>
                        <note pname="a" oct="3" dur="8"/>
                     </layer>
                  </staff>
                  <slur tstamp="0.5" curvedir="below" dur="1m+2.5" staff="2"/>
                  <dynam place="below" tstamp="0.5" staff="1">p</dynam>
                  <dir place="above" staff="1" tstamp="0">Langsam.</dir>
                  <dynam place="below" tstamp="0.5" staff="2">p</dynam>
               </measure>
               <measure n="1">
                  <staff n="1">
                     <layer>
                        <note pname="c" oct="5" dur="4">
                           <verse n="1">
                              <syl con="d" wordpos="i">lieb</syl>
                           </verse>
                           <verse n="2">
                              <syl>lieb'</syl>
                           </verse>
                           <verse n="3">
                              <syl>blick'</syl>
                           </verse>
                           <verse n="4">
                              <syl>nickst</syl>
                           </verse>
                        </note>
                        <note pname="b" oct="4" dur="8">
                           <verse n="1">
                              <syl con="d" wordpos="m">li</syl>
                           </verse>
                           <verse n="2">
                              <syl>ich</syl>
                           </verse>
                           <verse n="3">
                              <syl>ich</syl>
                           </verse>
                           <verse n="4">
                              <syl>du</syl>
                           </verse>
                        </note>
                        <note pname="a" oct="4" dur="8">
                           <verse n="1">
                              <syl wordpos="t">cher</syl>
                           </verse>
                           <verse n="2">
                              <syl>doch</syl>
                           </verse>
                           <verse n="3">
                              <syl>nach</syl>
                           </verse>
                           <verse n="4">
                              <syl>mir</syl>
                           </verse>
                        </note>
                     </layer>
                  </staff>
                  <staff n="2">
                     <layer>
                        <chord dur="4">
                           <note pname="c" oct="4"/>
                           <note pname="e" oct="4"/>
                           <note pname="a" oct="4"/>
                           <note pname="c" oct="5"/>
                        </chord>
                        <beam>
                           <chord dur="8">
                              <note pname="d" oct="4"/>
                              <note pname="e" oct="4"/>
                              <note pname="g" oct="4"/>
                              <note pname="b" oct="4"/>
                           </chord>
                           <chord dur="8">
                              <note pname="e" oct="4"/>
                              <note pname="a" oct="4"/>
                           </chord>
                        </beam>
                     </layer>
                  </staff>
                  <staff n="3">
                     <layer>
                        <chord dur="4">
                           <note pname="a" oct="2"/>
                           <note pname="e" oct="3"/>
                           <note pname="a" oct="3"/>
                        </chord>
                        <beam>
                           <chord dur="8">
                              <note pname="b" oct="2"/>
                              <note pname="e" oct="3"/>
                           </chord>
                           <chord dur="8">
                              <note pname="c" oct="3"/>
                              <note pname="e" oct="3"/>
                              <note pname="a" oct="3"/>
                           </chord>
                        </beam>
                     </layer>
                  </staff>
               </measure>
               ...
```

Eine **Dokumentation** des Codierungsformats liegt mit den [MEI Guidelines](http://music-encoding.org/documentation/guidelines2013/chapters)
vor, die über die **Website** der Music Encoding Initiative online
verfügbar sind und auch als
[PDF](http://music-encoding.googlecode.com/files/MEI_Guidelines_2013_v2.1.0.pdf)
abgerufen werden können. Weitere Hilfestellungen zum Umgang mit MEI in
der Praxis bieten die **Online-Tutorials [MEI 1st](http://music-encoding.org/support/MEI1st)**, die ebenfalls auf der
Website zur Verfügung stehen. Weitere
[MEI-Beispielcodierungen](http://music-encoding.org/documentation/samples)
finden sich auf der Website unter dem Menüpunkt
[Dokumentation](http://music-encoding.org/documentation). Das Format
wird durch die **Community** beständig weiter entwickelt. Es wurde eine
internationale Mailingliste
[MEI-L](http://music-encoding.org/support/mailingList) eingerichtet, auf
der aktuelle Themen diskutiert werden.


[^1]: Vgl. dazu: [http://music-encoding.org/about/history](http://music-encoding.org/about/history).

[^2]: Das Beispiel wurde im Rahmen des DFG/NEH-Projekts Digital Music Notation Data Model and Prototype Delivery System erstellt; zur vollständigeren Wiedergabe vgl. Anhang [7.4 MEI Beispiel](7.4-MEI-Beispiel.md).
