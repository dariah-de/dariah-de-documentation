# MuseData

last modified on Feb 24, 2014

[3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)

**[MuseData](http://musedata.org/)** wurde von Walter
P. Hewlett als eines der ersten Projekte am 1983 gegründeten [Center for Computer Assisted Research in the Humanities, Stanford (CCARH)](http://www.ccarh.org/) entwickelt. Es handelt sich dabei um
eine softwareunabhängige Datenstruktur, mit deren Hilfe musikalische
Inhalte gespeichert werden können. MuseData wurde vom CCARH vor allem
zur Digitalisierung bekannter klassischer Werke genutzt. Das Format ist
sehr vielseitig angelegt, eignet sich sowohl für die Erfassung von
Notation und Klang, erhebt jedoch keinen Anspruch auf Vollständigkeit. 

Die Auszeichnung von **Metadaten** erfolgt in MuseData innerhalb der
ersten zwölf Zeilen des Kopfbereichs (Record 1–12). Diese sehr begrenzte
Möglichkeit der Angabe von Metadaten (die ggf. durch weitere, frei
definierbare Records ergänzt werden kann) ist im folgenden Codebeispiel
durch Benennung der entsprechenden Elemente erkennbar. Die hier nicht
genutzten ersten drei Zeilen/Records können für Angaben des Copyrights
und weitere identifizierbare Informationen genutzt werden. Nachfolgend
ist eine Beispielcodierung der Metadaten des Lieds *[Der
Abendstern](attachments/20059198/22118461.jpg)* von Robert Schumann
abgebildet.

**Beispiel für die Auszeichnung von Metadaten in MuseData** Quelle
erweitern

``` syntaxhighlighter-pre
09/04/12 K. Herold
WK#:79 MV#:1
Reprint der Erstausgabe Leipzig 1849
Liederalbum für die Jugend
Der Abendstern
Pianoforte
Lied
group membership: score
score: part 1 of 29
[frei definierbar]
```

Zusätzlich zu den verbindlichen zwölf Feldern des o.g. Beispiels stehen
noch weitere Auszeichnungsfelder optional zur Verfügung. Diese sollten
auch genutzt werden, denn es fehlt unter anderem die obligatorische
Belegung eines Autor/Komponisten-Feldes. Bei den Codierungen des CCARH
werden diese Daten in der Regel in den drei ersten Zeilen (im obigen
Beispiel leer) abgelegt. Da diese Zeilen, ebenso wie die frei
definierbaren ab Record 13 jedoch nicht standardisiert sind, erschwert
dies den Austausch zwischen verschiedenen Institutionen, die diese
Felder in projektspezifischer Weise nutzen.

**Beispiel für die Codierung des Notentextes (Singstimme) in MuseData** Quelle erweitern

``` syntaxhighlighter-pre
02/01/14 K. Richts
WK#:79       MV#:1
Liederalbum für die Jugend
Der Abendstern
Singstimme
 
Group memberships: score
score: part 1 of 2
$ K:3  Q:2  T:2/4  C:0
A4   1          e     u                    Du|Wie|So|Wie
measure 1
C#5  2          q     d                    lieb-|lieb'|blick'|nickst
B4   1          e     d                    -li-|ich|ich|du
A4   1          e     u                    -cher|doch|nach|mir
measure 2
```

Alle Stimmen eines Werkes werden in separaten Dateien erfasst. Dabei
beginnt jede Datei mit der Erfassung von Metadaten zum Titel des
codierten Stückes (s.o.). Anschließend erfolgt die Codierung des
eigentlichen Notentextes in aufeinanderfolgenden Zeilen
(*records*). Informationen zur Tonart, Schlüsselung und Taktart eines
Werkes werden dabei in einer gemeinsamen Zeile (*record*) erfasst (vgl.
Z. 11) und mit einem "$" gekennzeichnet. Der erste Eintrag in Zeile 12
beschreibt den ersten Ton der Singstimme. Es handelt sich hier um ein
eingestrichenes A mit dem Wert einer Achtel (erkennbar aus dem "e" im
gleichen Record). Die Halsrichtung wird in MuseData
mit einem "u" ("up") oder "d" ("down") erfasst. Möglichkeiten zur
Textauszeichnung sind in diesem Datenformat nicht gegeben, daher werden
alle Silben, die auf einen Ton fallen, nebeneinander codiert und durch
ein Verkettungszeichen ("\|") voneinander getrennt.

Schwieriger gestaltet sich dagegen schon die Codierung einer
Klavierstimme. Akkorde (bzw. gleichzeitig klingende Töne) werden
erfasst, indem die einzelnen Töne wie gehabt in separaten records
untereinander codiert werden. Der erste Ton eines Akkords wird dabei wie
gewohnt codiert, die Codierung aller weitere Töne eines Akkords
erfordert ein vorangestelltes Leerzeichen (vgl. etwa die Zeilen 18 bis
20). Tonerhöhungen werden in MuseData durch eine Raute "#" erfasst (vgl.
etwa Z. 17 "C#5" oder Z. 20 "C#4"). Die Codierung von Balken erfolgt
direkt an den entsprechenden Noten über die Angabe eines "\["
(beginnender Balken) bzw. "\]" (endender Balken). Analog dazu werden
Bindebögen mit "(" (Beginn des Bindebogens) bzw. ")" (Ende des
Bindebogens) erfasst.

Mehrere Stimmen eines Systems werden in MuseData mit Hilfe sogenannter
*backups* codiert, mit denen die *division pointer* eines Taktes bis zu
der Zählzeit zurückgesetzt werden können, an der eine weitere Stimme
einsetzt. Ein Beispiel hierfür findet sich etwa in Zeile 14 der
Beispielcodierung.

**Beispiel für die Codierung des Notentextes (Klavierstimme) in MuseData** Quelle erweitern

``` syntaxhighlighter-pre
02/01/14 K. Richts
WK#:79       MV#:1
Liederalbum für die Jugend
Der Abendstern
Pianoforte
 
Group memberships: score
score: part 2 of 2
$ K:3  Q:2  T:2/4  C:0
A4   1        1 e     u        (
back 1
A3   1        1 e     d 
measure 1
C#5  2        1 q     u 
 A4  2        2 q     u 
 E4  2        3 q     u 
 C#4 2        4 q     u 
B4   1        1 e     u        [
 G4  1        1 e     u 
 E4  1        1 e     u 
 D4  1        1 e     u 
A4   1        1 e     u        ]
 E4  1        1 e     u        )
back 4
A3   2        1 q     d 
 E3  2        1 q     d 
 A2  2        1 q     d 
E3   1        1 e     d 
 B2  1        1 e     d        [
A3   1        1 e     d 
 E3  1        1 e     d 
 C#3 1        1 e     d        ]
measure 2
```

Auch wenn MuseData über einige Schnittstellen bzw. Konverter zu anderen
Formaten wie etwa Lilypond oder Humdrum verfügt, hat das Datenformat aus
heutiger Perspektive insgesamt nur noch "einen historischen
Wert". Das Format wird nicht weiterentwickelt und ist
auch für editorische Zwecke nicht geeignet.


[^1]: Siehe auch: Eleanor Selfridge-Field (Hg.), *Beyond Midi: The Handbook of Musical Codes*, Cambridge 1997; Johannes Kepper, *Musikedition im Zeichen neuer Medien, Historische Entwicklung und gegenwärtige Perspektiven musikalischer Gesamtausgaben*, Norderstedt 2011, S.
402–442; speziell zum Header lediglich S. 406–408.
[^2|: Die Abkürzung "q" steht entsprechend für eine Viertelnote ("quarter").
[^3]: Vergleiche dazu Johannes Kepper, *Musikedition im Zeichen neuer Medien*, S. 318.
