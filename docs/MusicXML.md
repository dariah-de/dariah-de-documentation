# MusicXML

last modified on Feb 23, 2014

[3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)

**[MusicXML](http://www.makemusic.com/musicxml)** ist vornehmlich
als **Austauschformat** zwischen verschiedenen proprietären und freien
Notensatzprogrammen im Einsatz, eignet sich jedoch nicht als
langfristiges Speicherformat für wissenschaftliche Zwecke. Es wurde seit
1998 maßgeblich von Michael Good entwickelt und basiert hauptsächlich
auf den beiden Datenformaten MuseData und Humdrum. Im November 2011
wurde das Datenformat an den Hersteller des Notensatzprogramms Finale
verkauft und hat sich aus diesem Grund für einen Einsatz im Bereich des
open access disqualifiziert.

Das Datenformat stellt **fünf top-level Elemente** für die Auszeichnung
von Metadaten
bereit: *work*, *movement-number*, *movement-title*, *identification* und *part-list*.
Nur das letztgenannte Element *part-list* ist obligatorisch, da es für
eine Ordnung und Benennung der nachfolgenden Codierung des musikalischen
Inhalts der Notenzeilen verantwortlich ist. Alle anderen Elemente sind
optional. Verglichen mit der Detailliertheit der Codierungen des
eigentlichen Notentextes wirkt der header-Bereich von MusicXML sehr
eingeschränkt; dies hängt jedoch mit der primären Zielsetzung des
Codierungsformats zusammen: 

**Beispiel für die Codierung der Metadaten in MusicXML** Quelle
erweitern

``` syntaxhighlighter-pre
<score-partwise version="3.0">
   <work>
      <work-number>op. 79</work-number>
      <work-title>Liederalbum für die Jugend</work-title>
   </work>
   <movement-number>1</movement-number>
   <movement-title>Der Abendstern</movement-title>
   <identification>
      <creator type="composer">Robert Schumann</creator>
      <creator type="lyricist">August Heinrich Hoffmann von Fallersleben</creator>
      <rights>Copyright 2012 Music Encoding Initiative</rights>
      <encoding>   
         <encoding-date>2012-08-15</encoding-date>
         <encoder>Kristina Richts</encoder>
         <encoding-description>MusicXML 1.0 example</encoding-description>
      </encoding>
      <source>Based on the reprint of the First Printing Leipzig 1849</source>
   </identification>
   <credit page="1">
      <credit-type>page number</credit-type>
      <credit-words>3</credit-words>
   </credit>
   <credit page="1">
      <credit-type>title</credit-type>
      <credit-words>Kinderlieder. (Hoffmann von Fallersleben.)</credit-words>
   </credit>
   <credit page="1">
      <credit-type>title</credit-type>
      <credit-words>1. Der Abendstern.</credit-words>
   </credit>
   <credit page="1">
      <credit-type>plate number</credit-type>
      <credit-words>8062</credit-words>
   </credit>
   <part-list>
      <score-part id="P1">
         <part-name>Singstimme.</part-name>
      </score-part>
      <score-part id="P2">
         <part-name>Pianoforte.</part-name>
      </score-part>
   </part-list>
   <part id="P1">
      <measure number="1"/>
   </part>
</score-partwise>
```

Nachfolgend ein Codierungsbeispiel, das den Auftakt und den ersten Takt
des Liedes *[Der Abendstern](attachments/20059228/26443879.jpg)* zeigt.
Für die Erläuterung der einzelnen Elemente und Attribute sei an dieser
Stelle auf ein [Beispiel der MusicXML Internetpräsenz](http://www.musicxml.com/tutorial/hello-world/) verwiesen.
Angelehnt an ein *helloworld* Beispiel aus der
Programmierung, wird hier eine einzelne Note codiert und der Quelltext
sehr gut nachvollziehbar beschrieben.

**Beispiel für die Codierung des Notentextes in MusicXML** Quelle
erweitern

``` syntaxhighlighter-pre
<part id="P_1">
   <measure number="0">   
      <attributes>
         <divisions>8</divisions>
         <key>
            <fifths>3</fifths>
            <mode>major</mode>
         </key> 
         <time>
            <beats>2</beats>
            <beat-type>4</beat-type>
         </time>
      </attributes>
      <note>
         <pitch>
            <step>A</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <voice>1</voice>
         <type>eighth</type>
         <lyric number="1">
            <syllabic>single</syllabic>
            <text>1. Du</text>
         </lyric>
         <lyric number="2">
            <syllabic>single</syllabic>
            <text>Wie</text>
         </lyric>
         <lyric number="3">
            <syllabic>single</syllabic>
            <text>So</text>
         </lyric>
         <lyric number="4">
            <syllabic>single</syllabic>
            <text>Wie</text>
         </lyric>
      </note>
   </measure>
   <measure number="1">
      <note>
         <pitch>
            <step>C</step>
            <alter>1</alter>
            <octave>5</octave>
         </pitch>
         <duration>4</duration>
         <voice>1</voice>
         <type>quarter</type>
         <lyric number="1">
            <syllabic>single</syllabic>
            <text>lieb_</text>
         </lyric>
         <lyric number="2">
            <syllabic>single</syllabic>
            <text>lieb'</text>
         </lyric>
         <lyric number="3">
            <syllabic>single</syllabic>
            <text>blick'</text>
         </lyric>
         <lyric number="4">
            <syllabic>single</syllabic>
            <text>nickst</text>
         </lyric>
      </note>
      <note>
         <pitch>
            <step>B</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <lyric number="1">
            <syllabic>single</syllabic>
            <text>li_</text>
         </lyric>
         <lyric number="2">
            <syllabic>single</syllabic>
            <text>ich</text>
         </lyric>
         <lyric number="3">
            <syllabic>single</syllabic>
            <text>ich</text>
         </lyric>
         <lyric number="4">
            <syllabic>single</syllabic>
            <text>du</text>
         </lyric>
      </note>
      <note>
         <pitch>
            <step>A</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <lyric number="1">
            <syllabic>single</syllabic>
            <text>cher</text>
         </lyric>
         <lyric number="2">
            <syllabic>single</syllabic>
            <text>doch</text>
         </lyric>
         <lyric number="3">
            <syllabic>single</syllabic>
            <text>nach</text>
         </lyric>
         <lyric number="4">
            <syllabic>single</syllabic>
            <text>mir</text>
         </lyric>
      </note>
   </measure>   
<!--=========================================-->
<part id="P2">   
   <measure number="0">
      <attributes>
         <divisions>8</divisions>
         <key>
            <fifths>3</fifths>
            <mode>major</mode>
         </key>
         <time>
            <beats>2</beats>
            <beat-type>4</beat-type>
         </time>
         <staves>2</staves>
         <clef number="1">
            <sign>G</sign>
            <line>2</line>
         </clef>
         <clef number="2">
            <sign>F</sign>
            <line>4</line>
         </clef>
      </attributes>
      <direction placement="below">
         <direction-type>
            <dynamics>
               <p/>
            </dynamics>
         </direction-type>
         <staff>1</staff>
      </direction>
      <note>
         <pitch>
            <step>A</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <staff>1</staff>
         <notations>
            <slur number="1" placement="below" type="start"/>
         </notations>
      </note>
      <backup>
         <duration>2</duration>
      </backup>
      <note>
         <pitch>
            <step>A</step>
            <octave>3</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
      </note>
   </measure>
   <measure number="1">
      <note>
         <pitch>
            <step>C</step>
            <alter>1</alter>
            <octave>4</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>up</stem>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>E</step>
            <octave>4</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>up</stem>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>A</step>
            <octave>4</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>up</stem>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>C</step>
            <alter>1</alter>
            <octave>5</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>up</stem>
      </note>
      <note>
         <pitch>
            <step>D</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">begin</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>E</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">begin</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>G</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">begin</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>B</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">begin</beam>
      </note>
      <note>
         <pitch>
            <step>E</step>
            <octave>4</octave> 
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">end</beam>
         <staff>1</staff>
         <notations>
            <slur number="1" placement="below" type="end"/>
         </notations>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>A</step>
            <octave>4</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>up</stem>
         <beam number="1">end</beam>
      </note>
      <backup>2</backup>
      <note>
         <pitch>
            <step>A</step>
            <octave>2</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>down</stem>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>E</step>
            <octave>3</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>down</stem>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>A</step>
            <octave>3</octave>
         </pitch>
         <duration>4</duration>
         <type>quarter</type>
         <stem>down</stem>
      </note>
      <note>
         <pitch>
            <step>B</step>
            <octave>2</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
         <beam number="2">begin</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>E</step>
            <octave>3</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
         <beam number="2">begin</beam>
      </note>
      <note>
         <pitch>
            <step>C</step>
            <alter>1</alter>
            <octave>3</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
         <beam number="2">end</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>E</step>
            <octave>3</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
         <beam number="2">end</beam>
      </note>
      <note>
         <chord/>
         <pitch>
            <step>A</step>
            <octave>3</octave>
         </pitch>
         <duration>8</duration>
         <type>eigth</type>
         <stem>down</stem>
         <beam number="2">end</beam>
      </note>
   </measure>
</part>
```


[^1]:*Hello World* ist ein kleines Computerprogramm, das oft verwendet
wird, um neue Programmiersprachen zu erlernen. Es zeigt die Bestandteile
der Syntax auf, die erforderlich sind, um ein lauffähiges Programm zu
erstellen. Die Funktionsfähigkeit des Programms wird durch die Ausgabe
*helloworld* bestätigt.
