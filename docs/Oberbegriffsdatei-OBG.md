# Oberbegriffsdatei (OBG)

last modified on Jan 14, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

Die **[Oberbegriffsdatei (OBG)](http://museum.zib.de/museumsvokabular/documents/obg.pdf)** ist
in 45 Hauptgruppen und darunter liegende Untergruppen aufgeteilt, von
Aneignende Wirtschaft über Architektur, Behältnis, Beleuchtung, Besteck,
Bildung, Elektrotechnisches Gerät, Gefäß, Graphik/Photographie, Hausrat,
Gesundheitswesen, Körperpflege, Hygiene, Kleidung, Landwirtschaftliches
Gerät, Landwirtschaftliche Maschine, Maschinenwesen, Meßgerät, Möbel,
Münze, Medaille, Zahlungsmittel, Musikinstrument, Klangkörper, Schmuck,
Spiel, Spielzeug, Textilie, Waffe, Werkzeug, Gerät bis Zunftzeichen.
Diese Systematik ist derzeit bei über 150 Museen in Deutschland in
Gebrauch. Seit 2008 wird die OBG im webbasierten
Vokabularverwaltungstool xTree geführt. Die redaktionelle Überarbeitung
erfolgt in einer überregionalen Arbeitsgruppe, bestehend aus:

- Landesstelle für die nichtstaatlichen Museen in Bayern, München
- Institut für Museumsforschung, Berlin
- digiCULT-Verbund eG, Kiel
- Stiftung Historische Museen Hamburg - Museum der Arbeit
- LWL-Freilichtmuseum Hagen - Landesmuseum für Handwerk und Technik
