# Object Names Thesaurus und Materials Thesaurus des British Museum

last modified on Feb 17, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

1980 begann das [British Museum](http://www.britishmuseum.org) seine
Terminologie zur Verschlagwortung zu strukturieren und erarbeitete einen
Thesaurus für Objekte und einen Thesaurus für Materialien. Der [Object Names Thesaurus](http://www.collectionslink.org.uk/assets/thesaurus_bmon/Objintro.htm) besteht
aus 33 Hauptgruppen und der [Materials Thesaurus](http://www.collectionslink.org.uk/assets/thesaurus_bmm/matintro.htm?phpMyAdmin=OYNyINPdn3sQmoXugKH1gcCLSW0) gliedert
sich in organische, anorganische und Werkstoffe.

- agricultural/subsistence
- animal equipment
- animal/vegetal remains
- architecture
- arms/armour
- artefact
- childcare equipment
- communications equipment
- component
- container
- costume
- culinary equipment
- currency
- fire/fire-lighting equipment
- food product
- furniture/furnishings
- games/sporting equipment
- instrument
- lighting equipment
- performance arts
- personal ornament
- production/replication
- religious/ritual equipment
- sample
- science/medicine
- sculpture
- social control equipment
- stimulant/narcotic equipment
- textile
- toilet/cosmetic equipment
- transport/carrier
- visual representation
