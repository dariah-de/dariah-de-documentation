# Reports and Milestones

## Projektphase III

### Cluster 1

| Bez.    | Titel                                                                                     |
|---------|-------------------------------------------------------------------------------------------|
| R 1.1.2 | [Usability und Nutzung im Produktivbetrieb](attachments/14651583/64976527.pdf)                     |
| M 1.2.1 | [Workshops TextGrid Nutzertreffen](http://dhd-blog.org/?p=7179)                                    |
| R 1.2.1 | [Disseminationsstrategien für digitale Publikationen](attachments/14651583/87983400.pdf)           |
| M 1.2.2 | [Workshop zu erweiterten Publikationen in den Geisteswissenschaften](https://dhd-blog.org/?p=8696) |
| M 1.2.3 | Workshops TextGrid Dissemination                                                                   |

### Cluster 2

| Bez.    | Titel                                                                                     |
|---------|-------------------------------------------------------------------------------------------|
| M 2.2.1 | [Metahosting in DARIAH-DE](attachments/14651583/M2.2.1_Metahosting_in%20DARIAH-DE_final.pdf)                                 |
| R 2.3.1 | [Anforderungsanalyse für Accounting in DARIAH-DE](attachments/14651583/R2.3.1_Anforderungsanalyse_fuer_Accounting_final.pdf) |
| R 2.3.2 | Konzept für Accounting in DARIAH-DE                                                                                                                                                                             |
| M 2.3.3 | Accounting Infrastruktur für DARIAH-DE                                                                                                                                                                          |

### Cluster 3

| Bez.      | Titel                                                                                   |
|-----------|-----------------------------------------------------------------------------------------|
| M 3.1.1   | [Erster Workshop mit der Geschäftsstelle der Max Weber Stiftung](http://dhd-blog.org/?p=7044)  |
| R 3.1.1   | [Gesamtgeschäftsmodell für DARIAH-DE](Gesamtgeschäftsmodell-fuer-DARIAH-DE.md)                                           |
| M 3.1.2   | [Erste Nachhaltigkeitskonferenz](https://forschungsinfrastrukturen.de/doku.php)                |
| M 3.1.3   | [Produktivphase des Coordination Office DARIAH-DE](http://dhd-blog.org/?p=7924)                |
| M 3.1.4   | [Zweiter Workshop mit der Geschäftsstelle der Max Weber Stiftung](http://dhd-blog.org/?p=8733) |
| M 3.1.5   | [Zweite Nachhaltigkeitskonferenz](https://forschungsinfrastrukturen.de/doku.php)               |
| R 3.1.2.1 | [Disseminationsstrategie](attachments/14651583/58503512.pdf)                                   |
| R 3.1.2.2 | [Disseminationsstrategie (Update)](attachments/14651583/87984889.pdf)                          |
| R 3.1.3.1 | [Zwischenbilanz: Betrieb des Coordination Office DARIAH-DE](attachments/14651583/64960129.pdf) |
| R 3.1.3.2 | [Bilanz: Betrieb des Coordination Office DARIAH-DE](attachments/14651583/87984886.pdf)         |
| M 3.2.1   | Produktivphase der DeISU ist erreicht                                                          |
| R 3.2.2.1 | [Zwischenbilanz Betrieb der DeISU](attachments/14651583/64949933.pdf)                          |
| R 3.2.2.2 | Bilanz Betrieb der DeISU                                                                       |
| R 3.2.2.3 | DeISU Abschlussreport                                                                          |
| M 3.2.3   | Integration Service Provider                                                                   |

### Cluster 4

| Bez.      | Titel                                                                                 |
|-----------|---------------------------------------------------------------------------------------|
| R 4.1.2.1 | [Dokumentation der Ergebnisse der Stakeholdertreffen „Wissenschaftliche Sammlungen“](attachments/14651583/R%204_1_2_1_final.pdf) |
| R 4.1.2.2 | [Dokumentation Stakeholdertreffen „Wissenschaftliche Sammlungen“](http://dhd-blog.org/?p=8714)                    |
| R 4.1.2.3 | [Dokumentation Stakeholdertreffen &bdquo;Wissenschaftliche Sammlungen&ldquo;](attachments/14651583/R%204_1_2_3_Final.pdf)        |
| M 4.1.3.1 | [Workshop I der Workshop-Reihe](http://dhd-blog.org/?p=7618)                                                    |
| M 4.1.3.2 | [Workshop II der Workshop-Reihe](http://dhd-blog.org/?p=7618)                                                     |
| M 4.1.3.3 | [Workshop III der Workshop-Reihe](http://dhd-blog.org/?p=8419)                                                    |
| M 4.1.3.4 | [Workshop IV der Workshop-Reihe](https://doi.org/10.5281/zenodo.582316)                                                     |
| M 4.1.3.5 | [Workshop V der Workshop-Reihe](https://doi.org/10.5282/o-bib/2018H4S287-294)                                                      |
| M 4.1.3.6 | [Workshop VI der Workshop-Reihe](https://dhd-blog.org/?p=10050)                                                     |
| M 4.2.1.1 | [Feedback zu Diensten und Materialien](attachments/14651583/M%204-2-1-1_final.pdf)                                               |
| R 4.2.1.2 | [Vorläufige Ergebnisse](http://dhd-blog.org/?p=8474)                                                              |
| R 4.2.1.3 | [Publikation der Ergebnisse](attachments/14651583/R%204-2-1-3_Final.pdf)                                                         |
| R 4.2.2   | [Publikation der Rückmeldungen und Beobachtungen zu den integrierenden Use-Cases](attachments/14651583/R4.2.2.pdf)    |
| R 4.2.3   | [DARIAH-DE Dienste Primer](http://dhd-blog.org/?p=9082)                                                           |
| M 4.2.4   | [Lizenz-Forum](attachments/14651583/M4-2-4_DARIAHIII_Milestone_Final.pdf)                                                                       |
| R 4.2.5   | [Dokumentation der Forschungsdaten-F&ouml;derationsarchitektur, Handreichungen](attachments/14651583/R4_2_5.pdf)      |
| M 4.3.1   | [Schnittstellenkonzept](attachments/14651583/M4.3.1_final.pdf)                                                              |
| M 4.3.2   | [DSA-Zertifizierung](attachments/14651583/M4.2.3_DSA-Zertifizierung.pdf)                                                                 |
| M 4.3.3.1 | DARIAH-DE Repositorium (Prototyp): [Presseinformation](http://dhd-blog.org/?p=8796), [Blogbeitrag](http://dhd-blog.org/?p=8798)                   |
| M 4.3.3.2 | [TextGrid-Produktivsystem](attachments/14651583/M%204.3.3.2%20TextGrid-Produktivsystem1.pdf)                                                           |
| M 4.3.3.3 | [DARIAH-DE Repositorium (Produktivsystem)](attachments/14651583/M%204.3.3.3%20DARIAH-DE%20Repositorium.pdf)                                           |
| M 4.3.4.1 | [Forschungsdaten-F&ouml;derations-Architektur (Prototyp)](attachments/14651583/M4_3_4_1_Forschungsdatenf%C3%B6derationsarchitektur.pdf)                            |
| M 4.3.4.2 | [Forschungsdaten-F&ouml;derations-Architektur (Produktivsystem)](attachments/14651583/M4_3_4_2.pdf)                     |
| M 4.3.5.1 | [Schulungs- und Dokumentationsportal (Prototyp)](attachments/14651583/M%204.3.5%20Beratungs-%20und%20Schulungsportal.pdf)                                     |
| M 4.3.5.2 | [Schulungs- und Dokumentationsportal (Produktivsystem)](https://de.dariah.eu/schulungs-/lehrmaterialien)                              |

### Cluster 5

| Bez.    | Titel                                                                                     |
|---------|-------------------------------------------------------------------------------------------|
| M 5.1.1 | Schulungsmaterialien Topic Modeling Tool                                                                                    |
| M 5.1.2 | Methodenworkshop 1                                                                                                          |
| M 5.1.3 | Methodenworkshop 2                                                                                                          |
| R 5.2.1 | [Dokumentation Cosmotool](https://dfa.de.dariah.eu/doc/cosmotool/)                                                          |
| M 5.2.1 | [Beta Release Cosmotool](attachments/14651583/55516090.pdf)                                                                 |
| M 5.2.2 | [Expertenkolloquium 1](http://dhd-blog.org/?p=8000)                                                                         |
| M 5.2.3 | [Expertenkolloquium 3](https://dhd-blog.org/?p=9834)                                                                        |
| M 5.3.1 | [Beta Release Topic Modeling Tool](https://github.com/DARIAH-DE/Topics)                                                     |
| M 5.3.2 | Expertenkolloquium 2 (Expertenworkshop Topic-Modeling)                                     |
| M 5.3.3 | Expertenkolloquium 4                                                                                                        |
| R 5.3.1 | [Dokumentation Topic Modeling Tool](http://dev.digital-humanities.de/ci/job/DARIAH-Topics/doclinks/1/docs/gen/modules.html) |

### Cluster 6

| Bez.    | Titel                                                                                     |
|---------|-------------------------------------------------------------------------------------------|
| R 6.1   | [Visualisierung (Semantische Technologien) - Visualsierung von semantisch annotierten Forschungsdaten](attachments/14651583/87984971.pdf)                                      |
| M 6.1.3 | [Visualisierung von Forschungsaktivität](attachments/14651583/58505390.pdf)                                                                                                    |
| M 6.2.1 | [Workshop 1](https://dhd-blog.org/?p=7183) "Geisteswissenschaftliche Forschungsinfrastrukturen und CIDOC CRM Annotation"                                                       |
| M 6.2.2 | Workshop 2 "Bilddaten in den Digitalen Geistes- und Kulturwissenschaften - Interoperabilität und Retrieval" |
| M 6.2.3 | [Workshop 3](https://dhd-blog.org/?p=11198) "Annotieren, analysieren, visualisieren"                                                                                           |

### Konsortialleitung

| Bez.    | Titel                                                                                     |
|---------|-------------------------------------------------------------------------------------------|
| R 7.1   | Gemeinsame Klausurtagung DARIAH-DE und CLARIN-D – Dieser Report ist auf [Anfrage](mailto:info@de.dariah.eu) einzusehen. |
| M 7.2.1 | [Fachkonferenz 2016](https://de.dariah.eu/dariah-de-grand-tour)                                                         |
| M 7.2.2 | [Fachkonferenz 2017](https://dhd-blog.org/?p=7963)                                                                      |
| M 7.2.3 | [Fachkonferenz 2018](https://de.dariah.eu/dariah-de-grand-tour-2018)                                                    |

## Abschlussbericht Projektphase I und II

- [Abschlussbericht DARIAH-DE I/II - Öffentliche Fassung](attachments/14651583/55515689.pdf)

## Projektphase II

### Reports

- [R 1.1.1 Organisation der VCC 4-Aktivitäten, Koordination der Aktivitäten von VCC4 und DARIAH-DE](attachments/14651583/R%201.1.1%20VCC4-Aktivit%C3%A4ten%20Vol%202.pdf?version=2&modificationDate=1453201111398&api=v2)
- [R 1.2.1  /M 7.6 Report Nutzungsverhalten in den Digital Humanities /DH-Bestandsaufnahme](attachments/14651583/Report1.2.1-final3.pdf)
- [R 1.2.2 / R 7.5 K](attachments/14651583/R_1.2.2_Usability_Criteria_for_External_Requests_of_Collaboration.pdf)[riterienkatalog Usability für externe Kooperationsanfragen /Zielgruppenanalyse](attachments/14651583/R1.2.2-7.5_final.pdf)
- [R 1.2.3 Report Usability von DH-Tools und -Services](attachments/14651583/AP1.2.3_Usability_von_DH-Tools_und-Services_final.pdf)
- [R 1.3.1 Erstellung Umfrage](attachments/14651583/R%201.3.1%20-%20Erhebung%20einer%20Nutzerbefragung%20zu%20Nutzererwartungen%20und%20-kriterien.pdf)
- [R 1.3.2 Konzept Report Erfolgskriterien](attachments/14651583/R%201.3.2%20-%20Konzept%20Report%20Erfolgskriterien.pdf)
- [R 1.3.3 Finale Version Erfolgskriterien](attachments/14651583/R133_Erfolgskriterien_Konsortium.pdf)
- [R 2.2.1 Kriterien für die Überführung fachwissenschaftl. Dienste in den Dauerbetrieb und Integration in die DARIAH-DE-Basisinfrastruktur](attachments/14651583/R2.2.1_Kriterien_für_die_Überführung_fachwissenschaftlicher_Dienste_in_den_Dauerbetrieb_und_die_Integration_in_Die_DARIAH-DE-Basisinfrastruktur.pdf)
- [R 2.2.3 Evaluation der in DARIAH-DE entwickelten fachwissenschaftl. Dienste, Übergabe der Ergebnisse inkl. Bericht an die Service-Unit](attachments/14651583/R2.2.3undM2.2.4.pdf?version=2&modificationDate=1458655073159&api=v2)
- [R 2.3.1 Auswahl und Beschreibung der initialen technischen Workflows und Policies für den Data Lifecycle](attachments/14651583/R2.3.1_V4.pdf?version=2&modificationDate=1458655285248&api=v2)
- [R 2.3.2 Kriterien für die Auswahl zusätzlich benötigter technischer Workflows und     Policies](attachments/14651583/R2.3.2%20Kriterien%20f%C3%BCr%20die%20Auswahl%20zus%C3%A4tzlich%20ben%C3%B6tigter%20technischer%20Workflows%20und%20Policies.pdf)
- [R 2.3.4 Bereitstellung technischer Workflows für FachwissenschaftlerInnen und Übergabe an die Service-Unit](attachments/14651583/R2.3.4_final.pdf)
- [R 2.4.2 Übergreifendes Serviceangebot mit Service Level Agreements für DARIAH-DE](attachments/14651583/R2.4.2.pdf)
- [R 3.1.2 Fortschreibung des Arbeitsplans](attachments/14651583/R%203.1.2.pdf)
- [R 3.2.1 Erstellen eines Fragebogens zu fachwissenschaftlichen Anforderungen](attachments/14651583/R_3.2.1_Fragebogen_fuer_Fachwissenschaftler.pdf)
- [R 3.2.2 Erstellen eines Fragebogens zu betrieblichen Anforderungen der Rechenzentren](attachments/14651583/R%203.2.2_Fragebogen_betrieblicher_Anforderungen.pdf)
- [R 3.2.3 Bericht und Analyse der Befragung der FachwissenschaftlerInnen](attachments/14651583/R_3.2.3_-_Analyse_des_Fragebogens_für_Fachwissenschaftler.pdf)
- [R 3.2.4 Bericht und Analyse der Befragung der Rechenzentren](attachments/14651583/R%203.2.4_final_Analyse%20des%20Fragebogens%20zu%20betrieblichen%20Anforderungen.pdf)
- [R 3.2.5 Erstellen eines Kriterienkatalogs für Dienste](attachments/14651583/44827438.pdf)
- [R 3.2.6 Report über die ausgewählten Dienste](attachments/14651583/R3.2.6_final_Report_ueber_die_ausgewaehlten_Dienste.pdf)
- [R 3.2.7 Report zur Aufnahme zukünftiger neuer Dienste](attachments/14651583/R3.2.7.pdf)
- [R 3.3.1 Aufgabenkatalog der DeISU](attachments/14651583/3.3.1_Aufgabenkatalog.v0.6.pdf)
- [R 3.3.2 Analyse der Basisangebote der Rechenzentren](attachments/14651583/R%203.3.2.pdf)
- R 3.3.3 Rechenzentren-Kooperationsmodell – Dieser Report ist auf [Anfrage](mailto:info@de.dariah.eu) einzusehen.
- R 3.3.4 Spezifizierung der Organisationsstruktur der DeISU – Dieser Report ist auf [Anfrage](mailto:info@de.dariah.eu) einzusehen.
- [R 3.3.5 DeISU-Satzung](attachments/14651583/DARIAH-DE-Satzung-v1.0.pdf)
- [R 3.4.1 Marktanalyse](attachments/14651583/R_3.4.1_Marktanalyse.pdf)
- [R 3.4.2 DeISU Geschäftsplan](attachments/14651583/46563345pdf)
- [R 4.2.1 Use-Cases zur Sammlungsmodellierung](attachments/14651583/R%204.2.1%20Use%20Cases%20zur%20Sammlungsmodellierung.pdf)
- [R 4.2.3 Dokumentation theorie- und verfahrensgeleiteter Sammlungskonzepte](attachments/14651583/R%204.2.3%20Dokumentation%20Sammlungskonzepte.pdf)
- [R 4.2.4 Aufbau und Nutzung von wissenschaftlichen Sammlungen](attachments/14651583/R4.2.4-final.pdf)
- [R 4.3.3 Historische Ortstypen als kontrolliertes Vokabular](attachments/14651583/40436364.pdf)
- [R 4.4.3 Juristische Handreichungen](http://webdoc.sub.gwdg.de/pub/mon/dariah-de/dwp-2015-12.pdf)
- [R 5.2.1 Beschreibung der Use Cases](attachments/14651583/R%205.2.1%20%E2%80%93%20Beschreibung%20der%20Use%20Cases.pdf)
- [R 5.2.2 Kommentierte Bibliographie](attachments/14651583/R%205.2.2%20%E2%80%93%20Kommentierte%20Bibliographie.pdf)
- [R 5.2.3 Stand der Forschung Textanalyse](attachments/14651583/R%205.2.3-StandderForschungTextanalyse.pdf)
- [R 5.3.1 Konzept Use Cases](attachments/14651583/R%205.3.1%20-%20Konzept%20Use%20Cases.pdf)
- [R 5.4.1 Lehrmaterialsammlung](attachments/14651583/R%205.4.1%20-%20Konzept%20Dissemination%20und%20Lehrmittelsammlung.pdf)
- [R 5.4.2 Bilanz Lehrmaterialien](attachments/14651583/R%205.4.2%20Bilanz%20Lehrmaterialien%20-%20Final.pdf)
- R.6.2.1 Digitale Annotationen: ‚Best Practices‘ und Potentiale ([Teil I](attachments/14651583/43221080.pdf) und [Teil II](attachments/14651583/43221081.pdf))  
- [R 6.4.1 Veranstaltungen](attachments/14651583/87985006.pdf)
- [R 6.4.3 Schulungsmaterialien](attachments/14651583/M6.4.3.pdf)
- [R 7.2 Marketingkonzept](attachments/14651583/R.%207.2.pdf)
- [R 7.3 / 8.1 DARIAH-DE Fellowship-Programm](attachments/14651583/DARIAH-DE_Fellowship-Programm_final.pdf?version=3&modificationDate=1422960041981&api=v2) / [DARIAH-DE-Award](attachments/14651583/DARIAH_DE_Award_FINAL.pdf?version=3&modificationDate=1422960050970&api=v2)
- [R 7.4 DARIAH-DE Wettbewerbe](attachments/14651583/R%207.4_DARIAH-DE_Wettbewerbe_FINAL.pdf)
- [R 7.7 DH-Portal](attachments/14651583/R%207.7%20DH-Portal.pdf)
- [R 7.8 Auswertung DARIAH-DE Outreach/Impact](attachments/14651583/DARIAH%20R.7.8.pdf)
- [R 7.9 Werkstattberichte](attachments/14651583/Report%207.9%20%E2%80%93%20Werkstattberichte%20%28Final%29.pdf)
- [R 8.1 / 7.3 DARIAH-DE Fellowship-Programm](attachments/14651583/DARIAH-DE_Fellowship-Programm_final.pdf?version=3&modificationDate=1422960041981&api=v2)
    / [DARIAH-DE-Award](attachments/14651583/DARIAH_DE_Award_FINAL.pdf?version=3&modificationDate=1422960050970&api=v2)  

### Milestones

- [M 2.2.2 Prototyp von Meta Hosting Service inkl. Bericht über den State of the Art für Meta Hosting Service](attachments/14651583/M2.2.2%20Prototype%20of%20Meta%20Hosting%20Service.pdf?version=2&modificationDate=1471941732951&api=v2)
    ([Appendix)](attachments/14651583/M2.2.2%20Appendix.pdf)
- [M 2.2.4 Beispielintegration von ausgewählten fachwissenschaftlichen Diensten in den Meta Hosting Service](attachments/14651583/R2.2.3undM2.2.4.pdf?version=2&modificationDate=1458655073159&api=v2)
- [M 2.3.3 Prototypische Implementierung der initialen technischen Workflows](attachments/14651583/M%202.3.3%20Prototypische%20Implementierung%20der%20initialen%20technischen%20Workflows.pdf)
- [M 2.4.1 Eingliederung der in DARIAH-DE entwickelten und angebotenen Komponenten in die Strukturen der Rechenzentren](attachments/14651583/M%202.4.1%20Eingliederung%20der%20in%20DARIAH-DE%20entwickelten%20und%20angebotenen%20Komponenten%20in%20die%20Strukturen%20der%20Rechenzentren.pdf)
- [M 4.1.1 Stakeholder: „Wissenschaftliche Sammlungen“](attachments/14651583/R%204.1.1%20Stakeholder%20Wissenschftliche%20Sammlungen.pdf)
- [M 4.2.2 Weiterentwicklung der Collection Registry - Schnittstellen](attachments/14651583/M4.2.2.pdf)
- [M 4.2.5 Eintragung existierender Forschungsdatensammlungen in die Collection Registry](attachments/14651583/M4.2.5-final.pdf)
- [M 4.3.1 Normdatensätze](attachments/14651583/M%204.3.1%20%20Normdatens%C3%A4tze.pdf)
- [M 4.3.2.1 DARIAH-DE-Repository (Prototyp)](attachments/14651583/M%204.3.2.1-DARIAH-Repositorium-Prototyp-final.pdf)
- [M 4.3.2.2 DARIAH-DE-Repository (Produktivsystem)](http://dx.doi.org/10.1515/bfp-2016-0020)
- M 4.4.1 Experten-Kolloqium zur Lizenzierung von Forschungsdaten in den Geschichtswissenschaften (Siehe Veranstaltung „Forschungsdaten für Andere - Lizenzen und Werkzeuge für Historiker“ URL: [https://de.dariah.eu/expertenkolloquien](https://de.dariah.eu/expertenkolloquien))
- M 4.4.2 Experten-Kolloqium zur Lizenzierung von Forschungsdaten in den Literaturwissenschaften (Siehe Veranstaltung „Lizenzierungsworkshop und TextGrid-Nutzertreffen“ URL:
    [https://de.dariah.eu/methoden-workshops](https://de.dariah.eu/methoden-workshops#collapseTen))
- M 4.4.4 Methoden-Workshop - Strategien zur Veröffentlichung und
    Vernetzung digitaler wissenschaftlicher Sammlungen (Siehe
    Veranstaltung „Veröffentlichung und Vernetzung digitaler
    wissenschaftlicher Forschungsdatensammlungen in den
    Geisteswissenschaften“ URL:
    [https://de.dariah.eu/methoden-workshops](https://de.dariah.eu/methoden-workshops))
- M 4.4.5 Methoden-Workshop - Interoperabilität durch kontrollierte
    Vokabulare und Normdaten (Siehe Veranstaltung „Quantitative
    Vorauswahl und Validierung für ein qualitatives Arbeiten in den
    Geisteswissenschaften: Ein iterativer Prozess?“ URL:
    [https://de.dariah.eu/methoden-workshops](https://de.dariah.eu/methoden-workshops))
- [M 4.4.6 Licensing-Tool (Produktivsystem)](attachments/14651583/M4.4.6-final.pdf)
- [M 5.2.1 Expertenkolloquium: Textkomplexität](http://dhd-blog.org/?p=6471)
- [M 5.2.2 Expertenkolloquium: Topic Models and Corpus Analysis](http://dhd-blog.org/?p=6472)
- M 5.3.2 Demonstratoren:
    [https://rawgit.com/DARIAH-DE/DARIAH-DKPro-Wrapper/master/doc/tutorial.html](https://rawgit.com/DARIAH-DE/DARIAH-DKPro-Wrapper/master/doc/tutorial.html),
    [http://search.de.dariah.eu/cosmotool/search](http://search.de.dariah.eu/cosmotool/search),
    [http://openphilology.github.io/migne-text-reuse/](http://openphilology.github.io/migne-text-reuse/)
- M 5.3.3 Use Case Komponenten:
    [https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper](https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper)
    [https://github.com/openphilology/migne-text-reuse](https://github.com/openphilology/migne-text-reuse)
- M 5.3.4 Public beta:
    [https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper](https://github.com/DARIAH-DE/DARIAH-DKPro-Wrapper)
    [https://github.com/openphilology/migne-text-reuse](https://github.com/openphilology/migne-text-reuse)
- [M 5.4.1 Stakeholder-Gremium](Begleitkonzept-zur-Vorbereitung-eines-Stakeholdergremiums-Fachgesellschaften.md)
- [M 5.4.2 Workshops 2014](attachments/14651583/M%205.4.2%20%C3%9Cbersicht%20Workshops%202015.pdf):
    zusammengefasst mit M 5.4.4 in 2015
- [M 5.4.3 Experten-Kolloquium](https://www.slm.uni-hamburg.de/forschung/tagungen/angewandte-dh.html)
- [M 5.4.4 Workshops 2015](attachments/14651583/M%205.4.2%20%C3%9Cbersicht%20Workshops%202015.pdf)
- [M 6.2.1 Spezifikation von Diensten](Spezifikation-von-Diensten.md) (korreliert mit [M 6.3.1](attachments/14651583/44827440.pdf))  
- [M 6.3.1 Integration](attachments/14651583/44827440.pdf)
- M 6.4.1 Zeitplan
- [M 6.4.2 Durchführung](attachments/14651583/M6.4.2.pdf)  
- M 6.4.3 Schulungsmaterialien
- [M 6.4.4 Bibliographie](attachments/14651583/87985009.pdf)
- M 8.2 / R 1.3.4 Nachhaltigkeitskonzept / Report nachhaltige
    Infrastruktur – Dieser Milestone/Report ist auf
    [Anfrage](mailto:info@de.dariah.eu) einzusehen.
- M 8.3 Finanzierungsmodell – Dieser Milestone ist auf
    [Anfrage](mailto:info@de.dariah.eu) einzusehen.

## Projektphase I

### Reports

- [R 1.3.3 Provenance](attachments/14651583/DARIAH-DE%20Report%201.3.3%20%E2%80%93%20Provenance.pdf)
- [R 2.2.3 Verfahren der Digital Humanities](attachments/14651583/M223_DH-Verfahren.pdf)
- [R 3.2.1 Essentials für fachspezifische Empfehlungen](attachments/14651583/R321_Empfehlungen_fuer_Metadaten_aus_einzelnen_Disziplinen_v1.0.pdf)
- [R 3.2.2 Fachspezifische Empfehlungen für Daten und Metadaten aus einzelnen Disziplinen (WIKI)](Fachspezifische-Empfehlungen-fuer-Daten-und-Metadaten.md)
- [R 1.3.2 Preservation Tools](attachments/14651583/R132_DARIAH-PreservationTools_2012-01-17.pdf)
- [R 3.3.1 fachübergreifende Interoperabilität](attachments/14651583/R3.3.1.pdf)
- [R 5.1.2 Jahresbericht 2012](attachments/14651583/20130423_DARIAH-EU_Annual_report_2012_DE-version.pdf)

### Milestones

- [M 1.2.1 Metadaten-Registry (Prototyp)](attachments/14651583/M1.2.1%20Schema%20Registry.pdf)
- [M 1.4.1.1 Archive in a Box](attachments/14651583/M1411_Archive_in_a_box.pdf)
- M 1.4.1.2 Service-Konzept, Prototyp (M1.4.1.2): intern)
- M 1.4.2.1 Facetted Browsing
- [M 1.4.3.1 Geobrowser Prototyp](attachments/14651583/M%201.4.3.1%20Geobrowser%20Prototyp_final.pdf)
- [M 2.2.5 Öffnung Portal](https://portal-de.dariah.eu/)
- [M 2.3.2 Konzepte zur Aus- und Fortbildung in den Digitalen Geisteswissenschaften](attachments/14651583/M223_KonzepteAus-undFortbildung.pdf)
- [M 2.3.3 DH studieren! Auf dem Weg zu einem Kern- und Referenzcurriculum](attachments/14651583/DARIAH-M2-3-3_DH-programs_1_1.pdf)
- [M 2.4.1 Expertenseminare](attachments/14651583/M241_Konzept-Expertenseminare.pdf)
- [M 2.4.2 Workshopreihe](attachments/14651583/M242_Methoden-Workshops%20v1.1.pdf)
- [M 2.4.3 Community Engagement auf EU-Ebene](attachments/14651583/M2.4.3_Community-Engagement%20v1.2a.pdf)
- [M 2.5.1.1 Blaupause für Virtuelle Forschungsumgebungen (1. Version)](attachments/14651583/VRE%20Blaupause%20Version%201.0%20Final.pdf)
- [M 2.5.1.2 Blaupause für Virtuelle Forschungsumgebungen (2. Version)](attachments/14651583/VRE%20Blaupause%202%200.pdf)
- [M 3.4.1 Metadaten-Crosswalk](attachments/14651583/M341_Metadaten-Crosswalk.pdf)
- [M 3.4.2 Collections Registry (Inhalt und Referenzen)](attachments/14651583/meilenstein_3_4_2.pdf)
- [M 4.3.2 DARIAH-DE Konferenz](attachments/14651583/M4.3.2_DARIAH-DE_Konferenz.pdf)
