# Sicherheitsvorfall / Security incident 15-04-2019

last modified on Apr 18, 2019

*English version below* 

Liebe Nutzende des DARIAH Wikis,

wir möchten Sie darüber informieren, dass am 15.04.2019 ein
Sicherheitsvorfall das DARIAH Wiki (wiki.de.dariah.eu) betreffend
entdeckt wurde. Das Wiki wurde sofort außer Betrieb genommen und der
Vorfall analysiert. Als Ursache wurde eine Sicherheitslücke in der
Wiki-Software „Confluence“ identifiziert. Nach eingehender Untersuchung
ist davon auszugehen, dass diese Sicherheitslücke in einem breit
angelegten Angriff automatisiert ausgenutzt wurde und die Wiki-Daten
selbst nicht das Ziel waren. Wir konnten keine Hinweise darauf finden,
dass Daten heruntergeladen oder verändert worden sind.

Ihr DARIAH Account und Ihre Zugangsdaten waren zu keiner Zeit gefährdet,
da keine diesbezüglichen Informationen oder gar Passworte im DARIAH Wiki
gespeichert oder von dort zugänglich sind. Folgende personenbezogenen
Daten könnten für die Angreifer sichtbar geworden sein: Name,
Emailadresse, letzte Aktivität, Autor von Beiträgen, freiwillige Angaben
im Persönlichen Bereich.

Das System wurde komplett neu aufgesetzt und am 16.04.2019 wieder in
Betrieb genommen.

Wir hoffen, dass der Ausfall Ihre Arbeit nicht nachhaltig beeinträchtigt
hat und stehen für weitere Fragen
unter [dco-de@de.dariah.eu](mailto:dco-de@de.dariah.eu) zur Verfügung.

Freundliche Grüße

Ihr DARIAH-DE Coordination Office

*Stand der Meldung: 18.04.2019 14:45 CEST*

Dear DARIAH Wiki user,

we would like to inform you about a security incident affecting the
DARIAH Wiki (wiki.de.dariah.eu) on 15.04.2019. The Wiki was immediately
taken out of service and the incident analyzed. A vulnerability in the
wiki software "Confluence" was identified as the cause. After a thorough
investigation, it can be assumed that this vulnerability was exploited
automatically in a large-scale attack and that the wiki data itself was
not the target. We couldn't find any evidence that data had been
downloaded or altered.

Your DARIAH account and access data have never been compromised as no
such information or passwords are stored in or accessible from the
DARIAH Wiki. The following personal data might have become visible to
the attackers: Name, email address, last activity, author of
contributions, optional details in the personal area.

The system was completely rebuilt and back online since 16-04-2019.

We hope the system outage did not strongly affect your work. We remain
available for further questions at
[dco-de@de.dariah.eu](mailto:dco-de@de.dariah.eu).

Kind regards

Your DARIAH-DE Coordination Office

*Last update: 18.04.2019 14:45 CEST*
