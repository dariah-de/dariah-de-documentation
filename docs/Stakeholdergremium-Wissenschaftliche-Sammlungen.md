# Stakeholdergremium "Wissenschaftliche Sammlungen"

last modified on Aug 23, 2018

Das Stakeholdergremium „Wissenschaftliche Sammlungen“ ist eine
Arbeitsgruppe, die durch DARIAH-DE ins Leben gerufen wurde und eine
Brücke zwischen dem digitalen Forschungsinfrastrukturprojekt und den
potentiellen NutzerInnen bildet.

Das Gremium setzt sich aus VertreterInnen unterschiedlicher geistes- und
kulturwissenschaftlicher Disziplinen zusammen. Diese unterscheiden sich
nicht nur im Hinblick auf ihren fachlichen Hintergrund voneinander,
sondern auch in Bezug auf die jeweils repräsentierten
EinzelforscherInnen, Forschungsinfrastrukturen oder Forschungs- bzw.
Gedächtnisinstitutionen. Neben Bibliotheken, Archiven und Museen gehören
dazu Universitäten, Akademien und andere außeruniversitäre
Forschungseinrichtungen.

Das Gremium wurde bereits in der zweiten Phase von DARIAH-DE einberufen.
Einen Rückblick auf die vergangene Arbeit und einen diskurshaften
Überblick der relevanten Themen leistet ein im Dezember 2016
erschienener Beitrag:

Jenny Oltersdorf, Stefan Schmunk: "Von Forschungsdaten und
wissenschaftlichen Sammlungen". In: Bibliothek Forschung und Praxis
40.2, 2016, 179–185.
[http://dx.doi.org/10.1515/bfp-2016-0036.](http://dx.doi.org/10.1515/bfp-2016-0036)

In der dritten Phase hat sich das Gremium vor allem mit der
Digitalisierung von kulturellem Erbe als Grundlage für digitale
Forschung beschäftigt. Der daraus hervorgegangene Beitrag erschien im
April 2018:

Lisa Klaffki, Stefan Schmunk, Thomas Stäcker. [„Stand der Kulturgutdigitalisierung in Deutschland. Eine Analyse und Handlungsvorschläge des DARIAH-DE Stakeholdergremiums ‚Wissenschaftliche Sammlungen‘“](http://resolver.sub.uni-goettingen.de/purl/?dariah-2018-1)
DARIAH-DE Working Papers Nr. 26. Göttingen: DARIAH-DE, 2018. URN:
[urn:nbn:de:gbv:7-dariah-2018-1-3](http://resolver.sub.uni-goettingen.de/purl/?dariah-2018-1).

## Beteiligte Institutionen und Personen

- Dr. Sonja Asal (Forschungsverbund Marbach Weimar Wolfenbüttel)
- Prof. Dr. Kai-Christian Bruhn (Fachhochschule Mainz)
- Fabian Cremer ((Max Weber Stiftung - Deutsche Geisteswissenschaftliche Institute im Ausland)  
- Aline Deicke (Akademie der Wissenschaften und der Literatur
Mainz)  
- Dr. Ralf Forster (Filmmuseum Potsdam)  
- Prof. Dr. Gudrun Gersmann (Universität zu Köln)  
- Dr. Alexander Geyken (Berlin-Brandenburgische Akademie der
Wissenschaften)  
- Prof. Dr. Nikolai Grube (Universität Bonn)  
- Gregor Horstkemper (Bayerische Staatsbibliothek München)  
- Prof. Dr. Martin Huber (Universität Bayreuth)  
- Dr. Michael Kaiser (Max Weber Stiftung - Deutsche
Geisteswissenschaftliche Institute im Ausland)  
- Dr. Roland S. Kamzelak (Deutsches Literaturarchiv Marbach)  
- Dr. Regina Keyler (Universitätsarchiv Tübingen)  
- Lisa Klaffki (Herzog August Bibliothek Wolfenbüttel)  
- PD Dr. Ingo Kottsieper (Akademie der Wissenschaften zu
Göttingen)  
- Dr. Beata Mache (Niedersächsische Staats- und
Universitätsbibliothek Göttingen)  
- Prof. Dr. Gerald Maier (Landesarchiv Baden-Württemberg)  
- Hanna-Lena Meiners (Niedersächsische Staats- und
Universitätsbibliothek Göttingen)  
- Anna Neovesky (Akademie der Wissenschaften und der Literatur
Mainz)  
- Prof. Dr. Heike Neuroth (Fachhochschule Potsdam)  
- Prof. Dr. Stefan Schmunk (Hochschule Darmstadt – University of
Applied Science)  
- Prof. Torsten Schrade (Akademie der Wissenschaften und der
Literatur Mainz)  
- Prof. Dr. Thomas Stäcker (Universitäts- und Landesbibliothek
Darmstadt)  
- Dipl.-Sozwiss. Ralf Stockmann (Staatsbibliothek zu Berlin)  
- Dr. Cornelia Weber (Hermann von Helmholtz - Zentrum für
Kulturtechnik)  
- John Hendrik Weitzmann (Creative Commons Deutschland)  
- Dr. Thomas Werneke (Zentrum für Zeithistorische Forschung
Potsdam)  
- Thorsten Wübbena (Universität Frankfurt am Main / DFK Paris)

## Kontakt

Lisa Klaffki, [klaffki@hab.de](mailto:klaffki@hab.de)

## Treffen

- 11.09.2017, KGI Frankfurt/Main
  - [https://dhd-blog.org/?p=8714](https://dhd-blog.org/?p=8714)
- 22.05.2017, HAB Wolfenbüttel
- 02.12.2016, BBAW Berlin
- 23.08.2016, SUB Göttingen
  - [http://dhd-blog.org/?p=7401](http://dhd-blog.org/?p=7401)
- 23.11.2015, DAI Berlin
- 29.05.2015, WikiMedia Deuschland, Berlin
- 02.03.2015, HU Berlin
- 17.10.2014, SUB Göttingen
- 23.07.2014, SUB Göttingen
