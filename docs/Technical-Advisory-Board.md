# Technical Advisory Board

last modified on Mär 07, 2018

Das Technical Advisory Board begleitet die Entwicklung der technischen
Infrastruktur der Projekte CLARIN-D und DARIAH-DE. Dieses Gremium ist
international, mit sechs IT-SpezialistInnen besetzt, die die (Weiter-)
Entwicklung der technischen Komponenten der Forschungsinfrastruktur
fördern. Dazu erhalten die Beiratsmitglieder einen privilegierten
Zugriff auf Informationen über und Ergebnisse aus dem Projekt. Zudem ist
eine E-Mail-Liste über aktuelle Themen eingerichtet worden.

Für Informationen zu den Mitgliedern des Technical Advisory Board
besuchen Sie bitte das [DARIAH-DE Portal](https://de.dariah.eu/de_DE/dariah-gremien).
