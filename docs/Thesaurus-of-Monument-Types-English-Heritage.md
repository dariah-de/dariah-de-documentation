# Thesaurus of Monument Types, English Heritage

last modified on Feb 17, 2014

[4.5 Objektinformationen](4.5-Objektinformationen.md)

[English Heritage](http://www.english-heritage.org.uk/publications/guidelines-and-standards/) ist
das britische Equivalent auf nationaler Ebene zu den
Landesdenkmalpflegeämtern in Deutschland und veröffentlicht Standards
und Richtlinien für die Katalogisierung von Denkmälern. Der [Thesaurus of Monument Types](http://thesaurus.english-heritage.org.uk/thesaurus.asp?thes_no=1) umfasst **18 Hauptgruppen** zu immobilen Objekten:

- agriculture and subsistence
- civil
- commemorative
- commercial
- communications
- defence
- domestic
- education
- gardens parks and urban spaces
- health and welfare
- industrial
- maritime
- monument \<by form\>
- recreational
- religious ritual and funerary
- transport
- unassigned
- water supply and drainage
