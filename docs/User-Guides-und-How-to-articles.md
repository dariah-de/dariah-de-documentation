# User Guides und How-to articles

last modified on Jan 25, 2022

Auf den nachfolgenden Seiten finden Sie User-Guides und How-to Artikel zu einzelnen Tools von DARIAH-DE. Auch die technische Dokumentation finden Sie in diesem Bereich. In den FAQ´s und den Nutzungsempfehlungen der Tools können Sie sich weitergehend informieren. Wenn Sie dennoch Fragen haben kontaktieren Sie uns gerne unter den jeweils angegebenen Kontaktdaten.

## [Das DARIAH-DE Repository](Das-DARIAH-DE-Repository.md)

## [Die Collection Registry](Die-Collection-Registry.md)

## [DARIAH Collection Description Data Model](DARIAH-Collection-Description-Data-Model-DCDDM.md)

## [Empfehlungen für Forschungsdaten, Tools und Metadaten in der DARIAH-DE Infrastruktur](Empfehlungen-fuer-Forschungsdaten.md)

## [DARIAH-DE Service Life Cycle](DARIAH-DE-Service-Life-Cycle.md)
