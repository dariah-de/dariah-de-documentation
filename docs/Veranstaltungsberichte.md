# Veranstaltungsberichte

last modified on Jan 15, 2018

## 8. TextGrid/DARIAH-Nutzertreffen

Unter dem Motto „Aller Anfang ist leicht“ fand am 13. und 14. Oktober
2016 an der [Christian-Albrechts-Universität zu Kiel](http://www.uni-kiel.de/) das achte TextGrid/DARIAH-Nutzertreffen
statt. Die Veranstaltung sollte vor allem EinsteigerInnen die
Möglichkeit geben, TextGrid anhand von typischen Nutzungsszenarien
kennenzulernen. Nach einer TEI-Einführung wurden Arbeitsabläufe bei der
Arbeit in der virtuellen Forschungsumgebung wie beispielsweise bei der
Erstellung einer digitalen Edition vorgestellt und in einfachen
praktischen Übungen von den TeilnehmerInnen selbst ausprobiert. Als
Vorkenntnisse wurden von den 35 TeilnehmerInnen lediglich Erfahrungen im
Umgang mit Computern erwartet.

### Vorträge

Philipp Hegel, Oliver Schmid: Hindernisse auf dem Weg zu TextGrid ...
und wie man sie überwindet

Michelle Rodzis, Larissa Figgen: Einblicke in die Arbeitsweise der
kritischen Hybridedition Bibliothek der Neologie (BdN)

Dirk Fleischer: Wissens- und Informationsstrukturen in der *UN
Convention for the Law of the Sea* mit TextGrid

Bertold Scharf: Quellensammlung zur Geschichte von Menschen mit
Behinderungen

### Workshops

Ein behutsamer Einstieg in XML und die Richtlinien der TEI

Einstieg in TextGrid: Erste Schritte in der virtuellen
Forschungsumgebung

Digitale Editionen: Die nächsten Schritte mit TextGrid

## 9. TextGrid/DARIAH-Nutzertreffen

Am 21. und 22. März 2017 fand an der [Bayerischen Akademie der Wissenschaften](https://www.badw.de/die-akademie.html) (BAdW) in München
das neunte TextGrid/DARIAH-Nutzertreffen mit den Schwerpunkten Digitale
Editionen und Geodaten statt. Wie beim achten Nutzertreffen sollte die
Veranstaltung vor allem EinsteigerInnen die Möglichkeit geben, TextGrid
anhand von typischen Nutzungsszenarien kennenzulernen. Nach einer
Einführung in das TextGridLab als virtuelle Arbeits- und
Forschungsumgebung wurde die Auszeichnungssprache XML/TEI im Rahmen
typischer Arbeitsschritte beim Erstellen digitaler Editionen anhand von
Beispielen vorgestellt. In einem weiteren Workshop wurde gezeigt, wie
Geo-Daten in XML-Dokumenten in anderen Werkzeugen wie dem DARIAH
Geo-Browser nachgenutzt werden können. Die vorgestellten Arbeitsabläufe
wurden im Rahmen der Workshops von den TeilnehmerInnen in einfachen
praktischen Übungen selbst ausprobiert. Als Vorkenntnisse wurden
lediglich Erfahrungen im Umgang mit Computern erwartet.

### Vorträge

Philipp Hegel, Oliver Schmid: Hindernisse auf dem Weg zu TextGrid ...
und wie man sie überwindet

Henry Zepeda, Bojidar Dimitrov: Online Transcriptions of Ptolemaic
Manuscripts and Users’ Experiences with Classical Text Editor

Jesper Zedlitz: cueML - eine kulinarische Textedition

Gabriele Radecke: Vorstellung der Ergebnisse des Projekts
"Genetisch-kritische und kommentierte Hybrid-Edition von
Theodor-Fontanes Notizbüchern"

### Workshops

Einstieg in TextGrid: Erste Schritte in der virtuellen
Forschungsumgebung

Ein behutsamer Einstieg in XML und die Richtlinien der TEI

In 80 Minuten um die Welt: Bilder und Geo-Daten in TextGrid/DARIAH

## 10. TextGrid/DARIAH-Nutzertreffen

Das zehnte TextGrid/DARIAH-Nutzertreffen zum Thema „Annotation mit
DARIAH-Werkzeugen“ fand am 25. und 26. Oktober 2017 am
[Max-Planck-Institut für Wissenschaftsgeschichte](https://www.mpiwg-berlin.mpg.de/de/) (MPIWG) in
Berlin statt. Das Nutzertreffen wurde mit einer Einführung in die
virtuelle Forschungsumgebung TextGrid eröffnet, im weiteren Verlauf der
Veranstaltung wechselten sich Vorträge und interaktive Workshops über
verschiedene Ausprägungen von Annotation und das Web Annotation Data
Model sowie die Werkzeuge Digilib und Annotation Sandbox ab. Abgerundet
wurde das Nutzertreffen durch eine Diskussionsrunde zu Anforderungen an
Funktionalität und Benutzerfreundlichkeit von Annotationswerkzeugen, an
der sich die 19 TeilnehmerInnen aktiv beteiligten.

### Vorträge

Harald Lordick: Geschichte annotiert – Web-Annotation in den
Geisteswissenschaften

Robert Casties: Bilder im Netz - digilib und IIIF in TextGrid und
darüber hinaus

Dirk Wintergrün: Beispielhafte Workflows für die Erschließung
historischer Korpora in der Wissenschaftsgeschichte

Danah Tonne: Bildannotation mit dem Web Annotation Data Model

### Workshops

Einstieg in TextGrid: Erste Schritte in der virtuellen
Forschungsumgebung

Annotation-Sandbox und Digilib

### Diskussionsrunde

Anforderungen an Funktionalität und Benutzerfreundlichkeit von
Annotationswerkzeugen
