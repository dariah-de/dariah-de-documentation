# Verhaltenskodex fuer die DARIAH-DE Dienste

last modified on Mai 07, 2020

Dieser Service Provider (SP) wird den relevanten Rechtsrahmen für den
Schutz von Personalangaben, orientiert an den EU-Datenschutz
Richtlinien, respektieren. Es gilt der
[Datenschutzhinweis](https://de.dariah.eu/privacy-policy).

| **Name des Services**                  | **DARIAH Services** |
|----------------------------------------|---------------------|
| Beschreibung des Services | Verschiedene Dienstleistungen, die durch das DARIAH-DE-Projekt (Digitalforschungsinfrastruktur für die Geistes- und Kulturwissenschaften) zur Verfügung gestellt werden |
| Datenkontrolleur und<br>Kontaktperson | GWDG<br>Am Faßberg 11<br>37077 Göttingen<br>Tibor Kálmán, [dariah-coc@gwdg.de](mailto:dariah-coc@gwdg.de) |
| Zuständigkeit /<br>Rechtssprechung | DE-NS Deutschland Niedersachsen |
| Personalangaben bearbeiten | 1. Folgende Daten werden von ihrer Heimatorganisation abgerufen:<br> - Ihre persönliche User ID (eduPersonPrincipalName, ePPN)<br>2. Folgende Daten werden wir bei Ihnen in Erfahrung bringen, wenn Ihre Heimatorganisation diese nicht zur Verfügung stellt:<br> - Vorname (givenname)<br> - Nachname (sn)<br> - Loginname (cn)<br> - E-Mail Addresse (mail)<br> - Name Ihrer Organisation (o)<br>3. Folgende Daten werden wir bei Ihnen in Erfahrung bringen:<br> - Akzeptanz der DARIAH Terms Of Use<br>4. Folgende Daten werden von der zentralen DARIAH-Attributautorität gesammelt:<br> - Mitgliedschaft in Berechtigungssgruppen |
| Zweck der Verarbeitung<br>von Personalangaben | Personalangaben und Protokolldateien werden verwendet für:<br> - Personalisierung von Webinhalten<br> - Nutzerautorisierung<br> - das Produzieren automatischer E-Mails<br> - das Diagnostizieren von technischen Problemen und Statistik |
| Dritte, denen Personalangaben<br>bekannt gegeben werden | Personalangaben werden an keine Dritten weitergegeben. |
| Wie man auf Personalangaben<br>zugreift, sie berichtigen und<br>löschen kann | Kontaktieren Sie die oben genannte Kontaktperson.<br>Um die von Ihrer Heimatorganisation veröffentlichten Daten zu berichtigen, kontaktieren Sie den IT-Informationsschalter Ihrer Heimatorganisation. |
| Datenspeicherung | Personalangaben werden nach Anfrage oder aber nach fünf Jahren der Untätigkeit eines Benutzers gelöscht.<br>Webserversoftwareprotokolle werden nach 90 Tagen gelöscht. |
| Datenschutzverhaltenskodex | Die Angaben zu ihrer Person werden gemäß des Verhaltenskodexes für Dienstleister (Code of Conduct for Service Providers), einem allgemeinen Standard für die Forschung und den Hochschulbildungssektor, aufbewahrt, um Ihre Privatsphäre zu schützen. |

Die aktuelle Liste von Dienstleistungen ist an diesen Verhaltenkodex
gebunden und kann hier eingesehen
werden: [https://de.dariah.eu/access-to-protected-resources-with-your-home-organisation-s-user-account](https://de.dariah.eu/access-to-protected-resources-with-your-home-organisation-s-user-account).

(Diese Übersetzung dient nur zu Informationszwecken. Rechtlich verbindlich ist ausschließlich die [englische Fassung](Code-of-Conduct-for-DARIAH-Services.md)).
