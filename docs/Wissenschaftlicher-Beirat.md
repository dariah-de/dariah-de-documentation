# Wissenschaftlicher Beirat

last modified on Jan 16, 2018

Der Wissenschaftliche Beirat aus FachwissenschaftlerInnen und
IT-SpezialistInnen begleitet die Entwicklung von DARIAH-DE. Dieses
Gremium mit seinen Mitgliedern aus dem deutschsprachigen Raum tagt circa
einmal im Jahr, um das Projekt und den Aufbau einer digitalen
Forschungsinfrastruktur aktiv zu begleiten und zu fördern. Zu diesem
Zweck erhalten die Beiratsmitglieder einen privilegierten Zugriff auf
Informationen über das Projekt und dessen Ergebnisse. Hierfür ist eine
E-Mail-Liste über aktuelle Themen eingerichtet worden.

## Mitglieder

- [Prof. Dr. Thomas Bürger](http://www.slub-dresden.de/ueber-uns/kontakt/aufgaben-von-prof-dr-thomas-buerger/index.html) (Sächsische Landesbibliothek – Staats- und Universitätsbibliothek Dresden,
    Generaldirektor)
- [Prof. Dr. Elisabeth Burr](http://www.uni-leipzig.de/%7Eburr/)(Universität
    Leipzig, Französische / frankophone und italienische
    Sprachwissenschaft)
- [Prof. Dr. Ortwin Dally](mailto:ortwin.dally@dainst.de) (Leitender
    Direktor Abteilung Rom des Deutschen Archäologischen Instituts)
- [Prof. Dr. Gudrun Gersmann](http://histinst.phil-fak.uni-koeln.de/gersmann.html)
    (Universität zu Köln, Historisches Institut)
- [Prof. Dr. Thomas Gloning](http://www.zmi.uni-giessen.de/home/profil-tgloning.html) (Universität
    Gießen, Zentrum für Medien und Interaktivität)
- [Prof. Dr. Günther Görz](http://www8.informatik.uni-erlangen.de/en/goerz.html) (Universität
    Erlangen, Institut für Informatik)
- [Dr. Peter Leinen](http://leinen.rz.uni-mannheim.de/leinen/kontakt/index.html) (Deutsche
    Nationalbibliothek Frankfurt, Leiter Informationstechnik)
- [Prof. Dr. Peter Matussek](http://www.peter-matussek.de/) (Universität
    Siegen, Medienwissenschaftliches Seminar)
- [Prof. Dr. Wolfgang Nagel](http://tu-dresden.de/die_tu_dresden/zentrale_einrichtungen/zih/wir_ueber_uns/mitarbeiter/nagel) (TU
    Dresden, Direktion Zentrum für Informationsdienste und
    Hochleistungsrechnen)
- [Prof. Dr. Gudrun Oevel](http://www.uni-paderborn.de/person/14924/)
    (Zentrum für Informations- und Medientechnologien (IMT), Universität
    Paderborn)
- [Ao.Univ.Prof. Dipl.-Ing. Dr.techn. Andreas Rauber](http://www.ifs.tuwien.ac.at/user/42)(TU
    Wien, Information & Software Engineering Group)
- [Frank Scholze](http://www.bibliothek.kit.edu/cms/mitarbeiter_107.php) (Karlsruher
    Institut für Technologie, Direktion Bibliothek)
- [Univ.-Prof. Dipl.-Ing. Dr. techn. Robert Sablatnig](mailto:Robert.sablatnig@tuwien.ac.at) (Technische
    Universität Wien, Institut für rechnergestützte Automation)  
    [Prof. Dr. Manfred Stede](mailto:stede@uni-potsdam.de) (Universität
    Potsdam, Angewandte Computerlinguistik)
- [Ass.-Prof. Mag. Dr. Johannes H. Stigler](https://online.uni-graz.at/kfu_online/visitenkarte.show_vcard?pPersonenId=6FAA7A93B3B3AD57&pPersonenGruppe=3) (Universität
    Graz, Zentrum für Informationsmodellierung in den
    Geisteswissenschaften)
- [Prof. Dr. Angelika Storrer](http://germanistik.uni-mannheim.de/abteilungen/germanistische_linguistik/prof_dr_angelika_storrer/index.html) (Universität
    Mannheim, Lehrstuhl für Germanistische Linguistik, Seminar für
    deutsche Philologie)

## Gäste (Förderer)

- [Dr. Henning Krüger](http://www.mwk.niedersachsen.de/portal/live.php?navigation_id=6250&_psmand=19) (Ministerium für Wissenschaft und Kultur Niedersachsen)
- [Hans Nerlich](http://pt-dlr-gsk.de/de/994.php) (PT-DLR-Geistes-, Sozial- und  ulturwissenschaften für das Bundesministerium für Bildung und Forschung)
