# DARIAH-DE – Digitale Forschungsinfrastruktur für die Geistes- und Kulturwissenschaften

last modified on Jan 25, 2019

**DARIAH-DE** unterstützt mit digitalen Ressourcen und Methoden arbeitende Geistes- und KulturwissenschaftlerInnen in Forschung und Lehre. Dafür baut das Projekt eine digitale Forschungsinfrastruktur für Werkzeuge und Forschungsdaten auf und entwickelt Materialien für Lehre und Weiterbildung im Bereich der Digital Humanities (DH). Die Ergebnise
dieser Arbeit sollen auf dieser Seite sowie dem [DARIAH-DE-Portal](https://de.dariah.eu/) dokumentiert und der Öffentlichkeit zur Verfügung gestellt werden.

Sie wollen mehr über DARIAH-DE erfahren? Dann nehmen Sie gern Kontakt zur Konsortialleitung auf: [info@de.dariah.eu](mailto:info@de.dariah.eu?subject=DARIAH-DE%3A%20Generelle%20Fragen%20und%20Kontakt)

## Dokumentation

- [User Guides und How-to articles](User-Guides-und-How-to-articles.md)
- [Technische Dokumentation](Technische-Dokumentationen.md)
- [Reports und Milestones](Reports-and-Milestones.md)

## Gremien

- [Stakeholdergremium "Wissenschaftliche Sammlungen"](Stakeholdergremium-Wissenschaftliche-Sammlungen.md)
- [Technical Advisory Board (TAB)](Technical-Advisory-Board.md)
- [Wissenschaftlicher Beirat](Wissenschaftlicher-Beirat.md)

## Weitere Informationen zum Projekt

- [Projekt-Cluster](https://de.dariah.eu/dariah-de-in-kurze)
- [Publikationen und Präsentationen](https://de.dariah.eu/publikationen-und-prasentationen)
- [DARIAH-DE Portal](https://de.dariah.eu)
- [DHd-Kanal](https://www.youtube.com/user/dhdkanal)
- [DHd-Blog](https://dhd-blog.org/)
- [Twitter](httpss://twitter.com/DARIAHde)
- [Homepage DARIAH-EU](https://www.dariah.eu/)
- [ESFRI](https://www.esfri.eu/)
- Annotationen: [Liste mit Annotationstools](https://docs.google.com/spreadsheet/ccc?key=0AgJnN0WXLOardFBVWmlJbk9HUk5iRW9LN19WYVhIVnc&usp=drive_web#gid=0) (Google Tabelle)
