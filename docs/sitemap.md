# Sitemap

- [DARIAH-DE Digitale Forschungsinfrastruktur für die Geistes- und Kulturwissenschaften](index.md)
  - [User Guides und How-to articles](User-Guides-und-How-to-articles.md)
    - [Das DARIAH-DE Repository](Das-DARIAH-DE-Repository.md)
    - [Die Collection Registry](Die-Collection-Registry.md)
      - [Best-Practice-Empfehlungen](Best-Practice-Empfehlungen.md)
    - [DARIAH Collection Description Data Model (DCDDM)](DARIAH-Collection-Description-Data-Model-DCDDM.md)
    - [DARIAH-DE Service Life Cycle](DARIAH-DE-Service-Life-Cycle.md)
      - [FAQs zur Datenföderationsarchitektur (DFA)](FAQs-zur-Datenfoederationsarchitektur-DFA.md)
      - [FAQs zum DARIAH-DE Repository](FAQs-zum-DARIAH-DE-Repository.md)
      - [FAQs zum Data Modelling Environment](FAQs-zum-Data-Modelling-Environment.md)
      - [FAQs zum Publikator](FAQs-zum-Publikator.md)
      - [FAQs zur Collection Registry](FAQs-zur-Collection-Registry.md)
      - [FAQs zur Generic Search](FAQs-zur-Generic-Search.md)
    - [Das DARIAH-DE Repository und das TextGrid Repository](Das-DARIAH-DE-Repository-und-das-TextGrid-Repository.md)
    - [Empfehlungen für Forschungsdaten, Tools und Metadaten in der DARIAH-DE Infrastruktur](Empfehlungen-fuer-Forschungsdaten.md)
  - [Technische Dokumentationen](Technische-Dokumentationen.md)
    - [DARIAH AAI Documentation](DARIAH-AAI-Documentation.md)
      - [Connect the DARIAH Attribute Authority](Connect-the-DARIAH-Attribute-Authority.md)
      - [DARIAH AAI NG Service Provider Workshop 2019](DARIAH-AAI-NG-Service-Provider-Workshop-2019.md)
      - [DARIAH Self Service Documentation](DARIAH-Self-Service-Documentation.md)
      - [DARIAH User Administration Documentation](DARIAH-User-Administration-Documentation.md)
      - [Example Shibboleth SP Configuration](Example-Shibboleth-SP-Configuration.md)
      - [Integrating Shibboleth Authentication into your Application](Integrating-Shibboleth-Authentication-into-your-Application.md)
      - [Known Issues / FAQ](Known-Issues-FAQ.md)
    - [DARIAH-DE Annotation Sandbox](DARIAH-DE-Annotation-Sandbox.md)
    - [DARIAH-DE Normdatendienste](DARIAH-DE-Normdatendienste.md)
    - [DARIAH-DE Repository](DARIAH-DE-Repository.md)
    - [Datasheet Editor Dokumentation](Datasheet-Editor-Dokumentation.md)
    - [Digilib](Digilib.md)
    - [Etherpad-Lite Dokumentation](Etherpad-Lite-Dokumentation.md)
    - [Geo-Browser Dokumentation](Geo-Browser-Dokumentation.md)
  - [Reports and Milestones](Reports-and-Milestones.md)
    - [Begleitkonzept zur Vorbereitung eines Stakeholdergremiums „Fachgesellschaften“ (M 5.4.1)](Begleitkonzept-zur-Vorbereitung-eines-Stakeholdergremiums-Fachgesellschaften.md)
    - [Fachspezifische Empfehlungen für Daten und Metadaten](Fachspezifische-Empfehlungen-fuer-Daten-und-Metadaten.md)
      - [0. Präambel](0.-Praeambel.md)
      - [1. Einleitung](1.-Einleitung.md)
        - [1.1 Ziel und methodisches Vorgehen](1.1-Ziel-und-methodisches-Vorgehen.md)
        - [1.2 Terminologie](1.2-Terminologie.md)
      - [2. Forschungsprozess](2.-Forschungsprozess.md)
        - [2.1 Forschungsmethoden und -verfahren](2.1-Forschungsmethoden-und--verfahren.md)
        - [2.2 Forschungsdaten – Nutzung von Standards in den Geisteswissenschaften](2.2-Forschungsdaten.md)
        - [2.3 Dokumentation der Forschungsergebnisse – Administrative Metadaten](2.3-Dokumentation-der-Forschungsergebnisse.md)
      - [3. Daten- und Metadatenformate in den Fachdisziplinen](3.-Daten--und-Metadatenformate-in-den-Fachdisziplinen.md)
        - [3.1 Überblick](3.1-Ueberblick.md)
        - [3.2 Archäologie](3.2-Archaeologie.md)
        - [3.3 Musikwissenschaft](3.3-Musikwissenschaft.md)
          - [Humdrum](Humdrum.md)
          - [MEI](MEI.md)
          - [MuseData](MuseData.md)
          - [MusicXML](MusicXML.md)
        - [3.4 Geschichtswissenschaft](3.4-Geschichtswissenschaft.md)
        - [3.5 Judaistik und Hebraistik](3.5-Judaistik-und-Hebraistik.md)
        - [3.6 Geisteswissenschaftliche Hilfswissenschaften](3.6-Geisteswissenschaftliche-Hilfswissenschaften.md)
      - [4. Beschreibung disziplinübergreifender Objekte](4.-Beschreibung-disziplinuebergreifender-Objekte.md)
        - [4.1 Überblick](4.1-Ueberblick.md)
        - [4.2 Datierung](4.2-Datierung.md)
        - [4.3 Ortsbeschreibung und Georeferenzierung](4.3-Ortsbeschreibung-und-Georeferenzierung.md)
        - [4.4 Personeninformationen](4.4-Personeninformationen.md)
        - [4.5 Objektinformationen](4.5-Objektinformationen.md)
          - [Cultural Objects Name Authority (CONA) und Art & Architecture Thesaurus (AAT)](Cultural-Objects-Name-Authority-CONA-und-Art-Architecture-Thesaurus-AAT.md)
          - [Dewey-Dezimalklassifikation (DDC)](Dewey-Dezimalklassifikation-DDC.md)
          - [Hessische Systematik](Hessische-Systematik.md)
          - [Hornbostel-Sachs-Klassifikation](Hornbostel-Sachs-Klassifikation.md)
          - [Iconclass](Iconclass.md)
          - [Oberbegriffsdatei (OBG)](Oberbegriffsdatei-OBG.md)
          - [Object Names Thesaurus und Materials Thesaurus des British Museum](Object-Names-Thesaurus-und-Materials-Thesaurus-des-British-Museum.md)
          - [Thesaurus of Monument Types, English Heritage](Thesaurus-of-Monument-Types-English-Heritage.md)
        - [4.6 Quellennachweise](4.6-Quellennachweise.md)
        - [4.7 Ereignisbeschreibung](4.7-Ereignisbeschreibung.md)
      - [5. Kontrolliert-Strukturierte Vokabulare](5.-Kontrolliert-Strukturierte-Vokabulare.md)
      - [6. Schlussfolgerungen](6.-Schlussfolgerungen.md)
      - [7. Anhang](7.-Anhang.md)
        - [7.1 Literatur und URLs](7.1-Literatur-und-URLs.md)
        - [7.2 Abkürzungen](7.2-Abkuerzungen.md)
        - [7.3 Zusammenstellung geisteswissenschaftlicher Quellentypen](7.3-Zusammenstellung-geisteswissenschaftlicher-Quellentypen.md)
        - [7.4 MEI Beispiel](7.4-MEI-Beispiel.md)
    - [Gesamtgeschäftsmodell für DARIAH-DE](Gesamtgeschäftsmodell-fuer-DARIAH-DE.md)
    - [M 5.3.2 Expertenworkshop 'Topic Modeling', Göttingen, 20.05.16](Expertenworkshop-Topic-Modeling.md)
    - [M 6.2.1 – Spezifikation von Diensten (public version)](Spezifikation-von-Diensten.md)
  - [Gremien](Gremien.md)
    - [Stakeholdergremium "Wissenschaftliche Sammlungen"](Stakeholdergremium-Wissenschaftliche-Sammlungen.md)
    - [Technical Advisory Board](Technical-Advisory-Board.md)
    - [Wissenschaftlicher Beirat](Wissenschaftlicher-Beirat.md)
  - [Veranstaltungsberichte](Veranstaltungsberichte.md)
